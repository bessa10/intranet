setTimeout(function () {
    $('#alert').hide();
}, 6000);

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
	if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
		document.getElementById("navbar").style.padding = "2px 10px";
		document.getElementById("logo").style.height = "27px";
		document.getElementById("icon-sticky").style.fontSize = "17px";
		document.getElementById("text-sticky").style.display = "none";
		document.getElementById("icon-sticky2").style.fontSize = "17px";
		document.getElementById("text-sticky2").style.display = "none";
		document.getElementById("icon-sticky3").style.fontSize = "17px";
		document.getElementById("text-sticky3").style.display = "none";
		document.getElementById("icon-sticky4").style.fontSize = "17px";
		document.getElementById("text-sticky4").style.display = "none";
		document.getElementById("icon-sticky5").style.fontSize = "17px";
		document.getElementById("text-sticky5").style.display = "none";
		document.getElementById("icon-sticky6").style.fontSize = "17px";
		document.getElementById("text-sticky6").style.display = "none";
		document.getElementById("icon-sticky7").style.fontSize = "17px";
		document.getElementById("text-sticky7").style.display = "none";
	} else {
		document.getElementById("navbar").style.padding = "10px 10px";
		document.getElementById("logo").style.height = "35px";
		document.getElementById("icon-sticky").style.fontSize = "20px";
		document.getElementById("text-sticky").style.display = "block";
		document.getElementById("icon-sticky2").style.fontSize = "20px";
		document.getElementById("text-sticky2").style.display = "block";
		document.getElementById("icon-sticky3").style.fontSize = "20px";
		document.getElementById("text-sticky3").style.display = "block";
		document.getElementById("icon-sticky4").style.fontSize = "20px";
		document.getElementById("text-sticky4").style.display = "block";
		document.getElementById("icon-sticky5").style.fontSize = "20px";
		document.getElementById("text-sticky5").style.display = "block";
		document.getElementById("icon-sticky6").style.fontSize = "20px";
		document.getElementById("text-sticky6").style.display = "block";
		document.getElementById("icon-sticky7").style.fontSize = "20px";
		document.getElementById("text-sticky7").style.display = "block";
	}
}
/*$(document).ready(function(){
	$(".btn-avancar").click(function() {
		$(".btn-avancar").attr('href', '#2a');
	});
})
*/
$(document).ready(function() {

	$("#ok-curso").css('display', 'none');
	$("#ok-docente").css('display', 'none');
	$("#ok-categoria").css('display', 'none');

	if (window.location.href.indexOf("add_curso") > -1) {
		$( ".curso-menu" ).addClass( "active" );
		$('.block-menu').addClass('disabled');

		if($('.block-menu').hasClass("active")) {
			$('.active').removeClass('disabled');
		}
	}

	if (window.location.href.indexOf("add_docente") > -1) {
		$( ".docente-menu" ).addClass( "active" );
		$('.block-menu').addClass('disabled');

		$("#ok-curso").css('display', 'block');

		if($('.block-menu').hasClass("active")) {
			$('.active').removeClass('disabled');
			$('.curso-menu').css('background','#064579');
			$('.curso-menu a').css('color','white');
		}
	}

	if (window.location.href.indexOf("add_categoria") > -1) {
		$( ".cat-menu" ).addClass( "active" );
		$('.block-menu').addClass('disabled');

		$("#ok-curso").css('display', 'block');
		$("#ok-docente").css('display', 'block');

		if($('.block-menu').hasClass("active")) {
			$('.active').removeClass('disabled');
			$('.curso-menu').css('background','#064579');
			$('.curso-menu a').css('color','white');
			$('.docente-menu').css('background','#064579');
			$('.docente-menu a').css('color','white');
		}
	}
	if (window.location.href.indexOf("marketing_publico_alvo") > -1) {
		$( ".mkt-menu" ).addClass( "active" );
		$( ".pa-menu" ).addClass( "active" );
		$('.block-menu').addClass('disabled');

		$("#ok-curso").css('display', 'block');
		$("#ok-docente").css('display', 'block');
		$("#ok-categoria").css('display', 'block');

		$(function(){
			$('#dropdownMenuLink').click();
		});
		if($('.block-menu').hasClass("active")) {
			$('.active').removeClass('disabled');
			$('.curso-menu').css('background','#064579');
			$('.curso-menu a').css('color','white');
			$('.docente-menu').css('background','#064579');
			$('.docente-menu a').css('color','white');
			$('.cat-menu').css('background','#064579');
			$('.cat-menu a').css('color','white');
		}
	}

	if (window.location.href.indexOf("marketing_persona") > -1) {
		$( ".mkt-menu" ).addClass( "active" );
		$( ".persona-menu" ).addClass( "active" );
		$('.block-menu').addClass('disabled');
		$(function(){
			$('#dropdownMenuLink').click();
		});
		if($('.block-menu').hasClass("active")) {
			$('.active').removeClass('disabled');
			$('.curso-menu').css('background','#064579');
			$('.curso-menu a').css('color','white');
			$('.docente-menu').css('background','#064579');
			$('.docente-menu a').css('color','white');
			$('.cat-menu').css('background','#064579');
			$('.cat-menu a').css('color','white');
			$('.pa-menu').css('background','#064579');
			$('.pa-menu').css('color','white');

		}
	}

	if (window.location.href.indexOf("relevancia") > -1) {
		$( ".mkt-menu" ).addClass( "active" );
		$( ".relevancia-menu" ).addClass( "active" );
		$('.block-menu').addClass('disabled');
		$(function(){
			$('#dropdownMenuLink').click();
		});
		if($('.block-menu').hasClass("active")) {
			$('.active').removeClass('disabled');
			$('.curso-menu').css('background','#064579');
			$('.curso-menu a').css('color','white');
			$('.docente-menu').css('background','#064579');
			$('.docente-menu a').css('color','white');
			$('.cat-menu').css('background','#064579');
			$('.cat-menu a').css('color','white');
			$('.pa-menu').css('background','#064579');
			$('.pa-menu').css('color','white');
			$('.persona-menu').css('background','#064579');
			$('.persona-menu').css('color','white');

		}
	}

	var input = document.querySelector('input[name="key_words"]'),
		// init Tagify script on the above inputs
		tagify = new Tagify(input, {
			whitelist: ["Ultrassom da tireoide", "Ultrassom das glândulas salivares", "Níveis cervicais", "Linfonodos", "Região\n" +
			"cervical", "Paratireoide", "TIRADS", "Genetica", "Psicologia", "Agrotoxicos", "Micropropagacao", "Biotecnologia", "Autismo", "Clonagem", "Dna", "Ecologia", "Quimica", "Alimentos"],
			maxTags: 10,
			dropdown: {
				maxItems: 20,           // <- mixumum allowed rendered suggestions
				classname: "tags-look", // <- custom classname for this dropdown, so it could be targeted
				enabled: 0,             // <- show suggestions on focus
				closeOnSelect: false    // <- do not hide the suggestions dropdown once an item has been selected
			}
		})
	$("#buscar_docente").prop('disabled', true);

});
function desabilitarBotao(){
	if ($("#text_docente").val() != '') {
		$("#buscar_docente").prop('disabled', false);
	}else{
		$("#buscar_docente").prop('disabled', true);
	}
}


function adicionarDocente(num_func,pessoa,tipo_pessoa,ativo, nome_compl,nome_abrev,nome_social,dt_nasc,sexo, est_civil,ano_ingresso, cpf, rg_tipo,rg_num,rg_dtexp, rg_uf,rg_emissor, endereco,end_num,end_compl,bairro, cidade,uf, cep,telefone, celular,email, url_particular,titulacao,atuacao_profis,trabalha_atualmente,onde_trabalha,obs,banco,agencia,conta_banco,razao_social,endemp,endemp_num,endemp_compl,endemp_bairro,endemp_municipio,endemp_cep,fone_emp,e_mail_emp,cnpj,url_profissional,preenchido) {

	$("#num_func").val(num_func);
	$("#pessoa").val(pessoa);
	$("#tipo_pessoa").val(tipo_pessoa);
	$("#ativo").val(ativo);
	$("#nome_compl").val(nome_compl);
	$("#nome_abrev").val(nome_abrev);
	$("#nome_social").val(nome_social);
	$("#dt_nasc").val(dt_nasc);
	$("#sexo").val(sexo);
	$("#est_civil").val(est_civil);
	$("#ano_ingresso").val(ano_ingresso);
	$("#cpf").val(cpf);
	$("#rg_tipo").val(rg_tipo);
	$("#rg_num").val(rg_num);
	$("#rg_dtexp").val(rg_dtexp);
	$("#rg_uf").val(rg_uf);
	$("#rg_emissor").val(rg_emissor);
	$("#endereco").val(endereco);
	$("#end_num").val(end_num);
	$("#end_compl").val(end_compl);
	$("#bairro").val(bairro);
	$("#cidade").val(cidade);
	$("#uf").val(uf);
	$("#cep").val(cep);
	$("#telefone").val(telefone);
	$("#celular").val(celular);
	$("#email").val(email);
	$("#url_particular").val(url_particular);
	$("#titulacao").val(titulacao);
	$("#atuacao_profis").val(atuacao_profis);
	$("#trabalha_atualmente").val(trabalha_atualmente);
	$("#onde_trabalha").val(onde_trabalha);
	$("#obs").val(obs);
	$("#banco").val(banco);
	$("#agencia").val(agencia);
	$("#conta_banco").val(conta_banco);
	$("#razao_social").val(razao_social);
	$("#endemp").val(endemp);
	$("#endemp_num").val(endemp_num);
	$("#endemp_compl").val(endemp_compl);
	$("#endemp_bairro").val(endemp_bairro);
	$("#endemp_municipio").val(endemp_municipio);
	$("#endemp_cep").val(endemp_cep);
	$("#fone_emp").val(fone_emp);
	$("#e_mail_emp").val(e_mail_emp);
	$("#cnpj").val(cnpj);
	$("#url_profissional").val(url_profissional);
	$("#preenchido").val(preenchido);
	
	$("#nome_compl_text").html(nome_compl);
	
	
	
	$('#modal_docente').modal('show');
}

function desativaDocente(num_func,pessoa,tipo_pessoa,ativo, nome_compl,nome_abrev,nome_social,dt_nasc,sexo, est_civil,ano_ingresso, cpf, rg_tipo,rg_num,rg_dtexp, rg_uf,rg_emissor, endereco,end_num,end_compl,bairro, cidade,uf, cep,telefone, celular,email, url_particular,titulacao,atuacao_profis,trabalha_atualmente,onde_trabalha,obs,banco,agencia,conta_banco,razao_social,endemp,endemp_num,endemp_compl,endemp_bairro,endemp_municipio,endemp_cep,fone_emp,e_mail_emp,cnpj,url_profissional,preenchido) {

	$("#num_func_desativa").val(num_func);
	$("#pessoa_desativa").val(pessoa);
	$("#tipo_pessoa_desativa").val(tipo_pessoa);
	$("#ativo_desativa").val(ativo);
	$("#nome_compl_desativa").val(nome_compl);
	$("#nome_abrev_desativa").val(nome_abrev);
	$("#nome_social_desativa").val(nome_social);
	$("#dt_nasc_desativa").val(dt_nasc);
	$("#sexo_desativa").val(sexo);
	$("#est_civil_desativa").val(est_civil);
	$("#ano_ingresso_desativa").val(ano_ingresso);
	$("#cpf_desativa").val(cpf);
	$("#rg_tipo_desativa").val(rg_tipo);
	$("#rg_num_desativa").val(rg_num);
	$("#rg_dtexp_desativa").val(rg_dtexp);
	$("#rg_uf_desativa").val(rg_uf);
	$("#rg_emissor_desativa").val(rg_emissor);
	$("#endereco_desativa").val(endereco);
	$("#end_num_desativa").val(end_num);
	$("#end_compl_desativa").val(end_compl);
	$("#bairro_desativa").val(bairro);
	$("#cidade_desativa").val(cidade);
	$("#uf_desativa").val(uf);
	$("#cep_desativa").val(cep);
	$("#telefone_desativa").val(telefone);
	$("#celular_desativa").val(celular);
	$("#email_desativa").val(email);
	$("#url_particular_desativa").val(url_particular);
	$("#titulacao_desativa").val(titulacao);
	$("#atuacao_profis_desativa").val(atuacao_profis);
	$("#trabalha_atualmente_desativa").val(trabalha_atualmente);
	$("#onde_trabalha_desativa").val(onde_trabalha);
	$("#obs_desativa").val(obs);
	$("#banco_desativa").val(banco);
	$("#agencia_desativa").val(agencia);
	$("#conta_banco_desativa").val(conta_banco);
	$("#razao_social_desativa").val(razao_social);
	$("#endemp_desativa").val(endemp);
	$("#endemp_num_desativa").val(endemp_num);
	$("#endemp_compl_desativa").val(endemp_compl);
	$("#endemp_bairro_desativa").val(endemp_bairro);
	$("#endemp_municipio_desativa").val(endemp_municipio);
	$("#endemp_cep_desativa").val(endemp_cep);
	$("#fone_emp_desativa").val(fone_emp);
	$("#e_mail_emp_desativa").val(e_mail_emp);
	$("#cnpj_desativa").val(cnpj);
	$("#url_profissional_desativa").val(url_profissional);
	$("#preenchido_desativa").val(preenchido);
	
	$("#nome_compl_desativa_text").html(nome_compl);
	
	
	
	$('#modal_desativa').modal('show');
}

function deletarDocente(nome_compl,id_docente) {
	$("#nome_compl_delete").html(nome_compl);
	$("#id_docente").val(id_docente);
	$('#modal_delete').modal('show');
}

  $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#rua").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#uf").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("...");
                        $("#bairro").val("...");
                        $("#cidade").val("...");
                        $("#uf").val("...");
                        $("#ibge").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#uf").val(dados.uf);
                                $("#ibge").val(dados.ibge);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });

            function limpa_formulário_cep_empresa() {
                // Limpa valores do formulário de cep.
                $("#rua_empresa").val("");
                $("#bairro_empresa").val("");
                $("#cidade_empresa").val("");
                $("#uf_empresa").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#cep_empresa").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep_empresa = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep_empresa != "") {

                    //Expressão regular para validar o CEP.
                    var validacep_empresa = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep_empresa.test(cep_empresa)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua_empresa").val("...");
                        $("#bairro_empresa").val("...");
                        $("#cidade_empresa").val("...");
                        $("#uf_empresa").val("...");
                        
                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep_empresa +"/json/?callback=?", function(dados_empresa) {

                            if (!("erro" in dados_empresa)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua_empresa").val(dados_empresa.logradouro);
                                $("#bairro_empresa").val(dados_empresa.bairro);
                                $("#cidade_empresa").val(dados_empresa.localidade);
                                $("#uf_empresa").val(dados_empresa.uf);
                   
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });

        	var $seuCampoCpf = $("#cpf");
        	$seuCampoCpf.mask('000.000.000-00', {reverse: true});
        	$('.date').mask('00/00/0000');
        	$('.phone').mask('(00) 00000-0000');
        	$('.cep').mask('00000-000');
        	$('.mnemonico').mask('AA_AAAA');
  			$('.cnpj').mask('00.000.000/0000-00', {reverse: true});

        });

$(document).ready(function() {
	$('#tipo_pessoa').change(function(){
		var valorEscolhido 	= $('#tipo_pessoa option:selected').val();
		if (valorEscolhido == 'PS'){
           
			$('#pessoa-fisica').closest('div').show(100);
			$('#pessoa-juridica').closest('div').hide(100);			
		}else if(valorEscolhido == 'PJ'){
			$('#pessoa-fisica').closest('div').hide(100);
			$('#pessoa-juridica').closest('div').show(100);
		}else{
			$('#pessoa-fisica').closest('div').show();
			$('#pessoa-juridica').closest('div').show();
		}
	});
	$("#docente_form").submit(function() {
		var valorEscolhido2 	= $('#tipo_pessoa option:selected').val();
    if(valorEscolhido2 == null || valorEscolhido2 == ""){
        alert('campo vazio');      
        return false;
    }
});

});

