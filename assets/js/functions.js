setTimeout(function () {
    $('#alert-publicacao').hide();
}, 6000);

$(document).ready(function(){
    
    const element =  document.querySelector('.postagens')
    element.classList.add('animated', 'fadeInUp');

});

function txtPublicacao() {	

	$("#txt-publicacao").show(1000);
	const element =  document.querySelector('#txt-publicacao')
    element.classList.add('animated', 'fadeInLeft');

    $("#btn-txt-publicacao").hide(500);
}

function cancelaTxtPublicacao() {

	$("#btn-txt-publicacao").show(1000);
	const element =  document.querySelector('#btn-txt-publicacao')
    element.classList.add('animated', 'fadeInLeft');

    $("#txt-publicacao").hide(500);
    $("#texto_publicacao").val('');
}

function txtIncorporaVideo() {

	$("#txt-incorpora").show(1000);
	const element =  document.querySelector('#txt-incorpora')
    element.classList.add('animated', 'fadeInLeft');

    $("#btn-incorpora-video").hide(500);
}

function cancelaIncorporaVideo() {

	$("#btn-incorpora-video").show();
	const element =  document.querySelector('#btn-incorpora-video')
    element.classList.add('animated', 'fadeIn');

    $("#txt-incorpora").hide(500);
    $("#incorpora_video").val('');
}