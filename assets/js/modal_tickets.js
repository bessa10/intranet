function ticketsCategory(txt_tipo, cid) {
    if(cid != '') {
        
        $("#txt_modal_tkt").html('Tickets do tipo - ' + txt_tipo);

        $("#modalTickets").modal('show');
        
        var url ='';

        url = window.location.href.replace('tickets_dev', 'list_tickets_tipo');

        console.log(url);

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                cid: cid
            },
            success : function(data) {

                //console.log(data);
                var html = '';
                var erro = '';

                if (data.length > 0) {

                    html += '<table class="table table-striped table-bordered" id="tabela-tkt-tipos">';

                    html += '<thead>';
                    html += '    <tr>';
                    html += '        <th class="text-center">Ticket</th>';
                    html += '        <th>Título</th>';
                    html += '        <th>Projeto</th>';
                    html += '        <th>Tipo</th>';
                    html += '        <th>Autor</th>';
                    html += '        <th class="text-center">Criado</th>';
                    html += '        <th class="text-center">Status</th>';
                    html += '    </tr>';
                    html += '</thead>';

                    html += '<tbody>';

                    for (var i = 0; i < data.length; i++) {

                        html += '<tr>';
                        html += '<td class="text-center"><strong>'+data[i].tid+'</strong></td>';
                        html += '<td>'+data[i].title+'</td>';
                        html += '<td>'+data[i].project+'</td>';
                        html += '<td>'+data[i].category+'</td>';
                        html += '<td>'+data[i].author+'</td>';
                        html += '<td>'+data[i].created+'</td>';
                        html += '<td class="text-center"><span class="badge badge-'+data[i].cor_status+'">'+data[i].status+'</span></td>';
                        html += '</tr>';
                    }
                    
                    html += '</tbody>';
                    html += '</table>';

                    $("#div_tickets_table").html(html);

                    $('#tabela-tkt-tipos').DataTable({
                        "language": {
                            "lengthMenu": "",
                            "zeroRecords": "Nenhum registro encontrado",
                            "info": "Mostrando página _PAGE_ de _PAGES_",
                            "infoEmpty": "Nenhum registro encontrado",
                            "infoFiltered": "(Filtrado de _MAX_ total records)",
                            "oPaginate": {
                                "sPrevious": "< Anterior",
                                "sNext": "Próximo >",
                            }
                        },
                        "searching": false,
                        "pageLength": 10,
                        "lengthMenu": false,
                        "order": [[ 0, "desc" ]]
                    });
                }
            }
        });       
    }
}

function ticketsDepartment(txt_department, id_department) {

    if(id_department != '') {
        
        $("#txt_modal_tkt").html('Tickets do departamento - ' + txt_department);

        $("#modalTickets").modal('show');
        
        var url ='';

        url = window.location.href.replace('tickets_dev', 'list_tickets_department');

        //console.log(url);

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                id_department: id_department
            },
            success : function(data) {

                //console.log(data);
                var html = '';
                var erro = '';

                if (data.length > 0) {

                    html += '<table class="table table-striped table-bordered" id="tabela-tkt-department">';

                    html += '<thead>';
                    html += '    <tr>';
                    html += '        <th class="text-center">Ticket</th>';
                    html += '        <th>Título</th>';
                    html += '        <th>Projeto</th>';
                    html += '        <th>Autor</th>';
                    html += '        <th class="text-center">Criado</th>';
                    html += '        <th class="text-center">Status</th>';
                    html += '    </tr>';
                    html += '</thead>';

                    html += '<tbody>';

                    for (var i = 0; i < data.length; i++) {

                        html += '<tr>';
                        html += '<td class="text-center"><strong>'+data[i].tid+'</strong></td>';
                        html += '<td>'+data[i].title+'</td>';
                        html += '<td>'+data[i].project+'</td>';
                        html += '<td>'+data[i].author+'</td>';
                        html += '<td>'+data[i].created+'</td>';
                        html += '<td class="text-center"><span class="badge badge-'+data[i].cor_status+'">'+data[i].status+'</span></td>';
                        html += '</tr>';
                    }
                    
                    html += '</tbody>';
                    html += '</table>';

                    $("#div_tickets_table").html(html);

                    $('#tabela-tkt-department').DataTable({
                        "language": {
                            "lengthMenu": "",
                            "zeroRecords": "Nenhum registro encontrado",
                            "info": "Mostrando página _PAGE_ de _PAGES_",
                            "infoEmpty": "Nenhum registro encontrado",
                            "infoFiltered": "(Filtrado de _MAX_ total records)",
                            "oPaginate": {
                                "sPrevious": "< Anterior",
                                "sNext": "Próximo >",
                            }
                        },
                        "searching": false,
                        "pageLength": 10,
                        "lengthMenu": false,
                        "order": [[ 0, "desc" ]]
                    });
                }
            }
        });       
    }
}

function devsEntreguesMes(mes_atual, ano_mes_atual) {

    if(ano_mes_atual != '' && mes_atual != '') {
        
        $("#txt_modal_tkt").html('Desenvolvimentos entregues no mês');

        $("#modalTickets").modal('show');
        
        var url ='';

        url = window.location.href.replace('tickets_dev', 'list_devs_entregues_mes');

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                ano_mes_atual: ano_mes_atual,
                mes_atual: mes_atual 
            },
            success : function(data) {

                var html = '';
                var erro = '';

                if (data.length > 0) {

                    html += '<table class="table table-striped table-bordered" id="tabela-tkt-department">';

                    html += '<thead>';
                    html += '    <tr>';
                    html += '        <th class="text-center">Ticket</th>';
                    html += '        <th>Título</th>';
                    html += '        <th>Projeto</th>';
                    html += '        <th>Autor</th>';
                    html += '        <th>Atendente</th>';
                    html += '        <th class="text-center">Finalizado</th>';
                    html += '    </tr>';
                    html += '</thead>';

                    html += '<tbody>';

                    for (var i = 0; i < data.length; i++) {

                        html += '<tr>';
                        html += '<td class="text-center"><strong>'+data[i].tid+'</strong></td>';
                        html += '<td>'+data[i].title+'</td>';
                        html += '<td>'+data[i].project+'</td>';
                        html += '<td>'+data[i].author+'</td>';
                        html += '<td>'+data[i].worker+'</td>';
                        html += '<td>'+data[i].completed+'</td>';
                        html += '</tr>';
                    }
                    
                    html += '</tbody>';
                    html += '</table>';

                    $("#div_tickets_table").html(html);

                    $('#tabela-tkt-department').DataTable({
                        "language": {
                            "lengthMenu": "",
                            "zeroRecords": "Nenhum registro encontrado",
                            "info": "Mostrando página _PAGE_ de _PAGES_",
                            "infoEmpty": "Nenhum registro encontrado",
                            "infoFiltered": "(Filtrado de _MAX_ total records)",
                            "oPaginate": {
                                "sPrevious": "< Anterior",
                                "sNext": "Próximo >",
                            }
                        },
                        "searching": false,
                        "pageLength": 10,
                        "lengthMenu": false,
                        "order": [[ 0, "desc" ]]
                    });
                }
            }
        });       
    }
}