$(document).ready(function(){
    $('#tabela-cursos-comp').DataTable({
        "language": {
            "lengthMenu": "",
            "zeroRecords": "Nenhum registro encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro encontrado",
            "infoFiltered": "(Filtrado de _MAX_ total records)",
            "oPaginate": {
                "sPrevious": "< Anterior",
                "sNext": "Próximo >",
            }
        },
        "searching": false,
        "pageLength": 25,
        "lengthMenu": false,
        "order": [[ 0, "asc" ]]
    });

    $('#tabela-cursos-incomp').DataTable({
        "language": {
            "lengthMenu": "",
            "zeroRecords": "Nenhum registro encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro encontrado",
            "infoFiltered": "(Filtrado de _MAX_ total records)",
            "oPaginate": {
                "sPrevious": "< Anterior",
                "sNext": "Próximo >",
            }
        },
        "searching": false,
        "pageLength": 25,
        "lengthMenu": false,
        "order": [[ 0, "asc" ]]
    });

    $('#tabela-disciplinas').DataTable({
        "language": {
            "lengthMenu": "",
            "zeroRecords": "Nenhum registro encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro encontrado",
            "infoFiltered": "(Filtrado de _MAX_ total records)",
            "oPaginate": {
                "sPrevious": "< Anterior",
                "sNext": "Próximo >",
            }
        },
        "searching": false,
        "pageLength": 25,
        "lengthMenu": false,
        "order": [[ 0, "asc" ]]
    });

    $('#tabela-docentes').DataTable({
        "language": {
            "lengthMenu": "",
            "zeroRecords": "Nenhum registro encontrado",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro encontrado",
            "infoFiltered": "(Filtrado de _MAX_ total records)",
            "oPaginate": {
                "sPrevious": "< Anterior",
                "sNext": "Próximo >",
            }
        },
        "searching": false,
        "pageLength": 15,
        "lengthMenu": false,
        "order": [[ 0, "asc" ]]
    });
});