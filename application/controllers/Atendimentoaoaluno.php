<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Atendimentoaoaluno extends CI_Controller {

	private $id_departamento;

	public function __construct() {
        parent::__construct();

        $this->id_departamento = 1;

        if(!acesso_departamento($this->id_departamento)){
        	die('acesso negado');
        }
    }

	public function index()
	{	
		$this->load->view('layout/topo');
		$this->load->view('layout/menu', array('id_departamento' => $this->id_departamento));
		$this->load->view('atendimento/index');
		$this->load->view('layout/rodape');
	}
}