<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comercial extends CI_Controller {

	private $id_departamento;

	public function __construct() {
        parent::__construct();

        $this->id_departamento = 2;

        if(!acesso_departamento($this->id_departamento)){
        	die('acesso negado');
        }
    }

	public function index()
	{	
		$this->load->view('layout/topo');
		$this->load->view('layout/menu', array('id_departamento' => $this->id_departamento));
		$this->load->view('comercial/index');
		$this->load->view('layout/rodape');
	}
}