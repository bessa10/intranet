<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function create()
    {
    	$this->load->library('ApiLyceum');
    	//$teste = $this->apilyceum->consulta_modalidade();
    	//$teste2 = $this->apilyceum->consulta_grupos();
    	//$teste3 = $this->apilyceum->consulta_organizacao();
        //$this->Roles_Model->check_permission('project', 2);
        /*
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $this->form_validation->set_rules('name', 'Nome do projeto', 'required');

            if($this->form_validation->run() != FALSE)
            {
                $form = $_POST;
                $this->Projects_Model->add_project($form);
                echo site_url('project/all');
                redirect(site_url('project/all'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Existem erros no preenchimento.');
            }
        }
        */
        $title = 'Novo Produto';
        $this->load->view('product/create', compact('title'));
    }
}
