<?php

class Usuario extends CI_Controller
{	
	private $dados_usuario;

	public function __construct() {
		parent::__construct();

		$this->dados_usuario = get_usuario();

		/*if(!$this->dados_usuario) {
		 	die('não autorizado');
		}*/

		$this->load->model('Usuario_Model');
		$this->load->library('form_validation');
	}

	public function perfil()
	{
		$usuario = $this->Usuario_Model->findUsuario($this->dados_usuario->id_usuario);

		$this->form_validation->set_rules('','','');


		if($this->form_validation->run() == false) {

			$this->load->view('layout/topo');
			$this->load->view('layout/menu');
			$this->load->view('usuario/perfil', compact('usuario'));
			$this->load->view('layout/rodape');

		} else {




		}
	}
}