<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produto extends CI_Controller
{
	public function __construct() {

        parent::__construct();

        $this->load->model('Produto_Model');
		$this->load->library('ApiLyceum');
        $this->load->library('form_validation');
    }

	public function cursos()
	{
		$cursos_incomp 		= $this->Produto_Model->cursos_incompletos();
		$cursos_completos 	= $this->apilyceum->cursos_completos();

		$this->load->view('layout/topo');
		$this->load->view('layout/menu');
		$this->load->view('produto/cursos', compact('cursos_incomp', 'cursos_completos'));
		$this->load->view('layout/rodape');
	}

	public function disciplinas()
	{
		$filtro = ($this->input->post()) ? $this->input->post() : null; 

		$disciplinas = $this->apilyceum->list_disciplinas($filtro);

		$modalidades = $this->Produto_Model->list_modalidades();

		$this->load->view('layout/topo');
		$this->load->view('layout/menu');
		$this->load->view('produto/disciplinas', compact('disciplinas', 'modalidades', 'filtro'));
		$this->load->view('layout/rodape');
	}

	public function add_curso($idCurso = null)
	{
		$user = get_usuario();

		$user_created = $user->nome;
		
		if(isset($idCurso)) {

			$curso = $this->Produto_Model->get_product($idCurso);
			
			if(!$curso){

				$this->session->set_flashdata(array('type' => 'error', 'msg' => 'Curso inválido.'));
				redirect('produto/add_curso');
			}

		} else {

			$curso = null;
		}


		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			$this->form_validation->set_rules('nome', 'Nome do curso', 'required');

			if($this->form_validation->run() != FALSE) {

				$form = $_POST;

				if($form['id_curso']){

					$nome_curso = $this->Produto_Model->get_nome_curso($form['nome']);

					if(!$nome_curso) {

						$this->Produto_Model->update_nome_curso($form['id_curso'], $form);

						$this->session->set_flashdata(array('type' => 'success', 'msg' => 'Nome do curso alterado.'));

						redirect('produto/add_docente/'.$form['id_curso']);
					
					} else {
						
						$this->session->set_flashdata(array('type' => 'error', 'msg' => 'Esse nome já foi escolhido.'));
					}
					
				} else {

					$nome_curso = $this->Produto_Model->get_nome_curso($form['nome']);
					
					if(!$nome_curso) {

						$id_curso = $this->Produto_Model->add_curso($form);

						if($id_curso) {

							$this->Produto_Model->alterar_etapa($id_curso, 2);

							$this->session->set_flashdata(array('type' => 'success', 'msg' => 'Curso cadastrado com sucesso.'));
							
							redirect('produto/add_docente/'.$id_curso);

						} else {

							$this->session->set_flashdata(array('type' => 'error', 'msg' => 'Não foi possível cadastrar o curso.'));
							
							redirect('produto/add_curso');
						}
					
					} else {

						$this->session->set_flashdata(array('type' => 'error', 'msg' => 'Esse nome já foi escolhido.'));
					}
			    }
			}
		}

		$title = 'Novo Curso';

		$this->load->view('layout/topo');
		$this->load->view('layout/menu');
		$this->load->view('curso/create', compact('title', 'user_created', 'curso'));
		$this->load->view('layout/rodape');
	}

	public function add_docente($idCurso = null)
	{
		if(!isset($idCurso)) {
			
			$this->session->set_flashdata(array('type' => 'error', 'msg' => 'Curso inválido.'));

			redirect('produto/add_curso');
		}

		$consul_modalidades 	= $this->apilyceum->consulta_modalidade();
		$consul_grupos 			= $this->apilyceum->consulta_grupos();
		$consul_organizacao 	= $this->apilyceum->consulta_organizacao();
		
		$user 				= get_usuario();
		$user_created 		= $user->nome;
		$curso 				= $this->Produto_Model->get_product($idCurso);
		$receber_docentes 	= $this->Produto_Model->get_docentes($idCurso);
		$docente 			= $this->Produto_Model->get_docente($idCurso);
		$consul_docentes 	= null;

		// Caso não existir docentes, iremos marcar na segunda etapa
		if(!$receber_docentes) {
			$this->Produto_Model->alterar_etapa($idCurso, 2);
		}


		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			$this->form_validation->set_rules('curso', 'Nome do curso', 'required');

			if ($this->input->post('nome_docente')) {

				$filtro = $this->input->post();
				$consul_docentes = $this->apilyceum->consulta_docentes($filtro);

			} else {

				$form = $_POST;
				$verificar_docente = $this->Produto_Model->verify_docente($form);

				if (!$verificar_docente) {
					$id_docentes = $this->Produto_Model->add_docentes($form);

				} else {

					$id_docentes = $verificar_docente->id_docente;
				}

				$verificar_curso_docente = $this->Produto_Model->verify_docente_curso( $idCurso, $id_docentes);

				if (!$verificar_curso_docente) {
					$add_docente_curso = $this->Produto_Model->add_curso_docente($idCurso, $id_docentes);
				}

				$this->Produto_Model->alterar_etapa($idCurso, 3);

				$this->session->set_flashdata(array('type' => 'success', 'msg' => 'Docente cadastrado com sucesso.'));

				redirect('produto/add_docente/' . $idCurso);
			}
		}

		$title = 'Novo Docente';

		$this->load->view('layout/topo');
		$this->load->view('layout/menu');
		$this->load->view('docente/create', compact('title', 'consul_modalidades', 'consul_grupos', 'consul_organizacao','user_created','curso','consul_docentes','receber_docentes','idCurso','docente'));
		$this->load->view('layout/rodape');

	}

	public function edit_docente($idCurso = null)
	{
		$this->load->model('Produto_Model');
		$this->load->library('ApiLyceum');
		$this->load->library('form_validation');
		if($this->input->post('PS')){
		$this->form_validation->set_rules('tipo_pessoa', 'Tipo do docente', 'required');
		$this->form_validation->set_rules('nome_compl', 'Nome do docente', 'required');
		$this->form_validation->set_rules('ativo', 'Docente ativo', 'required');
		$this->form_validation->set_rules('dt_nasc', 'Data de nascimento do docente', 'required');
		$this->form_validation->set_rules('est_civil', 'Estado civil do docente', 'required');
		$this->form_validation->set_rules('cpf', 'CPF do docente', 'required');
		$this->form_validation->set_rules('rg_tipo', 'Tipo de RG do docente', 'required');
		$this->form_validation->set_rules('rg_num', 'Número do docente', 'required');
		$this->form_validation->set_rules('rg_uf', 'UF do RG do docente', 'required');
		$this->form_validation->set_rules('rg_dtexp', 'Data de expedição do RG do docente', 'required');
		$this->form_validation->set_rules('rg_emissor', 'Emissor do RG do docente', 'required');
		$this->form_validation->set_rules('cep', 'CEP do docente', 'required');
		$this->form_validation->set_rules('celular', 'Celular do docente', 'required');
		$this->form_validation->set_rules('e_mail', 'Email do docente', 'required');
		}
		if(!isset($idCurso)){
			redirect('produto/add_curso');
		}

		if ($this->input->get('pessoa')) {
				$filtro2 = $this->input->get('pessoa');
				$consul_docentes = $this->apilyceum->consulta_docentes($filtro = null, $filtro2);
				

			}
		if($this->input->post()){
				
					$form = $_POST;
					$verificar_docente = $this->Produto_Model->verify_docente($form);
					if(!$verificar_docente){
						$editar_docente = $this->Produto_Model->add_docentes($form);
					}else{
						$verificar_docente_base = $this->Produto_Model->verify_docente_base($filtro2);
						$editar_docente = $this->Produto_Model->edit_docente($filtro2,$form);
					}
					$this->session->set_flashdata('success', 'Docente editado.');
					redirect('produto/add_docente/'.$idCurso);
		
			}


		$title = 'Editar Docente';
		$this->load->view('docente/edit', compact('title', 'consul_docentes','idCurso'));	


	}

	public function desativa_docente($idCurso = null)
	{
		$this->load->model('Produto_Model');
		$this->load->library('ApiLyceum');

		if(!isset($idCurso)){
			redirect('produto/add_curso');
		}

		$form = $_POST;
		$desativar_docente = $this->Produto_Model->desativa_docente($form);
		$this->session->set_flashdata('success', 'Docente desativado.');
		redirect('produto/add_docente/' . $idCurso);
	}

	public function new_docente($idCurso = null)
	{
		$this->load->model('Produto_Model');
	    $this->load->library('ApiLyceum');
	    $receber_docentes = $this->Produto_Model->get_docentes($idCurso);
		$docente = $this->Produto_Model->get_docente($idCurso);


	    $this->load->library('form_validation');
	    if($this->input->post('PS')){
		$this->form_validation->set_rules('tipo_pessoa', 'Tipo do docente', 'required');
		$this->form_validation->set_rules('nome_compl', 'Nome do docente', 'required');
		$this->form_validation->set_rules('ativo', 'Docente ativo', 'required');
		$this->form_validation->set_rules('dt_nasc', 'Data de nascimento do docente', 'required');
		$this->form_validation->set_rules('est_civil', 'Estado civil do docente', 'required');
		$this->form_validation->set_rules('cpf', 'CPF do docente', 'required');
		$this->form_validation->set_rules('rg_tipo', 'Tipo de RG do docente', 'required');
		$this->form_validation->set_rules('rg_num', 'Número do docente', 'required');
		$this->form_validation->set_rules('rg_uf', 'UF do RG do docente', 'required');
		$this->form_validation->set_rules('rg_dtexp', 'Data de expedição do RG do docente', 'required');
		$this->form_validation->set_rules('rg_emissor', 'Emissor do RG do docente', 'required');
		$this->form_validation->set_rules('cep', 'CEP do docente', 'required');
		$this->form_validation->set_rules('celular', 'Celular do docente', 'required');
		$this->form_validation->set_rules('e_mail', 'Email do docente', 'required');
		}
		if(!isset($idCurso)){
			redirect('produto/add_curso');
		}

		if($this->input->post()){
			
					$form = $_POST;
					$adicionar_docente = $this->Produto_Model->add_docentes($form);
					$this->session->set_flashdata('success', 'Docente criado.');
	
					redirect('produto/add_docente/'.$idCurso);
				

		}


		$title = 'Adicionar Docente';
		$this->load->view('docente/new_docente', compact('title','idCurso'));	


	}

	public function delete_docente($idCurso = null)
	{
		if(!isset($idCurso)){
			redirect('produto/add_curso');

			$this->load->model('Produto_Model');
		    $this->load->library('ApiLyceum');
		}

		$this->load->model('Produto_Model');
		$this->load->library('ApiLyceum');
		$consul_modalidades = $this->apilyceum->consulta_modalidade();
		$consul_grupos = $this->apilyceum->consulta_grupos();
		$consul_organizacao = $this->apilyceum->consulta_organizacao();
		$this->load->library('form_validation');
		$user = get_usuario();
		$user_created = $user->nome;
		$curso = $this->Produto_Model->get_product($idCurso);
		$receber_docentes = $this->Produto_Model->get_docentes($idCurso);
		$docente = $this->Produto_Model->get_docente($idCurso);

		$form = $_POST;
		$deletar_docente = $this->Produto_Model->delete_curso_docente($idCurso, $form);
		redirect('produto/add_docente/' . $idCurso);
	}

	public function add_categoria($idCurso = null)
	{
		if(!isset($idCurso)){
			redirect('produto/add_curso');
		}

		$this->load->model('Produto_Model');
		$this->load->library('ApiLyceum');
		$consul_modalidades = $this->apilyceum->consulta_modalidade();
		$consul_grupos = $this->apilyceum->consulta_grupos();
		$consul_organizacao = $this->apilyceum->consulta_organizacao();
		$this->load->library('form_validation');
		$user = get_usuario();
		$user_created = $user->nome;
		$curso = $this->Produto_Model->get_product($idCurso);


		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->form_validation->set_rules('mnemonico', 'mnemonico', 'required');
			if ($this->form_validation->run() != FALSE) {
				$form = $_POST;
				$verificar_mnemonico = $this->apilyceum->consulta_mnemonico($form['mnemonico']);
				$verificar_mnemonico_base = $this->Produto_Model->verify_mnemonico($form);
				if($verificar_mnemonico['dados'] == null){
					if(!$verificar_mnemonico_base){
					$add_categoria = $this->Produto_Model->add_categoria($form, $idCurso);
					$this->session->set_flashdata('success', 'Categorias inseridas.');
					redirect('produto/marketing_publico_alvo/' . $idCurso);
				}else{
					$this->session->set_flashdata('error', 'O mnemonico escolhido já está inserido no curso: '.$verificar_mnemonico_base->nome);
					redirect('produto/add_categoria/' . $idCurso);
				}

			}else{
					$this->session->set_flashdata('error', 'O mnemonico escolhido já está inserido no curso: '.$verificar_mnemonico['dados'][0]['TITULO']);
					redirect('produto/add_categoria/' . $idCurso);

				}
				
				
				
				

			}else {
				$this->session->set_flashdata('error', 'Existem erros no preenchimento.');
			}
		}

		$title = 'Novo Docente';
		$this->load->view('categoria/create', compact('title', 'consul_modalidades', 'consul_grupos', 'consul_organizacao','user_created','curso','idCurso'));
	}

	public function marketing_publico_alvo($idCurso = null)
	{
		$this->load->model('Produto_Model');
		$this->load->library('ApiLyceum');
		$consul_modalidades = $this->apilyceum->consulta_modalidade();
		$consul_grupos = $this->apilyceum->consulta_grupos();
		$consul_organizacao = $this->apilyceum->consulta_organizacao();
		$consul_especialidade = $this->apilyceum->consulta_especilidades();
		$this->load->library('form_validation');
		$user = get_usuario();
		$user_created = $user->nome;
		$curso = $this->Produto_Model->get_product($idCurso);
		$id_especialidade = $this->Produto_Model->add_especialidades();
		$especialidades = $this->Produto_Model->get_especialidades();
		$id_area_atuacao = $this->Produto_Model->get_area_atuacoes();
		$area_atuacoes = $this->Produto_Model->get_area_atuacoes();
		$nomeCurso = null;

		if(!isset($idCurso)){
			redirect('produto/add_curso');
		}

		if($_SERVER['REQUEST_METHOD'] == 'POST') {

				$form = $_POST;
				foreach($id_area_atuacao as $area_atuacao){
					$verify_area_curso = $this->Produto_Model->verify_area_atuacao_curso($area_atuacao->id_area, $idCurso);
					$gravar_area_curso = $this->Produto_Model->add_curso_area_atuacao($idCurso, $form); 
				}
				$verify_especialidade_curso = $this->Produto_Model->verify_especialidade_curso($id_especialidade, $idCurso);
				$gravar_especialidade_curso = $this->Produto_Model->add_curso_especialidade($idCurso, $form);

				if($gravar_especialidade_curso){
					$this->session->set_flashdata('success', 'especialidade inserida.');
					redirect('produto/marketing_persona/' . $idCurso);
				}else{
					$this->session->set_flashdata('error', 'especialidade não inserida.');
				}

				}



		$title = 'Publico alvo';
		$this->load->view('marketing/publico_alvo', compact('title','consul_especialidade','user_created','curso','nomeCurso','especialidades','area_atuacoes','idCurso'));
	}

	public function marketing_persona($idCurso = null)
	{
		$this->load->model('Produto_Model');
		$this->load->library('ApiLyceum');
		$consul_modalidades = $this->apilyceum->consulta_modalidade();
		$consul_grupos = $this->apilyceum->consulta_grupos();
		$consul_organizacao = $this->apilyceum->consulta_organizacao();
		$consul_especialidade = $this->apilyceum->consulta_especilidades();
		$this->load->library('form_validation');
		$user = get_usuario();
		$user_created = $user->nome;
		$curso = $this->Produto_Model->get_product($idCurso);
		$receber_personas = $this->Produto_Model->get_personas($idCurso);

		$title = null;
		$nomeCurso = null;
		if(!isset($idCurso)){
			redirect('produto/add_curso');
		}

		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$this->form_validation->set_rules('nome', 'Nome da persona', 'required');
			$this->form_validation->set_rules('especialidade', 'especialidade da persona', 'required');
			$this->form_validation->set_rules('cargo_ocupacao', 'cargo de ocupacaoe da persona', 'required');
			$this->form_validation->set_rules('onde_trabalha', 'onde trabalha a persona', 'required');

			if($this->form_validation->run() != FALSE) {
				$form = $_POST;
				$persona = $this->Produto_Model->add_persona($form);
				$verificar_curso_persona = $this->Produto_Model->verify_persona_curso($idCurso,$persona);
				if (!$verificar_curso_persona) {
					$add_persona_curso = $this->Produto_Model->add_curso_persona($idCurso,$persona);
					redirect('produto/marketing_persona/' . $idCurso);
				}
			}
				else
				{
					$this->session->set_flashdata('error', 'Existem erros no preenchimento.');
				}
			}

		$this->load->view('marketing/persona', compact('title','user_created','curso','nomeCurso','receber_personas','idCurso'));
	}

	public function relevancia($idCurso = null)
	{
		$this->load->model('Produto_Model');
		$this->load->library('ApiLyceum');
		$this->load->library('form_validation');
		$user = get_usuario();
		$user_created = $user->nome;
		$curso = $this->Produto_Model->get_product($idCurso);


		if(!isset($idCurso)){
			redirect('produto/add_curso');
		}

		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			$this->form_validation->set_rules('relevancia', 'Relevância', 'required');
			$this->form_validation->set_rules('objetivos_especificos', 'Objetivos especificos', 'required');
			$this->form_validation->set_rules('diferenciais_determinantes', 'Diferenciais determinantes', 'required');
			$this->form_validation->set_rules('concorrentes', 'Concorrentes', 'required');
			$this->form_validation->set_rules('congresso_eventos', 'Congresso eventos', 'required');

			if ($this->form_validation->run() != FALSE) {
				$form = $_POST;
				$keywords = $_POST['key_words'];
				$keyword = explode(',', $keywords);
				foreach ($keyword as $tag){
					$tag2 = explode(':', $tag);

					$arr = array('"','}',']');
					$rep = array('','');
					$add_keywords = str_replace($arr,$rep,$tag2[1]);
					$add_keyword = $this->Produto_Model->add_key_words( $idCurso,$add_keywords);

				}
				$add_relevancia = $this->Produto_Model->add_relevancia($form, $idCurso);

				//redirect('produto/marketing_persona/' . $idCurso);

			} else {
				$this->session->set_flashdata('error', 'Existem erros no preenchimento.');
			}
		}

		$this->load->view('marketing/relevancia', compact('user_created','curso','idCurso'));

	}

}
