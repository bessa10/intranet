<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiro extends CI_Controller {

	private $id_departamento;

	public function __construct() {
        parent::__construct();

        $this->id_departamento = 3;

        if(!acesso_departamento($this->id_departamento)){
        	die('acesso negado');
        }
    }

	public function index()
	{	
		$this->load->view('layout/topo');
		$this->load->view('layout/menu', array('id_departamento' => $this->id_departamento));
		$this->load->view('financeiro/index');
		$this->load->view('layout/rodape');
	}

	public function inadimplencia()
	{	
		$this->load->view('layout/topo');
		$this->load->view('layout/menu', array('id_departamento' => $this->id_departamento));
		$this->load->view('financeiro/inadimplencia');
		$this->load->view('layout/rodape');
	}


}