<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gut extends CI_Controller {

	public function index()
	{	
		$this->load->model('Gut_Model');

		$catalogos = $this->Gut_Model->list_catalogo_servicos();

		$projects = $this->Gut_Model->list_projects();

		$this->load->view('layout/topo');
		//$this->load->view('layout/menu');
		$this->load->view('gut/index', compact('catalogos', 'projects'));
		$this->load->view('layout/rodape');
	}
	public function ti()
	{
		$this->load->model('Gut_Model');

		$catalogos = $this->Gut_Model->list_catalogo_servicos_ti();

		$projects = $this->Gut_Model->list_projects_ti();

		$this->load->view('layout/topo');
		//$this->load->view('layout/menu');
		$this->load->view('gut/ti', compact('catalogos', 'projects'));
		$this->load->view('layout/rodape');
	}
	public function facilities()
	{
		$this->load->model('Gut_Model');

		$catalogos = $this->Gut_Model->list_catalogo_servicos_facilities();

		$projects = $this->Gut_Model->list_projects_facilities();

		$this->load->view('layout/topo');
		//$this->load->view('layout/menu');
		$this->load->view('gut/facilities', compact('catalogos', 'projects'));
		$this->load->view('layout/rodape');
	}
}
