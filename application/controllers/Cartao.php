<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cartao extends CI_Controller {

	private $id_departamento;

	public function __construct() {

        parent::__construct();

        $this->load->model('Card_Model');
        $this->load->library('form_validation');
    }

	public function index()
	{

	$user = get_usuario();

	if($_SERVER['REQUEST_METHOD'] == 'POST') {

	$this->form_validation->set_rules('nome_destinatario', 'Nome destinatario', 'required');
	//$this->form_validation->set_rules('email_destinatario', 'E-mail destinatario', 'required');
	$this->form_validation->set_rules('mensagem_personalizada', 'Mensagem personalizada', 'required');
	$this->form_validation->set_rules('modelo_cartao', 'Modelo cartão', 'required');

			if($this->form_validation->run() != FALSE) {

				$form = $_POST;
				$form['nome_remetente'] = $user->nome;
				$form['email_remetente'] = $user->email;
				
					if($form) {

						$id_cartao = $this->Card_Model->add_cartao($form);

						if($id_cartao) {
							
							//$this->session->set_flashdata(array('type' => 'success', 'msg' => 'Card.'));
							
							redirect('cartao/enviar_card/'.$id_cartao);

						} else {

							$this->session->set_flashdata(array('type' => 'error', 'msg' => 'Não foi possível adicionar o cartão.'));
							
							redirect('card/index');
						}
					
					}
			    }
			}

		$title = 'Criar card';
	
		$this->load->view('layout/topo');
		$this->load->view('layout/menu', array('id_departamento' => $this->id_departamento));
		$this->load->view('card/index', compact('title'));
		$this->load->view('layout/rodape');
	}

	public function enviar_card($id_cartao = null)
	{
		if(!isset($id_cartao)) {
			
			$this->session->set_flashdata(array('type' => 'error', 'msg' => 'Card inválido.'));

			redirect('card/index');
		}

		$dados_card = $this->Card_Model->get_cartao($id_cartao);

		$title = 'Enviar card';

		$this->load->view('layout/topo');
		$this->load->view('layout/menu', array('id_departamento' => $this->id_departamento));
		$this->load->view('card/enviar_card', compact('title','dados_card'));
		$this->load->view('layout/rodape');


	}

}