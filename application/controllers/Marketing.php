<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marketing extends CI_Controller {

	private $id_departamento;

	public function __construct() {
        parent::__construct();

        $this->id_departamento = 5;

        if(!acesso_departamento($this->id_departamento)){
        	die('acesso negado');
        }
    }

	public function index()
	{	
		$this->load->view('layout/topo');
		$this->load->view('layout/menu', array('id_departamento' => $this->id_departamento));
		$this->load->view('marketing/index');
		$this->load->view('layout/rodape');
	}

	public function evolucao_matriculas()
	{
		$this->load->view('layout/topo');
		$this->load->view('layout/menu', array('id_departamento' => $this->id_departamento));
		$this->load->view('marketing/evolucao_matriculas');
		$this->load->view('layout/rodape');
	}
}