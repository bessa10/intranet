<?php

class Postagem extends CI_Controller
{
	private $dados_usuario;

	public function __construct() {
		parent::__construct();

		$this->dados_usuario = get_usuario();

		/*if(!$this->dados_usuario) {
		 	die('não autorizado');
		}*/

		$this->load->model('Postagem_Model');
		$this->load->library('form_validation');
	}

	public function aguardando()
	{
		$postagens_aguardando = $this->Postagem_Model->postagens_aguardando();

		$this->load->view('layout/topo');
		$this->load->view('layout/menu');
		$this->load->view('postagem/aguardando', compact('postagens_aguardando'));
		$this->load->view('layout/rodape');
	}

	public function aprovar_reprovar()
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			$tipo = $this->input->post('tipo');

			$id_postagem = $this->input->post('id_postagem');

			if($tipo == 1) {
				// Aprovar

				$dados['aprovado']				= 1;
				$dados['dth_aprovado']			= date('Y-m-d H:i:s');
				$dados['cod_usuario_aprovado']	= $this->dados_usuario->id_usuario;

				$this->Postagem_Model->alterar_postagem($dados, $id_postagem);

				$type = 'success';
				$msg = 'Postagem aprovada com sucesso.';

			} elseif ($tipo == 2) {
				// Reprovar

				$dados['excluido']				= 1;
				$dados['dth_excluido']			= date('Y-m-d H:i:s');
				$dados['cod_usuario_excluido']	= $this->dados_usuario->id_usuario;

				$this->Postagem_Model->alterar_postagem($dados, $id_postagem);

				$type = 'success';
				$msg = 'Postagem reprovada com sucesso.';

			} else {

				$type = 'danger';
				$msg = 'Operação inválida, por favor tente novamente.';
			}

			$this->session->set_flashdata(array(
				'type'	=> $type,
				'msg'	=> $msg
			));
		
			redirect('postagem/aguardando');
		}	
	}
}