<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistemas extends CI_Controller {

	private $id_departamento;

	public function __construct() {
        parent::__construct();

        $this->id_departamento = 4;

        $this->load->model('Sistemas_Model');
        $this->load->model('Log_Acesso_Model');
    }

	public function index()
	{	
		$this->load->view('layout/topo');
		$this->load->view('layout/menu', array('id_departamento' => $this->id_departamento));
		$this->load->view('sistemas/index');
		$this->load->view('layout/rodape');
	}

	public function equipe()
	{	
		if(!acesso_departamento($this->id_departamento)){
        	redirect(base_url());
        }

		$id_departamento = $this->id_departamento;

		$datas_select = $this->Sistemas_Model->list_mes_ano_filtro();

		$filtro = null;

		if($this->input->post('mes_ano')) {

			$filtro['mes_ano'] = $this->input->post('mes_ano');

			$dados_data = ano_mes_passado_atual($filtro);
		}

		$dados_data = ano_mes_passado_atual($filtro);

		$mes_atual 			= $dados_data['mes_atual'];
		$ano_mes_atual 		= $dados_data['ano_mes_atual'];

		$mes_passado 		= $dados_data['mes_passado'];
		$ano_mes_passado 	= $dados_data['ano_mes_passado'];

		/*
		$mes_atual = 11;
		$ano_mes_atual = 2020;

		$mes_passado = 10;
		$ano_mes_passado = 2020;
		*/

		$funcoes = array(
			array(
				'id_func'	=> 1,
				'nome_func'	=> 'DEV WEB'
			),
			array(
				'id_func'	=> 2,
				'nome_func'	=> 'DEV FULL'
			),
			array(
				'id_func'	=> 3,
				'nome_func'	=> 'SUPPORT'
			),
			/*array(
				'id_func'	=> 4,
				'nome_func'	=> 'SYS HEAD'
			),*/
			array(
				'id_func'	=> 5,
				'nome_func'	=> 'DESIGN WEB'
			),
			array(
				'id_func'	=> 6,
				'nome_func'	=> 'SAP SPEC'
			),
		);

		//$list_dash_class = $this->Sistemas_Model->list_dash_class();

		$dados_geral = array();

		// ma (mês atual)
		// mp (mês passado)

		foreach($funcoes as $key => $value) {

			//echo $value['id_func'] . '<br>' . $value['nome_func'] . '<hr>';
			$dados = array();
			$tabela_dash = array();

			$funcao = $value['nome_func'];

			$dados['funcao'] 	= $funcao;
			$dados['id_func'] 	= $value['id_func'];

			$dados['total_worker_tkt_ma'] = $this->Sistemas_Model->total_worker_tickets_mes($mes_atual,$ano_mes_atual,$funcao);
			$dados['total_worker_tkt_mp'] = $this->Sistemas_Model->total_worker_tickets_mes($mes_passado,$ano_mes_passado,$funcao);

			$total_worker_ma = $dados['total_worker_tkt_ma']->total_worker;
			$total_worker_mp = $dados['total_worker_tkt_mp']->total_worker;

			$soma_valor_hora_ma = $dados['total_worker_tkt_ma']->soma_valor_hora;
			$soma_valor_hora_mp = $dados['total_worker_tkt_mp']->soma_valor_hora;

			// caso o filtro selecionado for igual ao mês atual, iremos pegar até o dia atual
			if($mes_atual == date('m') && $ano_mes_atual == date('Y')) {

				// dados até o dia atual
				$dados_horas_ma = $this->Sistemas_Model->total_horas_mes_atual();
				
			} else {

				$dados_horas_ma = $this->Sistemas_Model->total_horas_mes($mes_atual, $ano_mes_atual);
			}	
			
			$dados_horas_mp = $this->Sistemas_Model->total_horas_mes($mes_passado,$ano_mes_passado);

			$total_horas_ma = $dados_horas_ma->total * $total_worker_ma;
			$total_horas_mp = $dados_horas_mp->total * $total_worker_mp;

			//echo 'total_horas_mp - ' . $total_horas_mp . '<br>';
			//echo 'total_horas_ma - ' . $total_horas_ma . '<br>';
			//die;

			$dados['total_horas_ma'] = $total_horas_ma;
			$dados['total_horas_mp'] = $total_horas_mp;

			$dados['total_valor_full_ma'] = $dados_horas_ma->total * round($soma_valor_hora_ma);
			$dados['total_valor_full_mp'] = $dados_horas_mp->total * round($soma_valor_hora_mp);

			$dados['total_horas_prod_ma'] = 0;
			$dados['total_horas_prod_mp'] = 0;

			$dados['total_valor_prod_ma'] = 0;
			$dados['total_valor_prod_mp'] = 0;

			// Definindo variável improdutividade
			$horas_imp_mp = $total_horas_mp;
			$horas_imp_ma = $total_horas_ma;

			$porcentagem_total_ma = 0;

			$list_dash_class = $this->Sistemas_Model->list_dash_class($mes_atual, $ano_mes_atual, $funcao);

			//var_dump($list_dash_class);die;

			// Montando a tabela com dash class
			foreach ($list_dash_class as $row_dash) {

				$dados_tabela = array();

				$valor_dash_ma = $this->Sistemas_Model->valor_dash_class($mes_atual,$ano_mes_atual,$row_dash->id_dash_class,$funcao);

				$valor_dash_mp = $this->Sistemas_Model->valor_dash_class($mes_passado,$ano_mes_passado,$row_dash->id_dash_class,$funcao);

				$dados_tabela['id_dash_class']		= $row_dash->id_dash_class;
				$dados_tabela['nome_dash_class']	= $row_dash->nome_dash_class;
				$dados_tabela['ordem']				= $row_dash->nome_dash_class;

				// Valores mês atual
				if($valor_dash_ma) {

					$dados_tabela['dash_hs_ma']		= convert_min_hr($valor_dash_ma->min);
					$dados_tabela['dash_valor_ma']	= convert_min_hr($valor_dash_ma->min) * $soma_valor_hora_ma;

				} else {

					$dados_tabela['dash_hs_ma']		= 0;
					$dados_tabela['dash_valor_ma']	= 0;
				}

				// Valores mês passado
				if($valor_dash_mp) {

					$dados_tabela['dash_hs_mp']		= convert_min_hr($valor_dash_mp->min);
					$dados_tabela['dash_valor_mp']	= convert_min_hr($valor_dash_mp->min) * $soma_valor_hora_mp;

				} else {

					$dados_tabela['dash_hs_mp']		= 0;
					$dados_tabela['dash_valor_mp']	= 0;
				}

				// Calculando porcentagem por linha 
				if($dados_tabela['dash_hs_ma'] > 0) {

					$dados_tabela['porc_hs_ma']	= ($dados_tabela['dash_hs_ma'] * 100) / $dados['total_horas_ma'];

				} else {

					$dados_tabela['porc_hs_ma'] = 0;
				}

				if($dados_tabela['dash_hs_mp'] > 0) {

					$dados_tabela['porc_hs_mp']	= ($dados_tabela['dash_hs_mp'] * 100) / $dados['total_horas_mp'];

				} else {

					$dados_tabela['porc_hs_mp'] = 0;
				}

				// Verificando produtividade
				$produtividade = $this->Sistemas_Model->retorna_produtividade($funcao, $row_dash->id_dash_class);

				if($produtividade) {

					$dados_tabela['produtivo'] = $produtividade->produtivo;

					if($produtividade->produtivo == 1) {

						// somando porcentagem geral do mês atual
						$porcentagem_total_ma += $dados_tabela['porc_hs_ma'];

						// somando valores totais de hora e valor
						$dados['total_horas_prod_ma'] += $dados_tabela['dash_hs_ma'];
						$dados['total_horas_prod_mp'] += $dados_tabela['dash_hs_mp'];

						if($dados_tabela['dash_valor_ma'] > 0) {

							$dados['total_valor_prod_ma'] += ($dados_tabela['dash_valor_ma'] / $total_worker_ma);

						} else {

							$dados['total_valor_prod_ma'] += 0;
						}

						if($dados_tabela['dash_valor_mp'] > 0) {

							$dados['total_valor_prod_mp'] += ($dados_tabela['dash_valor_mp'] / $total_worker_mp);

						} else {

							$dados['total_valor_prod_mp'] += 0;
						}
					}

				} else {

					$dados_tabela['produtivo'] = 0;
				}

				// Subtraindo valores para pegar improdutividade
				$horas_imp_mp -= $dados_tabela['dash_hs_mp'];
				$horas_imp_ma -= $dados_tabela['dash_hs_ma'];

				$tabela_dash[] = $dados_tabela;
			}

			$dados['tabela_dash'] = $tabela_dash;

			$dados['porcentagem_total_ma'] = $porcentagem_total_ma;

			// horas improdutivas
			$dados['horas_imp_mp']	= ($horas_imp_mp > 0) ? $horas_imp_mp : 0;
			$dados['horas_imp_ma']	= ($horas_imp_ma > 0) ? $horas_imp_ma : 0;

			// valores improdutivos
			if($horas_imp_mp > 0) {
				
				$dados['valor_imp_mp']	= ($horas_imp_mp * $soma_valor_hora_mp) / $total_worker_mp;
			
			} else {

				$dados['valor_imp_mp'] = 0;
			}

			if($horas_imp_ma > 0) {
				
				$dados['valor_imp_ma']	= ($horas_imp_ma * $soma_valor_hora_ma) / $total_worker_ma;

			} else {

				$dados['valor_imp_ma'] = 0;
			}

			// porcentagem improdutiva
			if($horas_imp_mp > 0) {
				
				$dados['porc_imp_mp']	= ($horas_imp_mp * 100) / $dados['total_horas_mp'];

			} else {

				$dados['porc_imp_mp'] = 0;
			}

			if($horas_imp_ma > 0) {

				$dados['porc_imp_ma']	= ($horas_imp_ma * 100) / $dados['total_horas_ma'];
			
			} else {

				$dados['porc_imp_ma'] = 0;
			}

			// Calculando porcentagem geral do last month
			$dados['porc_geral_mp'] = ($dados['total_horas_prod_mp'] * 100) / $total_horas_mp;

			$dados_geral[] = $dados;
		}

		## TOTAL GERAL ##
		$total_tickets_abertos 		= $this->Sistemas_Model->tickets_worker_abertos();
		$total_tickets_progress		= $this->Sistemas_Model->tickets_worker_progress();
		$total_devs_progress		= $this->Sistemas_Model->tickets_worker_progress(null, true);
		$total_tickets_atrasados	= $this->Sistemas_Model->tickets_atrasados();
		$total_fechados_mes			= $this->Sistemas_Model->tickets_entregues_mes($mes_atual, $ano_mes_atual);

		$dados_totais['total_tickets_abertos']		= $total_tickets_abertos->total;
		$dados_totais['total_tickets_progress']		= $total_tickets_progress->total;
		$dados_totais['total_devs_progress']		= $total_devs_progress->total;
		$dados_totais['total_tickets_atrasados']	= $total_tickets_atrasados->total;
		$dados_totais['total_fechados_mes']			= $total_fechados_mes->total;

		$this->load->view('layout/topo');
		$this->load->view('layout/menu', compact('id_departamento', 'datas_select', 'filtro'));
		$this->load->view('sistemas/equipe', compact('dados_geral'));
		$this->load->view('sistemas/total_tickets', compact('dados_totais'));
		$this->load->view('layout/rodape');
	}

	public function tickets_dev()
	{	
		$dados 		= array();
		$tabela_dep = array();

		$id_departamento = $this->id_departamento;

		$datas_select = $this->Sistemas_Model->list_mes_ano_filtro();

		$filtro = null;

		if($this->input->post('mes_ano')) {

			$filtro['mes_ano'] = $this->input->post('mes_ano');

			$dados_data = ano_mes_passado_atual($filtro);
		}

		$dados_data = ano_mes_passado_atual($filtro);

		// ma (mês atual)
		$mes_atual 			= $dados_data['mes_atual'];
		$ano_mes_atual 		= $dados_data['ano_mes_atual'];
		// mp (mês passado)
		$mes_passado 		= $dados_data['mes_passado'];
		$ano_mes_passado 	= $dados_data['ano_mes_passado'];

		/*
		$mes_atual = 11;
		$ano_mes_atual = 2020;

		$mes_passado = 10;
		$ano_mes_passado = 2020;
		*/

		$list_departments = $this->Sistemas_Model->list_departments($mes_atual, $ano_mes_atual);

		$total_tkt_dep_ma = 0;
		$total_tkt_dep_mp = 0;

		$total_hs_dep_ma = 0;
		$total_hs_dep_mp = 0;

		$total_valor_dep_ma = 0;
		$total_valor_dep_mp = 0;

		foreach($list_departments as $row_dep) {

			// Pegando os totais de minutos e tickets por departamento
			$dados_deps_ma = $this->Sistemas_Model->total_tickets_departments($mes_atual, $ano_mes_atual, $row_dep->id_department);
			$dados_deps_mp = $this->Sistemas_Model->total_tickets_departments($mes_passado, $ano_mes_passado, $row_dep->id_department);

			$dados_valor_ma = $this->Sistemas_Model->valor_tickets_departamento($mes_atual,$ano_mes_atual,$row_dep->id_department);
			$dados_valor_mp = $this->Sistemas_Model->valor_tickets_departamento($mes_passado,$ano_mes_passado,$row_dep->id_department);

			$soma_valor_hora_ma = 0;
			$soma_valor_hora_mp = 0;

			$dados_tabela['id_department']	= $row_dep->id_department;
			$dados_tabela['sigla_dep']		= $row_dep->sigla_dep;
			$dados_tabela['nome_dep']		= $row_dep->name_dep;
			
			// Mês atual
			if($dados_deps_ma) {

				$dados_tabela['tickets_ma']	= $dados_deps_ma->total_tickets;
				$dados_tabela['hs_ma']		= convert_min_hr($dados_deps_ma->min);
				$dados_tabela['valor_ma']	= $dados_valor_ma->valor_hora;

			} else {

				$dados_tabela['tickets_ma']	= 0;
				$dados_tabela['hs_ma']		= 0;
				$dados_tabela['valor_ma']	= 0;
			}

			// Mês passado
			if($dados_deps_mp) {

				$dados_tabela['tickets_mp']	= $dados_deps_mp->total_tickets;
				$dados_tabela['hs_mp']		= convert_min_hr($dados_deps_mp->min);
				$dados_tabela['valor_mp']	= $dados_valor_mp->valor_hora;

			} else {

				$dados_tabela['tickets_mp']	= 0;
				$dados_tabela['hs_mp']		= 0;
				$dados_tabela['valor_mp']	= 0;
			}

			// Somanto totais
			$total_tkt_dep_ma += $dados_tabela['tickets_ma'];
			$total_tkt_dep_mp += $dados_tabela['tickets_mp'];

			$total_hs_dep_ma += $dados_tabela['hs_ma'];
			$total_hs_dep_mp += $dados_tabela['hs_mp'];

			$total_valor_dep_ma += $dados_tabela['valor_ma'];
			$total_valor_dep_mp += $dados_tabela['valor_mp'];

			$tabela_dep[] = $dados_tabela;
		}

		// Pegando dados de pessoas sem departamento cadastrado

		// Pegando os totais de minutos e tickets dos departamentos null
		$dados_deps_out_ma = $this->Sistemas_Model->total_tickets_departments($mes_atual, $ano_mes_atual, null);
		$dados_deps_out_mp = $this->Sistemas_Model->total_tickets_departments($mes_passado, $ano_mes_passado, null);

		$dados_workers_ma = $this->Sistemas_Model->list_workers_tickets_departamento($mes_atual, $ano_mes_atual, null);
		$dados_workers_mp = $this->Sistemas_Model->list_workers_tickets_departamento($mes_passado, $ano_mes_passado, null);

		$dados_valor_ma = $this->Sistemas_Model->valor_tickets_departamento($mes_atual, $ano_mes_atual, null);
		$dados_valor_mp = $this->Sistemas_Model->valor_tickets_departamento($mes_passado, $ano_mes_passado, null);

		$soma_valor_hora_ma = 0;
		$soma_valor_hora_mp = 0;

		$dados_tabela['id_department']	= null;
		$dados_tabela['sigla_dep']		= 'OUT';
		$dados_tabela['nome_dep']		= 'Outros';

		if($dados_deps_out_ma) {

			$dados_tabela['tickets_ma']	= $dados_deps_out_ma->total_tickets;
			$dados_tabela['hs_ma']		= convert_min_hr($dados_deps_out_ma->min);
			$dados_tabela['valor_ma']	= $dados_valor_ma->valor_hora;

		} else {

			$dados_tabela['tickets_ma']	= 0;
			$dados_tabela['hs_ma']		= 0;
			$dados_tabela['valor_ma']	= 0;
		}


		if($dados_deps_out_mp) {

			$dados_tabela['tickets_mp']	= $dados_deps_out_mp->total_tickets;
			$dados_tabela['hs_mp']		= convert_min_hr($dados_deps_out_mp->min);
			$dados_tabela['valor_mp']	= $dados_valor_mp->valor_hora;

		} else {
			
			$dados_tabela['tickets_mp']	= 0;
			$dados_tabela['hs_mp']		= 0;
			$dados_tabela['valor_mp']	= 0;
		}

		$total_tkt_dep_ma += $dados_tabela['tickets_ma'];
		$total_tkt_dep_mp += $dados_tabela['tickets_mp'];

		$total_hs_dep_ma += $dados_tabela['hs_ma'];
		$total_hs_dep_mp += $dados_tabela['hs_mp'];

		$total_valor_dep_ma += $dados_tabela['valor_ma'];
		$total_valor_dep_mp += $dados_tabela['valor_mp'];

		$tabela_dep[] = $dados_tabela;

		//var_dump($tabela_dep);die;

		$dados['tabela_dep'] = $tabela_dep;

		$dados['total_tkt_dep_ma'] = $total_tkt_dep_ma;
		$dados['total_tkt_dep_mp'] = $total_tkt_dep_mp;

		$dados['total_hs_dep_ma'] = $total_hs_dep_ma;
		$dados['total_hs_dep_mp'] = $total_hs_dep_mp;

		$dados['total_valor_dep_ma'] = $total_valor_dep_ma;
		$dados['total_valor_dep_mp'] = $total_valor_dep_mp;

		// Montando tabela com os top 5 departamentos com mais horas e valores
		$top_departments = $this->Sistemas_Model->list_top_departments($mes_atual, $ano_mes_atual);

		$tabela_top_dep = array();

		$valor_total_out 	= 0;
		$hs_total_out 		= 0;

		for ($i=0; $i < count($top_departments); $i++) { 
			
			$dados_tabela = array();

			$dados_valor = $this->Sistemas_Model->valor_tickets_departamento($mes_atual, $ano_mes_atual, $top_departments[$i]->id_department);

			if($i < 5) {

				$dados_tabela['sigla_dep']		= $top_departments[$i]->sigla_dep;
				$dados_tabela['name_dep']		= $top_departments[$i]->name_dep;
				$dados_tabela['hs_total']		= $top_departments[$i]->hs;
				$dados_tabela['valor_total']	= $dados_valor->valor_hora;

				$tabela_top_dep[] = $dados_tabela;
			
			} else {
				/* somar os valores de outros departamentos para ficar agrupado */
				$valor_total_out 	+= $dados_valor->valor_hora;
				$hs_total_out		+= $top_departments[$i]->hs;
			}
		}

		/* passando os dados de outros departamentos para a tabela */
		$dados_tabela['sigla_dep'] 		= '...';
		$dados_tabela['name_dep']		= 'Outros departamentos';
		$dados_tabela['valor_total']	= $valor_total_out;
		$dados_tabela['hs_total']		= $hs_total_out;

		$tabela_top_dep[] = $dados_tabela;

		$dados['tabela_top_dep'] = $tabela_top_dep;

		// Montando tabela com os top 5 projetos com mais horas e valores
		$top_projects = $this->Sistemas_Model->list_top_projects($mes_atual, $ano_mes_atual);

		$tabela_top_prj = array();

		$valor_total_out	= 0;
		$total_valor_prj 	= 0;
		$hs_total_prj_out	= 0;
		$total_hs_prj		= 0;

		for ($i=0; $i < count($top_projects); $i++) { 
			
			$dados_tabela = array();

			$dados_valor = $this->Sistemas_Model->valor_tickets_projects($mes_atual, $ano_mes_atual, $top_projects[$i]->pid);

			$total_valor_prj 	+= $dados_valor->valor_hora;

			$total_hs_prj		+= convert_min_hr($top_projects[$i]->MIN);

			if($i < 5) {

				$dados_tabela['sigla_project']		= $top_projects[$i]->sigla_project;
				$dados_tabela['name_project']		= $top_projects[$i]->name_project;
				$dados_tabela['valor_total']		= $dados_valor->valor_hora;
				$dados_tabela['hs_total']			= convert_min_hr($top_projects[$i]->MIN);

				$tabela_top_prj[] = $dados_tabela;
			
			} else {
				/* somar os valores de outros departamentos para ficar agrupado */
				$valor_total_out 	+= $dados_valor->valor_hora;

				$hs_total_prj_out 	+= convert_min_hr($top_projects[$i]->MIN);
			}
		}

		//echo $total_valor_prj; die;

		/* passando os dados de outros projetos para a tabela */
		$dados_tabela['sigla_project'] 		= '...';
		$dados_tabela['name_project']		= 'Outros sistemas';
		$dados_tabela['valor_total']		= $valor_total_out;
		$dados_tabela['hs_total']			= $hs_total_prj_out;

		$tabela_top_prj[] = $dados_tabela;

		$dados['tabela_top_prj'] 	= $tabela_top_prj;
		$dados['total_valor_prj']	= $total_valor_prj;
		$dados['total_hs_prj']		= $total_hs_prj;

		// tabela com os tickets por categoria
		$list_categories = $this->Sistemas_Model->list_categories($mes_atual, $ano_mes_atual);

		$tabela_cat = array();

		foreach($list_categories as $row_cat) {

			// Pegando os totais de minutos e tickets por departamento
			$dados_cat_ma = $this->Sistemas_Model->total_tickets_categories($mes_atual, $ano_mes_atual, $row_cat->cid);
			$dados_cat_mp = $this->Sistemas_Model->total_tickets_categories($mes_passado, $ano_mes_passado, $row_cat->cid);

			$dados_valor_ma = $this->Sistemas_Model->valor_tickets_categories($mes_atual, $ano_mes_atual, $row_cat->cid);
			$dados_valor_mp = $this->Sistemas_Model->valor_tickets_categories($mes_passado, $ano_mes_passado, $row_cat->cid);

			$soma_valor_hora_ma = 0;
			$soma_valor_hora_mp = 0;

			$dados_tabela['cid']		= $row_cat->cid;
			$dados_tabela['name']		= $row_cat->name;
			$dados_tabela['name_abrev']	= $row_cat->name_abrev;
			
			// Mês atual
			if($dados_cat_ma) {

				$dados_tabela['tickets_ma']	= $dados_cat_ma->total_tickets;
				$dados_tabela['hs_ma']		= convert_min_hr($dados_cat_ma->min);
				$dados_tabela['valor_ma']	= $dados_valor_ma->valor_hora;

			} else {

				$dados_tabela['tickets_ma']	= 0;
				$dados_tabela['hs_ma']		= 0;
				$dados_tabela['valor_ma']	= 0;
			}

			// Mês passado
			if($dados_cat_mp) {

				$dados_tabela['tickets_mp']	= $dados_cat_mp->total_tickets;
				$dados_tabela['hs_mp']		= convert_min_hr($dados_cat_mp->min);
				$dados_tabela['valor_mp']	= $dados_valor_mp->valor_hora;

			} else {

				$dados_tabela['tickets_mp']	= 0;
				$dados_tabela['hs_mp']		= 0;
				$dados_tabela['valor_mp']	= 0;
			}

			$tabela_cat[] = $dados_tabela;

		}

		$dados['tabela_cat'] = $tabela_cat;

		// TICKETS DEV	
		$projetcs_dev_ma = $this->Sistemas_Model->list_top_projects($mes_atual, $ano_mes_atual, true);

		$projetcs_dev_mp = $this->Sistemas_Model->list_top_projects($mes_passado, $ano_mes_passado, true);

		// somando totais do mês passado
		$total_valor_dev_mp	= 0;
		$total_horas_dev_mp	= 0;

		foreach ($projetcs_dev_mp as $row_dev_mp) {

			$dados_valor = $this->Sistemas_Model->valor_tickets_projects($mes_passado, $ano_mes_passado, $row_dev_mp->pid, true);

			$total_valor_dev_mp	+= $dados_valor->valor_hora;
			$total_horas_dev_mp	+= convert_min_hr($row_dev_mp->MIN);
		}

		$dados['total_valor_dev_mp']	= $total_valor_dev_mp;
		$dados['total_horas_dev_mp']	= $total_horas_dev_mp;

		$tabela_prj_dev = array();

		$total_valor_dev_ma	= 0;
		$total_horas_dev_ma	= 0;

		foreach ($projetcs_dev_ma as $row_dev) {
		
			$dados_tabela = array();

			$dados_tabela['pid']			= $row_dev->pid;
			$dados_tabela['name_project']	= $row_dev->name_project;
			$dados_tabela['sigla_project']	= $row_dev->sigla_project; 
			$dados_tabela['hs']				= convert_min_hr($row_dev->MIN);

			$dados_valor = $this->Sistemas_Model->valor_tickets_projects($mes_atual, $ano_mes_atual, $row_dev->pid, true);

			$dados_tabela['valor_hora']		= $dados_valor->valor_hora;

			$tabela_prj_dev[] = $dados_tabela;

			// somando totais
			$total_valor_dev_ma	+= $dados_valor->valor_hora;
			$total_horas_dev_ma	+= $dados_tabela['hs']; 
		}

		$dados['tabela_prj_dev'] 		= $tabela_prj_dev;
		$dados['total_valor_dev_ma']	= $total_valor_dev_ma;
		$dados['total_horas_dev_ma']	= $total_horas_dev_ma;

		// Tickets finaliados no mês
		$devs_entregues_ma = $this->Sistemas_Model->tickets_entregues_mes($mes_atual, $ano_mes_atual, 1);

		$devs_entregues_mp = $this->Sistemas_Model->tickets_entregues_mes($mes_passado, $ano_mes_passado, 1);

		$dados['devs_entregues_ma']	= $devs_entregues_ma->total;
		$dados['devs_entregues_mp'] = $devs_entregues_mp->total;

		// Tickets por status
		$dados['list_tickets_status'] = $this->Sistemas_Model->list_tickets_status();

		// Tickets em dev com status Na fila ou Em atendimento
		$list_tickets_dev_fila = $this->Sistemas_Model->list_tickets_dev_fila();


		$tabela_tkt_dev_fila = array();
		$total_valor_tkt_dev = 0;

		$total_horas_tkt_dev = 0;

		foreach ($list_tickets_dev_fila as $row_tkt) {
			
			$dados_tabela = array();

			$dados_valor = $this->Sistemas_Model->valor_tickets_dev_fila($row_tkt->pid);

			$dados_tabela['pid']			= $row_tkt->pid;
			$dados_tabela['name_project']	= $row_tkt->name_project;
			$dados_tabela['sigla_project']	= $row_tkt->sigla_project;
			$dados_tabela['hs']				= $row_tkt->hs;
			$dados_tabela['valor_total']	= $dados_valor->valor_total;

			$total_valor_tkt_dev += $dados_valor->valor_total;

			$total_horas_tkt_dev += $row_tkt->hs;

			$tabela_tkt_dev_fila[] = $dados_tabela;
		}

		## MONTANDO DADOS PARA O GRÁFICO ##
		$name_prj_graf 	= '';
		$sigla_prj_graf = '';
		$valor_dev_graf = '';

		foreach ($tabela_tkt_dev_fila as $row_dev_fila) {
		
			$name_prj_graf 	.= ",'".$row_dev_fila['name_project']."'<br>";
			$sigla_prj_graf .= ",'".$row_dev_fila['sigla_project'] . "'";
			$valor_dev_graf .= ",".formata_preco_dash($row_dev_fila['valor_total']);
		}

		$name_prj_graf 	= substr($name_prj_graf, 1);
		$sigla_prj_graf = substr($sigla_prj_graf, 1);
		$valor_dev_graf = substr($valor_dev_graf, 1);	

		//echo $sigla_prj_graf . '<br>' . $valor_dev_graf; die;

		$dados['name_prj_graf']		= $name_prj_graf ;
		$dados['sigla_prj_graf']	= $sigla_prj_graf;
		$dados['valor_dev_graf']	= $valor_dev_graf;

		//var_dump($tabela_tkt_dev_fila);die;
		$dados['tabela_tkt_dev_fila'] 	= $tabela_tkt_dev_fila;
		$dados['total_valor_tkt_dev']	= $total_valor_tkt_dev;
		$dados['total_horas_tkt_dev']	= $total_horas_tkt_dev;

		## TOTAL GERAL ##
		$total_tickets_abertos 		= $this->Sistemas_Model->tickets_worker_abertos();
		$total_tickets_progress		= $this->Sistemas_Model->tickets_worker_progress();
		$total_devs_progress		= $this->Sistemas_Model->tickets_worker_progress(null, true);
		$total_tickets_atrasados	= $this->Sistemas_Model->tickets_atrasados();
		$total_fechados_mes			= $this->Sistemas_Model->tickets_entregues_mes($mes_atual, $ano_mes_atual);

		$dados_totais['total_tickets_abertos']		= $total_tickets_abertos->total;
		$dados_totais['total_tickets_progress']		= $total_tickets_progress->total;
		$dados_totais['total_devs_progress']		= $total_devs_progress->total;
		$dados_totais['total_tickets_atrasados']	= $total_tickets_atrasados->total;
		$dados_totais['total_fechados_mes']			= $total_fechados_mes->total;

		$this->load->view('layout/topo');
		$this->load->view('layout/menu', compact('id_departamento', 'datas_select', 'filtro'));
		$this->load->view('sistemas/tickets_dev', compact('dados', 'dados_data'));
		$this->load->view('sistemas/total_tickets', compact('dados_totais'));
		$this->load->view('layout/rodape');
	}

	public function real_time()
	{	
		if(!acesso_departamento($this->id_departamento)){
        	redirect(base_url());
        }
        
		$dados_geral = array();

		$equipe_sistemas = $this->Sistemas_Model->list_equipe_sistemas();

		$dados_data = ano_mes_passado_atual();
		// ma (mês atual)
		$mes_atual 			= $dados_data['mes_atual'];
		$ano_mes_atual 		= $dados_data['ano_mes_atual'];
		// mp (mês passado)
		$mes_passado 		= $dados_data['mes_passado'];
		$ano_mes_passado 	= $dados_data['ano_mes_passado'];

		/*
		$mes_atual = 11;
		$ano_mes_atual = 2020;

		$mes_passado = 10;
		$ano_mes_passado = 2020;
		*/

		$dados_pessoa = array();

		// fazendo um laço para pegar os dados de cada funcionário
		foreach ($equipe_sistemas as $row_equipe) {
			
			// dados funcionário
			$dados['uid']			= $row_equipe->uid;
			$dados['username']		= $row_equipe->username;
			$dados['name']			= $row_equipe->name;
			$dados['email']			= $row_equipe->email;
			$dados['funcao']		= $row_equipe->funcao;
			$dados['valor_hora']	= $row_equipe->valor_hora;

			$dados['img_perfil'] 	= imagem_usuario($row_equipe->username);

			// Ticket que está trabalhando atualmente ou o último ticket trabalhado
			$ult_tkt_trab = $this->Sistemas_Model->ultimo_tkt_trab($row_equipe->uid);

			$dados['ult_tid']		= $ult_tkt_trab->tid;
			$dados['ult_title']		= $ult_tkt_trab->title;
			$dados['ult_local']		= $ult_tkt_trab->nome_local;

			// Somando as horas do último ticket trabalhado
			$dados_hora = $this->Sistemas_Model->horas_ult_tkt($ult_tkt_trab->tid, $row_equipe->uid);

			$dados_hora_hj = $this->Sistemas_Model->horas_ult_tkt($ult_tkt_trab->tid,$row_equipe->uid,  true);

			$dados['dados_hora'] = (convert_min_hr($dados_hora->MIN)>0)?convert_min_hr($dados_hora->MIN):round($dados_hora->MIN/60,2);

			$dados['dados_hora_hj'] = (convert_min_hr($dados_hora_hj->MIN)>0)?convert_min_hr($dados_hora_hj->MIN):round($dados_hora_hj->MIN/60,2);

			// Tickets em aberto do usuário
			$tickets_abertos 			= $this->Sistemas_Model->tickets_worker_abertos($row_equipe->uid);
			$tickets_worker_progress 	= $this->Sistemas_Model->tickets_worker_progress($row_equipe->uid);
			$dev_worker_progress		= $this->Sistemas_Model->tickets_worker_progress($row_equipe->uid, true);
			$tickets_atrasados			= $this->Sistemas_Model->tickets_atrasados($row_equipe->uid);

			$dados['tickets_abertos']			= $tickets_abertos->total;
			$dados['tickets_worker_progress']	= $tickets_worker_progress->total;
			$dados['dev_worker_progress']		= $dev_worker_progress->total;
			$dados['tickets_atrasados']			= $tickets_atrasados->total;

			$dados_pessoa[] = $dados;
		}

		$dados_geral['dados_pessoa'] = $dados_pessoa;

		## TOTAL GERAL ##
		$total_tickets_abertos 		= $this->Sistemas_Model->tickets_worker_abertos();
		$total_tickets_progress		= $this->Sistemas_Model->tickets_worker_progress();
		$total_devs_progress		= $this->Sistemas_Model->tickets_worker_progress(null, true);
		$total_tickets_atrasados	= $this->Sistemas_Model->tickets_atrasados();
		$total_fechados_mes			= $this->Sistemas_Model->tickets_entregues_mes($mes_atual, $ano_mes_atual);

		$dados_totais['total_tickets_abertos']		= $total_tickets_abertos->total;
		$dados_totais['total_tickets_progress']		= $total_tickets_progress->total;
		$dados_totais['total_devs_progress']		= $total_devs_progress->total;
		$dados_totais['total_tickets_atrasados']	= $total_tickets_atrasados->total;
		$dados_totais['total_fechados_mes']			= $total_fechados_mes->total;

		$this->load->view('layout/topo');
		$this->load->view('layout/menu', array('id_departamento' => $this->id_departamento));
		$this->load->view('sistemas/real_time', compact('dados_geral'));
		$this->load->view('sistemas/total_tickets', compact('dados_totais'));
		$this->load->view('layout/rodape');
	}

	public function sla()
	{	
		$id_departamento = $this->id_departamento;

		$datas_select = $this->Sistemas_Model->list_mes_ano_filtro();

		$filtro = null;

		if($this->input->post('mes_ano')) {

			$filtro['mes_ano'] = $this->input->post('mes_ano');

			$dados_data = ano_mes_passado_atual($filtro);
		}

		$dados_data = ano_mes_passado_atual($filtro);

		// ma (mês atual)
		$mes_atual 			= $dados_data['mes_atual'];
		$ano_mes_atual 		= $dados_data['ano_mes_atual'];
		// mp (mês passado)
		$mes_passado 		= $dados_data['mes_passado'];
		$ano_mes_passado 	= $dados_data['ano_mes_passado'];

		/*
		$mes_atual = 12;
		$ano_mes_atual = 2020;

		$mes_passado = 11;
		$ano_mes_passado = 2020;
		*/

		$dados = array();

		// Total de tickets
		// Totais
		$total_tickets_ma = $this->Sistemas_Model->total_tickets_mes($mes_atual, $ano_mes_atual, false);
		$total_tickets_mp = $this->Sistemas_Model->total_tickets_mes($mes_passado, $ano_mes_passado, false);

		$dados['total_tickets_ma'] 	= $total_tickets_ma->total;
		$dados['total_tickets_mp'] 	= $total_tickets_mp->total;

		// Avaliados
		$total_avaliado_ma = $this->Sistemas_Model->total_tickets_mes($mes_atual, $ano_mes_atual, true);
		$total_avaliado_mp = $this->Sistemas_Model->total_tickets_mes($mes_passado, $ano_mes_passado, true);

		$dados['total_avaliado_ma']	= $total_avaliado_ma->total;
		$dados['total_avaliado_mp']	= $total_avaliado_mp->total;

		// Porcentagem Satisfação
		$porc_avaliado_ma	= formata_porc2(($dados['total_avaliado_ma'] * 100) / $dados['total_tickets_ma']);
		$porc_avaliado_mp	= formata_porc2(($dados['total_avaliado_mp'] * 100) / $dados['total_tickets_mp']);

		$dados['porc_avaliado_ma']	= $porc_avaliado_ma;
		$dados['porc_avaliado_mp']	= $porc_avaliado_mp;

		// Porcentagem SLA
		// Em dia
		$total_tickets_in_ma = $this->Sistemas_Model->total_tickets_mes($mes_atual, $ano_mes_atual, null, 1);
		$total_tickets_in_mp = $this->Sistemas_Model->total_tickets_mes($mes_passado, $ano_mes_passado, null, 1);

		$porc_tickets_in_ma = formata_porc2(($total_tickets_in_ma->total * 100) / $dados['total_tickets_ma']);
		$porc_tickets_in_mp = formata_porc2(($total_tickets_in_mp->total * 100) / $dados['total_tickets_mp']);

		$dados['porc_tickets_in_ma'] = $porc_tickets_in_ma;
		$dados['porc_tickets_in_mp'] = $porc_tickets_in_mp;

		// Atrasados
		$total_tickets_out_ma = $this->Sistemas_Model->total_tickets_mes($mes_atual, $ano_mes_atual, null, 2);
		$total_tickets_out_mp = $this->Sistemas_Model->total_tickets_mes($mes_passado, $ano_mes_passado, null, 2);
		
		$porc_tickets_out_ma = formata_porc2(($total_tickets_out_ma->total * 100) / $dados['total_tickets_ma']);
		$porc_tickets_out_mp = formata_porc2(($total_tickets_out_mp->total * 100) / $dados['total_tickets_mp']);

		$dados['porc_tickets_out_ma'] = $porc_tickets_out_ma;
		$dados['porc_tickets_out_mp'] = $porc_tickets_out_mp;

		$notas = array(
			array('nota' => 5),
			array('nota' => 4),
			array('nota' => 3),
			array('nota' => 2),
			array('nota' => 1)
		);

		$tabela_notas = array();

		$notas1_ma = 0; // notas 3,4,5
		$notas2_ma = 0; // notas 1,2

		$notas1_mp = 0; // notas 3,4,5
		$notas2_mp = 0; // notas 1,2

		foreach ($notas as $key => $value) {
			
			// Buscando os tickets avaliados de acordo com a nota
			$dados_nota_ma = $this->Sistemas_Model->total_tickets_nota_mes($mes_atual, $ano_mes_atual, $value['nota']);
			$dados_nota_mp = $this->Sistemas_Model->total_tickets_nota_mes($mes_passado, $ano_mes_passado, $value['nota']);

			$dados_tabela['total_tickets_ma']	= $dados_nota_ma->total;
			$dados_tabela['porc_tickets_ma']	= formata_porc2(($dados_nota_ma->total * 100) / $dados['total_tickets_ma']);
			$dados_tabela['total_tickets_mp']	= $dados_nota_mp->total;
			$dados_tabela['porc_tickets_mp']	= formata_porc2(($dados_nota_mp->total * 100) / $dados['total_tickets_mp']);
			$dados_tabela['nota']				= $value['nota'];

			// Somanto as notas
			if ($value['nota'] == 1 || $value['nota'] == 2) {

				$notas2_ma += formata_porc2($dados_tabela['porc_tickets_ma']);
				$notas2_mp += formata_porc2($dados_tabela['porc_tickets_mp']);
			}

			if ($value['nota'] == 3 || $value['nota'] == 4 || $value['nota'] == 5) {

				$notas1_ma += formata_porc2($dados_tabela['porc_tickets_ma']);
				$notas1_mp += formata_porc2($dados_tabela['porc_tickets_mp']);
			}

			$tabela_notas[] = $dados_tabela;
		}

		$dados['tabela_notas'] = $tabela_notas;

		$dados_graf_ma = '';
		$dados_graf_mp = '';
		$notas_graf = '';

		// Montando os valores para o gráfico de avaliados
		foreach ($tabela_notas as $row_tabela) {

			if($row_tabela['porc_tickets_ma'] > 0 || $row_tabela['porc_tickets_mp'] > 0) {

				$notas_graf .= ','.$row_tabela['nota'];
				$dados_graf_ma .= ','.$row_tabela['porc_tickets_ma'];
				$dados_graf_mp .= ','.$row_tabela['porc_tickets_mp'];
			}
		}

		$notas_graf		= substr($notas_graf, 1);
		$dados_graf_ma 	= substr($dados_graf_ma, 1);
		$dados_graf_mp 	= substr($dados_graf_mp, 1);

		$dados['notas_graf']	= $notas_graf;
		$dados['dados_graf_ma'] = $dados_graf_ma;
		$dados['dados_graf_mp'] = $dados_graf_mp;

		## CALCULANDO AS DIFERENÇAS SATISFAÇÃO ## 

		// Avaliados
		if ($porc_avaliado_ma > $porc_avaliado_mp) {
			// melhorou
			$icone_av 		= 'fas fa-arrow-up';
			$color_av		= 'text-success';
			$diferenca_av 	= $porc_avaliado_ma - $porc_avaliado_mp;

		} elseif ($porc_avaliado_ma == $porc_avaliado_mp) {
			// está igual
			$icone_av 		= 'fas fa-equals';
			$color_av		= 'text-info';
			$diferenca_av 	= 0;

		} else {
			// ainda está abaixo
			$icone_av 		= 'fas fa-arrow-down';
			$color_av		= 'text-danger';
			$diferenca_av 	= $porc_avaliado_mp - $porc_avaliado_ma;
		}

		$dados['icone_av']		= $icone_av;
		$dados['color_av']		= $color_av;
		$dados['diferenca_av']	= $diferenca_av;


		// Notas 3,4,5
		if ($notas1_ma > $notas1_mp) {

			$icone_n1 		= 'fas fa-arrow-up';
			$color_n1		= 'text-success';
			$diferenca_n1 	= $notas1_ma - $notas1_mp;

		} elseif ($notas1_ma == $notas1_mp) {

			$icone_n1 		= 'fas fa-equals';
			$color_n1		= 'text-info';
			$diferenca_n1 	= 0;

		} else {

			$icone_n1 		= 'fas fa-arrow-down';
			$color_n1		= 'text-danger';
			$diferenca_n1 	= $notas1_mp - $notas1_ma;
		}

		$dados['icone_n1']		= $icone_n1;
		$dados['color_n1']		= $color_n1;
		$dados['diferenca_n1']	= $diferenca_n1;

		// Notas 1,2
		if ($notas2_ma > $notas2_mp) {

			$icone_n2 		= 'fas fa-arrow-up';
			$color_n2		= 'text-success';
			$diferenca_n2 	= $notas2_ma - $notas2_mp;

		} elseif ($notas2_ma == $notas2_mp) {

			$icone_n2 		= 'fas fa-equals';
			$color_n2		= 'text-info';
			$diferenca_n2 	= 0;

		} else {

			$icone_n2 		= 'fas fa-arrow-down';
			$color_n2		= 'text-danger';
			$diferenca_n2 	= $notas2_mp - $notas2_ma;
		}

		$dados['icone_n2']		= $icone_n2;
		$dados['color_n2']		= $color_n2;
		$dados['diferenca_n2']	= $diferenca_n2;

		$list_categories = $this->Sistemas_Model->list_categories_tkts_fechados($mes_atual, $ano_mes_atual);

		$tabela_cat = array();

		foreach($list_categories as $row_cat) {

			$dados_tickets_ma 		= $this->Sistemas_Model->total_tickets_mes($mes_atual, $ano_mes_atual, null, null, $row_cat->cid);
			$dados_tickets_mp 		= $this->Sistemas_Model->total_tickets_mes($mes_passado, $ano_mes_passado, null, null, $row_cat->cid);

			$dados_tickets_in_ma 	= $this->Sistemas_Model->total_tickets_mes($mes_atual, $ano_mes_atual, null, 1, $row_cat->cid);
			$dados_tickets_in_mp 	= $this->Sistemas_Model->total_tickets_mes($mes_passado, $ano_mes_passado, null, 1, $row_cat->cid);

			$dados_tickets_out_ma 	= $this->Sistemas_Model->total_tickets_mes($mes_atual, $ano_mes_atual, null, 2, $row_cat->cid);
			$dados_tickets_out_mp 	= $this->Sistemas_Model->total_tickets_mes($mes_passado, $ano_mes_passado, null, 2, $row_cat->cid);
			
			$dados_tabela['cid']		= $row_cat->cid;
			$dados_tabela['name']		= $row_cat->name;
			$dados_tabela['name_abrev']	= $row_cat->name_abrev;
			
			$dados_tabela['tickets_ma']		= $dados_tickets_ma->total;
			$dados_tabela['tickets_mp']		= $dados_tickets_mp->total;
			$dados_tabela['tickets_in_ma']	= $dados_tickets_in_ma->total;
			$dados_tabela['tickets_in_mp']	= $dados_tickets_in_mp->total;
			$dados_tabela['tickets_out_ma']	= $dados_tickets_out_ma->total;
			$dados_tabela['tickets_out_mp']	= $dados_tickets_out_mp->total;

			$tabela_cat[] = $dados_tabela;
		}

		$dados['tabela_cat'] = $tabela_cat;

		## CALCULANDO AS DIFERENÇAS SLA ##

		// tickets
		if($total_tickets_ma->total > $total_tickets_mp->total) {

			$icone_tkt 		= 'fas fa-arrow-up';
			$color_tkt		= 'text-danger';
			$diferenca_tkt 	= formata_porc2((($total_tickets_ma->total - $total_tickets_mp->total) / $total_tickets_ma->total) * 100);

		} elseif ($total_tickets_ma->total == $total_tickets_mp->total) {
			
			$icone_tkt 		= 'fas fa-equals';
			$color_tkt		= 'text-info';
			$diferenca_tkt 	= 0;

		} else {

			$icone_tkt 		= 'fas fa-arrow-down';
			$color_tkt		= 'text-success';
			$diferenca_tkt 	= formata_porc2((($total_tickets_mp->total - $total_tickets_ma->total) / $total_tickets_mp->total) * 100);
		}

		$dados['icone_tkt'] 	= $icone_tkt;
		$dados['color_tkt']		= $color_tkt;
		$dados['diferenca_tkt']	= $diferenca_tkt;

		// in 
		if ($porc_tickets_in_ma > $porc_tickets_in_mp) {

			$icone_in 		= 'fas fa-arrow-up';
			$color_in		= 'text-success';
			$diferenca_in 	= $porc_tickets_in_ma - $porc_tickets_in_mp;

		} elseif ($porc_tickets_in_ma == $porc_tickets_in_mp) {
			
			$icone_in 		= 'fas fa-equals';
			$color_in		= 'text-info';
			$diferenca_in 	= 0;

		} else {

			$icone_in 		= 'fas fa-arrow-down';
			$color_in		= 'text-danger';
			$diferenca_in 	= $porc_tickets_in_mp - $porc_tickets_in_ma;
		}

		$dados['icone_in']		= $icone_in;
		$dados['color_in']		= $color_in;
		$dados['diferenca_in']	= $diferenca_in;

		// out
		if ($porc_tickets_out_ma > $porc_tickets_out_mp) {

			$icone_out 		= 'fas fa-arrow-up';
			$color_out		= 'text-danger';
			$diferenca_out 	= $porc_tickets_out_ma - $porc_tickets_out_mp;

		} elseif ($porc_tickets_out_ma == $porc_tickets_out_mp) {
			
			$icone_out 		= 'fas fa-equals';
			$color_out		= 'text-info';
			$diferenca_out 	= 0;

		} else {

			$icone_out 		= 'fas fa-arrow-down';
			$color_out		= 'text-success';
			$diferenca_out 	= $porc_tickets_out_mp - $porc_tickets_out_ma;
		}

		$dados['icone_out']		= $icone_out;
		$dados['color_out']		= $color_out;
		$dados['diferenca_out']	= $diferenca_out;

		$this->load->view('layout/topo');
		$this->load->view('layout/menu', compact('id_departamento', 'datas_select', 'filtro'));
		$this->load->view('sistemas/sla', compact('dados'));
		$this->load->view('layout/rodape');
	}

	public function list_tickets_tipo()
	{
		$this->load->library('form_validation');

        $retorno = null;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $this->form_validation->set_rules('cid', 'tipo', 'required');

            if ($this->form_validation->run() != FALSE) {

            	$cid = $this->input->post('cid');

            	$dados_data = ano_mes_passado_atual();
				
				$mes_atual 			= $dados_data['mes_atual'];
				$ano_mes_atual 		= $dados_data['ano_mes_atual'];

				$tickets = $this->Sistemas_Model->list_tickets_tipo($mes_atual, $ano_mes_atual, $cid);

				echo json_encode($tickets);
				die;
            }
        }
	}

	public function list_tickets_department()
	{
		$this->load->library('form_validation');

        $retorno = null;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $this->form_validation->set_rules('id_department', 'department', 'required');

            if ($this->form_validation->run() != FALSE) {

            	$id_department = $this->input->post('id_department');

            	$dados_data = ano_mes_passado_atual();
				
				$mes_atual 			= $dados_data['mes_atual'];
				$ano_mes_atual 		= $dados_data['ano_mes_atual'];

				$tickets = $this->Sistemas_Model->list_tickets_department($mes_atual, $ano_mes_atual, $id_department);

				echo json_encode($tickets);
				die;
            }
        }
	}

	public function list_devs_entregues_mes()
	{
		$this->load->library('form_validation');

        $retorno = null;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $this->form_validation->set_rules('ano_mes_atual', 'Ano mês atual', 'required');
            $this->form_validation->set_rules('mes_atual', 'Mês atual', 'required');

            if ($this->form_validation->run() != FALSE) {

            	$mes_atual 		= $this->input->post('mes_atual');
            	$ano_mes_atual 	= $this->input->post('ano_mes_atual');

            	// Tickets finaliados no mês
				$tickets = $this->Sistemas_Model->list_devs_entregues_mes($mes_atual, $ano_mes_atual);

				echo json_encode($tickets);
				die;
            }
        }
	}

	public function list_devs_horas_mes()
	{
		$this->load->library('form_validation');

        $retorno = null;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $this->form_validation->set_rules('ano_mes_atual', 'Ano mês atual', 'required');
            $this->form_validation->set_rules('mes_atual', 'Mês atual', 'required');

            if ($this->form_validation->run() != FALSE) {

            	$mes_atual 		= $this->input->post('mes_atual');
            	$ano_mes_atual 	= $this->input->post('ano_mes_atual');

            	// Tickets finaliados no mês
				$tickets = $this->Sistemas_Model->list_devs_entregues_mes($mes_atual, $ano_mes_atual);

				echo json_encode($tickets);
				die;
            }
        }
	}

	public function dash_geral()
	{	
		if(!usuarios_acesso_monitoramento()){
        	redirect(base_url());
        }
        $url = base_url(uri_string());
        $user = $this->dados_usuario = get_usuario();
        if($user){
        	$grava_log = $this->Log_Acesso_Model->grava_log($url,$user);
        }

    	$this->load->library('ApiLyceum');
    	$consul_dados 	= $this->apilyceum->dash_rd();

    	$dia = date("Y-m-d");

	    $retorno_log = $this->Sistemas_Model->listar_log($dia);
	    $retorno_site = $this->Sistemas_Model->listar_site($dia);
	    $retorno_mkt = $this->Sistemas_Model->listar_mkt($dia);
	    $retorno_crm = $this->Sistemas_Model->listar_crm($dia);
	    $oportunidades_manuais 	= $this->Sistemas_Model->listar_oportunidades_manuais($dia);
	    $oportunidades_erro 	= $this->Sistemas_Model->listar_oportunidades_erro($dia);

		$validacao = array(($consul_dados['dados'][0]['QTD_LOG']),($consul_dados['dados'][0]['QTD_FORM']),($consul_dados['dados'][0]['QTD_MKT']),($consul_dados['dados'][0]['QTD_CRM']));
		$log_erro_crm = $this->apilyceum->dash_log('FORM_RDCRM');
		$log_erro_mkt = $this->apilyceum->dash_log('FORM_RDMKT');
		$grafico_pizza = $this->apilyceum->dash_geral();
		$grafico_detalhado = $this->apilyceum->dash_detalhado();
		$string_dia = $grafico_detalhado['dados'][0]['DIA'];
   		$string = $string_dia;
    	$dia_convertido = strtotime($string);
    	$somente_dia = date("d",$dia_convertido);
    	$erro_crm_relacao_mkt = $this->Sistemas_Model->listar_errocrm_marketing($dia);


    	$dados_google = $this->apilyceum->api_google();




		$retorno_servicos = $this->apilyceum->dash_servicos();
  
	  	//$result_array = array_unique($validacao);
		if($validacao[0]){
			$validacao_value = '<i class="fas fa-check icon_correct"></i>';
		}
		if($validacao[0] == $validacao[1]){
			$validacao_value1 = '<i class="fas fa-check icon_correct"></i>';
		}else{

			$validacao_value1 = '<i class="fas fa-exclamation-triangle icon_difference pulse"></i>';
		}

		if(($validacao[1] - $oportunidades_erro->total) == $validacao[2]){
			$validacao_value2 = '<i class="fas fa-check icon_correct"></i>';
		}else{

			$validacao_value2 = '<a data-toggle="modal" data-target="#mkt_error" class="sobrepor_icon"><i class="fas fa-exclamation-triangle icon_difference pulse"></i></a>';
		}
		if($validacao[2] == $validacao[3]){

			$validacao_value3 = '<i class="fas fa-check icon_correct"></i>';
		}else{

			$validacao_value3 = '<a data-toggle="modal" data-target="#crm_error" class="sobrepor_icon"><i class="fas fa-exclamation-triangle icon_difference pulse"></i></a>';
		}

		$this->load->view('layout/topo');
		//$this->load->view('layout/menu', array('id_departamento' => $this->id_departamento));
		$this->load->view('sistemas/dash_geral', compact('consul_dados','retorno_log','retorno_site','retorno_mkt','retorno_crm','validacao_value','validacao_value1','validacao_value2', 'validacao_value3','log_erro_crm','log_erro_mkt','grafico_pizza','grafico_detalhado','retorno_servicos','somente_dia', 'oportunidades_manuais', 'oportunidades_erro','erro_crm_relacao_mkt'));
		$this->load->view('layout/rodape');
	}
}
