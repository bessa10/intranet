<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller 
{
	private $dados_usuario;

	public function __construct() {
		parent::__construct();

		$this->dados_usuario = get_usuario();

		/*if(!$this->dados_usuario) {
		 	die('não autorizado');
		}*/

		$this->load->model('Solicitacao_Model');
		$this->load->model('Postagem_Model');

		$this->load->library('form_validation');
	}

	public function index()
	{	
		// últimas solicitações nos sistemas de chamados
		$dados_usuario = $this->dados_usuario;

		$usuario = (isset($this->dados_usuario->usuario)) ? $this->dados_usuario->usuario : null;

		$chamados_sistemas 		= $this->Solicitacao_Model->ultimos_chamados_sistemas($usuario);
		$chamados_ti 			= $this->Solicitacao_Model->ultimos_chamados_ti($usuario);
		$chamados_facilities 	= $this->Solicitacao_Model->ultimos_chamados_facilities($usuario);
		$chamados_int_dados 	= $this->Solicitacao_Model->ultimos_chamados_int_dados($usuario);
		$chamados_gestao_proj 	= $this->Solicitacao_Model->ultimos_chamados_gestao_projetos($usuario);

		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			if(!$this->input->post('texto_publicacao') && !$this->input->post('incorpora_video') && !$this->input->post('anexo[]')) {

				$this->session->set_flashdata(array(
					'type'	=> 'danger',
					'msg'	=> 'Por favor preencha no mínimo uma das informações.'
				));
			}

			// Realizando Upload 
			if($_FILES && $_FILES['attachments']['name'][0])
            {
                $attachments = $this->upload_file();

                if($attachments === FALSE)
                {
                    return;
                }
            }

			$dados['id_usuario']		= $dados_usuario->id_usuario;
			$dados['texto_publicacao']	= $this->input->post('texto_publicacao');
			$dados['incorpora_video']	= $this->input->post('incorpora_video');
			
			$id_postagem = $this->Postagem_Model->gravar($dados);

			if(!$id_postagem) {

				$this->session->set_flashdata(array(
					'type'	=> 'danger',
					'msg'	=> 'Não foi possível cadastrar a publicação, por favor tente novamente.'
				));

			} else {

				if(!empty($attachments)) {

                	$this->Postagem_Model->gravar_anexos($attachments, $id_postagem);  
                }

                $this->session->set_flashdata(array(
					'type'	=> 'success',
					'msg'	=> 'Postagem cadastrada com sucesso, por favor aguarde a aprovação para aparecer no feed.'
				));
			}
		}

		$postagens_aprovadas = $this->Postagem_Model->postagens_aprovadas();

		$aniversarios = $this->Postagem_Model->aniversarios_mes();

		$this->load->view('layout/topo');
		$this->load->view('layout/menu');
		$this->load->view('index', compact('dados_usuario','chamados_sistemas','chamados_ti','chamados_facilities', 'chamados_int_dados', 'chamados_gestao_proj', 'postagens_aprovadas', 'aniversarios'));
		$this->load->view('layout/rodape');
	}

	public function upload_file()
    {
        $attachments = array();

        $this->load->library('upload');

        $files = $_FILES;
        $files_count = count($_FILES['attachments']['name']);

        for($i = 0; $i < $files_count; $i++)
        {
            $_FILES['attachments']['name']= $files['attachments']['name'][$i];
            $_FILES['attachments']['type']= $files['attachments']['type'][$i];
            $_FILES['attachments']['tmp_name']= $files['attachments']['tmp_name'][$i];
            $_FILES['attachments']['error']= $files['attachments']['error'][$i];
            $_FILES['attachments']['size']= $files['attachments']['size'][$i];

            $this->upload->initialize($this->set_upload_options());

            if(!$this->upload->do_upload('attachments')) {

                $this->session->set_flashdata('error', $this->upload->display_errors('', ''));
                
                return false;
            
            } else {

                $attachments[] = $this->upload->data();
            }
        }

        return $attachments;
    }

    private function set_upload_options()
    {
        //upload an image options
        $config = array();
        $config['upload_path']      = './attachments/';
        $config['allowed_types']    = 'gif|jpg|jpeg|png|doc|docx|csv|xls|xlsx|html|pdf|mp4|3g2|3gp|3gpp|avi|mov|mp4';
        $config['encrypt_name']		= true;

        return $config;
    }
}
