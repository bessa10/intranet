<?php

class Apilyceum
{
	private $url;
	private $user_api;
	private $password_api;

	public function __construct()
	{
		$this->url      	= "http://secretaria.cetrus.com.br:8080/API";
		$this->user_api 	= "apiuser";
		$this->password_api = "apiuser@123";
	}

	public function sendRDHttp($link = null, $data = null, $method = 'GET')
	{
		$url = $this->url.$link;

		$data = json_encode($data);

		$headers = array(
			'Content-Type: application/json',
			'Authorization: Basic '. base64_encode($this->user_api.':'.$this->password_api)
		);

		$sessao_curl = curl_init(); //inicializando

		if($method == 'GET'){
			curl_setopt($sessao_curl, CURLOPT_CUSTOMREQUEST, 'GET');
		}else if($method == 'POST'){
			curl_setopt($sessao_curl, CURLOPT_POST, true);
			curl_setopt($sessao_curl, CURLOPT_POSTFIELDS, $data); //passagem de paramentro

		}else if($method == 'PUT'){
			curl_setopt($sessao_curl, CURLOPT_CUSTOMREQUEST, 'PUT');

		}else if($method == 'DELETE'){
			curl_setopt($sessao_curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
			curl_setopt($sessao_curl, CURLOPT_POST, true);
			curl_setopt($sessao_curl, CURLOPT_POSTFIELDS, $data); //passagem de paramentro
		}

		curl_setopt($sessao_curl, CURLOPT_URL, $url);
		curl_setopt($sessao_curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($sessao_curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($sessao_curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($sessao_curl, CURLOPT_SSL_VERIFYPEER, false);

		$response = curl_exec($sessao_curl);

		if(!$response){
			$response = curl_error($sessao_curl);
		}

		curl_close($sessao_curl);


		return $response;

	}

	public function consulta_modalidade($FORM =  null)
	{


			$link = '/comum/tipo/CSV/titulo/modalidade/consultaDinamica';

			$data = '';

			$json = $this->sendRDHttp($link, $data, 'POST');

			//echo $json;die;

			$retorno = json_decode($json, TRUE);

			//var_dump($retorno);die;

			return $retorno;

	}


	public function consulta_grupos($FORM =  null)
	{


		$link = '/comum/tipo/CSV/titulo/grupo_curso/consultaDinamica';

		$data = '';

		$json = $this->sendRDHttp($link, $data, 'POST');

		//echo $json;die;

		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;

	}

	public function consulta_organizacao($FORM =  null)
	{


		$link = '/comum/tipo/CSV/titulo/organizacao_curso/consultaDinamica';

		$data = '';

		$json = $this->sendRDHttp($link, $data, 'POST');

		//echo $json;die;

		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;

	}

	public function consulta_docentes($filtro = null, $filtro2 = null)
	{


		$link = '/comum/tipo/CSV/titulo/docente/consultaDinamica';

		$data['p_NOME'] = $filtro['nome_docente'];
		$data['p_PESSOA'] = $filtro2;

		$json = $this->sendRDHttp($link, $data, 'POST');

		//echo $json;die;
		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;

	}

	public function consulta_especilidades($FORM =  null)
	{
		$link = '/comum/tipo/CSV/titulo/especialidades/consultaDinamica';

		$data = '';

		$json = $this->sendRDHttp($link, $data, 'POST');

		//echo $json;die;

		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;
	}

	public function consulta_mnemonico($filtro = null)
	{
		$link = '/comum/tipo/CSV/titulo/mnemonico/consultaDinamica';

		$data['P_MNEMONICO'] = $filtro;

		$json = $this->sendRDHttp($link, $data, 'POST');

		//echo $json;die;

		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;
	}

	public function cursos_completos($filtro = null)
	{
		$link = '/comum/tipo/CSV/titulo/cursos_api/consultaDinamica';

		$data = '';

		$json = $this->sendRDHttp($link, $data, 'POST');

		//echo $json;die;

		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;

	}

	public function list_disciplinas($filtro = null)
	{
		$link = '/comum/tipo/CSV/titulo/disciplinas_api/consultaDinamica';

		$data['p_MODALIDADE'] = "";

		if($filtro['modalidade']) {

			$data['p_MODALIDADE'] = $filtro['modalidade'];
		}

		$json = $this->sendRDHttp($link, $data, 'POST');

		//echo $json;die;

		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;
	}

	public function dash_rd()
	{
		$link = '/comum/tipo/CSV/titulo/retorno_painel/consultaDinamica';

		$data['DIA'] = "";

		$data['DIA'] = date("Y-m-d");
		

		$json = $this->sendRDHttp($link, $data, 'POST');

		//echo $json;die;

		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;
	}

	public function dash_log($origem)
	{
		$link = '/comum/tipo/CSV/titulo/retorno_painel_DIF/consultaDinamica';

		$data['DIA'] = "";

		$data['DIA'] = date("Y-m-d");
		$data['ORIGEM'] = $origem;
		

		$json = $this->sendRDHttp($link, $data, 'POST');

		//echo $json;die;

		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;
	}

	public function dash_geral()
	{
		$link = '/comum/tipo/CSV/titulo/retorno_painel_geral/consultaDinamica';

		$data['DATA'] = "";

		$data = '';
		

		$json = $this->sendRDHttp($link, $data, 'POST');

		//echo $json;die;

		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;
	}

		public function dash_detalhado()
	{
		$link = '/comum/tipo/CSV/titulo/retorno_painel_geral_detalhada/consultaDinamica';

		$data = "";

		$json = $this->sendRDHttp($link,$data, 'POST');

		//echo $json;die;

		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;
	}

		public function dash_servicos()
	{
		$link = '/comum/tipo/CSV/titulo/retorno_painel_servicos/consultaDinamica';

		$data['DATA'] = date("Y-m-d");

		$json = $this->sendRDHttp($link,$data, 'POST');

		//echo $json;die;

		$retorno = json_decode($json, TRUE);

		//var_dump($retorno);die;

		return $retorno;
	}

	public function api_google()
	{

		$curl = curl_init();

		curl_setopt_array($curl, array(
  			CURLOPT_URL => 'https://www.googleapis.com/analytics/v3/management/accounts/7283705/webproperties/UA-7283705-1/profiles/15557421/goals/?access_token=ya29.A0AfH6SMBMdX7M3HE8HVHcJ20M2LABQGKC96_Cn6FFJX1zalczm2cRk2GlOHltwjVMRSwL-MY64nBbcqQZwf8Mh4oaBhBqoGzV5mkWlPuVpyly_NI75KOsLYiSQztsfOSWD0TLEpomiB7AM5QqOR7D3a-NoIM5',
  		CURLOPT_RETURNTRANSFER => true,
  		CURLOPT_ENCODING => '',
  		CURLOPT_MAXREDIRS => 10,
  		CURLOPT_TIMEOUT => 0,
  		CURLOPT_FOLLOWLOCATION => true,
  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  		CURLOPT_CUSTOMREQUEST => 'GET',
		));

		$response = curl_exec($curl);
		$retorno = json_decode($response, TRUE);


		curl_close($curl);

		return $retorno;
		//echo $response;
	}
}