<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('formata_preco')) {

    function formata_preco($valor) {
        $moeda = number_format($valor, 2, ',', '.');

        return $moeda;
    }

}

if (!function_exists('formata_preco_dash')) {

    function formata_preco_dash($valor) {

        if($valor < 1000) {

            $dig1 = ((string)abs($valor))[0];

            $moeda = (float) 0 . '.'. $dig1;

        } else {

            $moeda = number_format($valor, 2, ',', '.');

            $moeda = round($moeda,1);
        }

        return $moeda;
    }
}

if (!function_exists('formata_porc')) {

    function formata_porc($valor) {

        $valor = round($valor,2);

        return $valor;
    }
}

if (!function_exists('formata_porc2')) {

    function formata_porc2($valor) {

        $valor = round($valor);

        return $valor;
    }
}


if (!function_exists('formata_hs')) {

    function formata_hs($valor) {

        $valor = round($valor, 0); 

        return $valor;
    }
}

if (!function_exists('convert_min_hr')) {
    function convert_min_hr($min) {

        $hora = floor($min / 60);

        return $hora;

    }
}


if (!function_exists('formata_cnpj')) {

    function formata_cnpj($valor) {
        $cnpj = preg_replace("/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})([0-9]{2})/", "$1.$2.$3/$4-$5", $valor);

        return $cnpj;
    }

}
if (!function_exists('formata_cep')) {

    function formata_cep($valor) {
        $cep = substr($valor, 0, 5) . '-' . substr($valor, 5, 3);

        return $cep;
    }

}
if (!function_exists('formata_data')) {

    function formata_data($valor) {
        $data = date('d/m/Y', strtotime($valor));

        return $data;
    }
}


if (!function_exists('formata_dataMes')) {

    function formata_dataMes($valor) {
        $data = date('d/m', strtotime($valor));

        return $data;
    }
}

if (!function_exists('formata_dia')) {

    function formata_dia($valor) {
        $data = date('d', strtotime($valor));

        return $data;
    }
}

if (!function_exists('formata_data_hora')) {

    function formata_data_hora($valor) {
        $data = date('d/m/Y H:i:s', strtotime($valor));

        return $data;
    }

}

if (!function_exists('colocar_barras')) {

    function colocar_barras($valor) {
        $dia = substr($valor, -8, 2);

        $mes = substr($valor, -6, 2);

        $ano = substr($valor, -4);

        $data = $dia . '/' . $mes . '/' . $ano;

        return $data;
    }

}
if (!function_exists('date_converter')) {

    function date_converter($_date = null) {
        $format = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/';

        if ($_date != null && preg_match($format, $_date, $partes)) {
            return $partes[3] . '-' . $partes[2] . '-' . $partes[1];
        }
        return false;
    }

}
if (!function_exists('limpa_cnpj_cep')) {

    function limpa_cnpj_cep($valor) {
        $procura = array('.', '/', '-', ',');

        $cnpj = str_replace($procura, '', $valor);

        return $cnpj;
    }

}
if (!function_exists('limpa_login')) {

    function limpa_login($valor) {
        $procura = array(' ', '&', '-', ';', 'ç', 'Ç', 'á', 'à', 'â', 'ã', 'À', 'Á', 'Â', 'Ã', 'é', 'É', 'è', 'È', 'ê', 'Ê', 'í', 'Í', 'ó', 'Ó', 'ô', 'Ô', 'ú', 'Ú', 'ù', 'Ù');

        $troca = array('_', '', '', '', 'c', 'c', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u');

        $login = str_replace($procura, $troca, $valor);

        return $login;
    }

}

if (!function_exists('countPostagensAprovar')) {

    function countPostagensAprovar() {
        $instance = & get_instance();

        $result = $instance->db
                    ->where(array('aprovado' => 0, 'excluido' => 0))
                    ->get('tb_postagens')
                    ->num_rows();
        return $result;
    }
}


if (!function_exists('anexosPostagem')) {

    function anexosImgPostagem($id_postagem) {
        $instance = & get_instance();

        $result = $instance->db
                    ->where(array('id_postagem' => $id_postagem, 'is_image' => 'Y', 'excluido' => 0))
                    ->get('tb_anexos_postagens')
                    ->result();
        return $result;
    }
}


if (!function_exists('anexosPostagem')) {

    function anexosVideoPostagem($id_postagem) {
        $instance = & get_instance();

        $result = $instance->db
                    ->where(array('id_postagem' => $id_postagem, 'excluido' => 0))
                    ->like('file_type', 'video')
                    ->get('tb_anexos_postagens')
                    ->result();
        return $result;
    }
}


if (!function_exists('anexosPostagem')) {

    function anexosOutrosPostagem($id_postagem) {
        $instance = & get_instance();

        $result = $instance->db
                    ->where(array('id_postagem' => $id_postagem, 'is_image' => 'N', 'excluido' => 0))
                    ->not_like('file_type', 'video')
                    ->get('tb_anexos_postagens')
                    ->result();
        return $result;
    }
}


if (!function_exists('get_usuario')) {

    function get_usuario() {

        if(ENVIRONMENT == 'development') {

            $usuario = 'bruno.chaves';

        } else {

            //Pegando o usuário que está logado na rede
            $usuario = $_SERVER['PHP_AUTH_USER'];
        }

        $instance = & get_instance();

        $result = $instance->db
                    ->where(array('usuario' => $usuario, 'cancelado' => 0))
                    ->get('tb_usuarios')
                    ->row();

        return $result;
    }
}

if (!function_exists('get_menu')) {

    function get_menu() {

        $instance = & get_instance();

        //Pegando o usuário que está logado na rede
        $usuario = get_usuario();

        $instance->db->where('visivel', 1);

        if($usuario) {

            $grupo_acesso = $usuario->grupo_acesso;

            $array_acesso = explode(',', $grupo_acesso);

            if($grupo_acesso != 'geral') {

                $array_acesso = explode(',', $grupo_acesso);

                $instance->db
                    ->where('mostrar_geral', 1)
                    ->or_where_in('grupo_acesso', $array_acesso);
            }

        } else {

            $instance->db->where('mostrar_geral', 1);
        }

        $result = $instance->db->get('tb_departamentos')->result();        
            
        return $result;
    }
}

if (!function_exists('menus_departamentos')) {

    function menus_departamentos($id_departamento = null) {

        if($id_departamento) {

            $usuario = get_usuario();

            $instance = & get_instance();

            $instance->db
                ->join('tb_departamentos', 'tb_departamentos.id_departamento = tb_menus_departamentos.id_departamento', 'inner')
                ->where('tb_menus_departamentos.id_departamento', $id_departamento)
                ->where('tb_menus_departamentos.visivel', 1);

            if($usuario) {

                $grupo_acesso = $usuario->grupo_acesso;

                $array_acesso = explode(',', $grupo_acesso);

                if($grupo_acesso != 'geral') {

                    $array_acesso = explode(',', $grupo_acesso);

                    $instance->db
                        ->where('tb_menus_departamentos.mostrar_geral', 1)
                        ->or_where_in('tb_departamentos.grupo_acesso', $array_acesso);
                }

            } else {

                $instance->db->where('tb_menus_departamentos.mostrar_geral', 1);
            }

            $instance->db->where('tb_departamentos.id_departamento', $id_departamento);

            $result = $instance->db->get('tb_menus_departamentos')->result();

            return $result;

        } else {

            return false;
        }
    }
}

if (!function_exists('acesso_departamento')) {

    function acesso_departamento($id_departamento) {

        $usuario = get_usuario();

        if($usuario) {
            
            $instance = & get_instance();

            $grupo_acesso = $usuario->grupo_acesso;

            $array_acesso = explode(',', $grupo_acesso);

            if($grupo_acesso != 'geral') {

                $array_acesso = explode(',', $grupo_acesso);

                $instance->db->where_in('grupo_acesso', $array_acesso);
            }

            $result = $instance->db
                        ->where('id_departamento', $id_departamento)
                        //->like('grupo_acesso', $grupo_acesso)
                        ->get('tb_departamentos')
                        ->row();

            return $result;

        } else {

            return false;
        }
    }
}

function selectDB($name, $title = null, $values, $value_name, $value_id, $default = null, $class = null, $js = null, $padrao = null, $form_error = null)
{
    if($name) {

        $error = ($form_error) ? 'is-invalid' : '';

        $class = ($class) ? ' class="'. $class .' form-control '.$error.' "' : 'class="form-control '.$error.'"';

        $js    = ($js) ? ' onchange="'. $js .'" ' : '';

        $padrao = ($padrao) ? $padrao : 'Selecione';

        if($title) {
            $html = '<label>'.$title.'</label>';
        } else {
            $html = '';
        }

        $html .= '<select name="'. $name .'" id="'.$name.'" ' . $class . $js .'><option value="">'.$padrao.'</option>';

        if($values) {

            foreach ($values as $list) {

                $select = '';

                if($list->{$value_id} == $default) {

                    $select = ' selected';
                }

                $html .='<option id="id_'. $list->{$value_id} .'" value="'. $list->{$value_id} .'"'. $select .'>'. $list->{$value_name} ."</option>\n";
            }
        }

        $html .= "</select>\n";

        $html .='<div class="invalid-feedback">'.$form_error.'</div>';

        return $html;
    }
}

if (!function_exists('ano_mes_passado_atual')) {

    function ano_mes_passado_atual($filtro = null) {

        $dados = array();

        if($filtro['mes_ano']) {

            $data = explode('/', $filtro['mes_ano']);

            $dados['mes_atual']     = date($data[0]);
            $dados['ano_mes_atual'] = date($data[1]);

            if($dados['mes_atual'] == "01") {
                
                $dados['mes_passado']       = 12;
                $dados['ano_mes_passado']   = $data[1] - 1;

            } else {
                $dados['mes_passado']       = date($data[0]) - 1;
                $dados['ano_mes_passado']   = date($data[1]);
            }

        } else {

            $dados['mes_atual']     = date('m');
            $dados['ano_mes_atual'] = date('Y');

            if($dados['mes_atual'] == "01") {
                $dados['mes_passado']       = 12;
                $dados['ano_mes_passado']   = date('Y') - 1;

            } else {
                $dados['mes_passado']       = date('m') - 1;
                $dados['ano_mes_passado']   = date('Y');
            }
        }

        return $dados;
    }
}

if (!function_exists('imagem_usuario')) {

    function imagem_usuario($username) {

        $instance = & get_instance();

        $dados_usuario =  $instance->db
                            ->where('usuario', $username)
                            ->get('tb_usuarios')
                            ->row();

        if($dados_usuario) {

            return $dados_usuario->img_perfil;

        } else {

            return 'user.png';
        }
    }
}

if (!function_exists('usuarios_acesso_monitoramento')) {

    function usuarios_acesso_monitoramento() {

        $usuario = get_usuario();

        $grupo_acesso = $usuario->grupo_acesso;

        $array_acesso = explode(',', $grupo_acesso);

        if($grupo_acesso == 'geral') {

            return true;

        } else {

            $usuarios_liberar = array(
                'fabio.abreu',
                'paula.fonseca',
                'rita.braghetti',
                'sebastiao',
                'claudio',
                'marcio.roldao',
                'bruno.chaves',
                'eduardo.araujo',
                'daniela.macedo',
                'gabriela.antunes'
            );

            if (count($array_acesso) > 0) {

                if (in_array('sistemas', $array_acesso)) {

                    return true;

                } else {

                    if (in_array($usuario->usuario, $usuarios_liberar)) {

                        return true;

                    } else {

                        return false;
                    }
                }

            } else {

                if (in_array($usuario->usuario, $usuarios_liberar)) {

                    return true;

                } else {

                    return false;
                }
            }
        }
    }
}