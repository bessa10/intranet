<div class="container">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h2 class="text-center">Publicações aguardando aprovação</h2><br>

		<?php if($this->session->flashdata()): ?>
        <div id="alert-publicacao">	
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="alert alert-<?=$this->session->flashdata('type')?>" id="alert">
                    <strong><?= $this->session->flashdata('msg') ?></strong>
                </div>
            </div>
    	</div>
        <?php endif ?>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
		<?php if($postagens_aguardando): ?>	
			<?php foreach($postagens_aguardando as $postagem): ?>
				<?php 
					$anexos_img = anexosImgPostagem($postagem->id_postagem);
					$anexos_video = anexosVideoPostagem($postagem->id_postagem);
					$anexos_outros = anexosOutrosPostagem($postagem->id_postagem);
				?>
				<div class="card box-postagem">
					<div class="card-body">
						<?php
						if($postagem->img_perfil!= null) {
							$img_usuario = $postagem->img_perfil;
						} else {
							$img_usuario = 'user.png';
						}
						?>
						<div class="row">
							<ul class="ul-user-post">
								<li><div class="box-img-post" style="background-image: url(<?= base_url().'assets/uploads/usuarios/'.$img_usuario ?>)"></div></li>
								<li><p><?= $postagem->nome ?><br><small><?=formata_data_hora($postagem->dth_insert)?></small></p></li>
							</ul>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<?php if($postagem->texto_publicacao): ?>
								<p><?= $postagem->texto_publicacao ?></p>
								<?php endif ?>
								<?php if($postagem->incorpora_video): ?>
								<div class="embed-responsive embed-responsive-16by9">
									<?= $postagem->incorpora_video ?>
								</div>
								<?php endif ?>
								<?php if(count($anexos_img) > 0): ?>
									<?php if(count($anexos_img) > 1): ?>
									<div id="carouselImg<?=$postagem->id_postagem?>" class="carousel slide" data-ride="carousel">
										<div class="carousel-inner">
										<?php foreach($anexos_img as $key => $value): ?>
											<div class="carousel-item <?=($key==0)?'active':''?>" data-interval="3000">
													<img src="<?= base_url().'attachments/'.$value->filename?>" class="d-block w-100">
											</div>
										<?php endforeach ?>
										</div>
										<a class="carousel-control-prev" href="#carouselImg<?=$postagem->id_postagem?>" role="button" data-slide="prev">
											<span class="carousel-control-prev-icon" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
											</a>
											<a class="carousel-control-next" href="#carouselImg<?=$postagem->id_postagem?>" role="button" data-slide="next">
											<span class="carousel-control-next-icon" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
											</a>
									</div>
									<?php else : ?>
										<?php foreach($anexos_img as $img): ?>
												<img src="<?= base_url().'attachments/'.$img->filename?>" class="img-fluid">
										<?php endforeach ?>
									<?php endif ?>
								<?php endif ?>

								<?php if(count($anexos_video)>0): ?>
									<?php foreach($anexos_video as $key => $value): ?>
						    		<div class="embed-responsive embed-responsive-16by9" style="margin-top:20px;margin-bottom:20px;">
										<video controls>
											<source src="<?= base_url().'attachments/'.$value->filename?>" type="<?= $value->file_type ?>">
										</video>
									</div>
						    		<?php endforeach ?>
									
								<?php endif ?>
							</div>
						</div><hr>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<button type="button" onclick="aprovaReprovaPost(1, <?=$postagem->id_postagem?>, '<?=$postagem->nome?>')" class="btn btn-lg btn-block btn-success"><i class="fas fa-thumbs-up"></i>&nbsp;&nbsp;Aprovar</button>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
								<button type="button" onclick="aprovaReprovaPost(2, <?=$postagem->id_postagem?>, '<?=$postagem->nome?>')"  class="btn btn-lg btn-block btn-danger"><i class="fas fa-thumbs-down"></i>&nbsp;&nbsp;Reprovar</button>
							</div>

						</div>
					</div>
				</div>
			<?php endforeach ?>
		<?php else: ?>	
			<div class="card">
				<div class="card-body">
					<h5 class="text-center">Não existem postagens para aprovar...</h5>
				</div>
			</div>
		<?php endif ?>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalAprovar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header modal-ap-rp">
        		<h5 class="modal-title" id="exampleModalLabel"><span id="titulo_modal"></span></h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
      		<form action="<?=base_url().'postagem/aprovar_reprovar'?>" method="post">
	      		<div class="modal-body">
	        		<input type="hidden" name="tipo" id="tipo">
	        		<input type="hidden" name="id_postagem" id="id_postagem">
	        		Deseja realmente <span id="txt_tipo"></span> a postagem de <strong><span id="nome_usuario"></span></strong>?
	      		</div>
	      		<div class="modal-footer">
	      			<div class="col-lg-12">
		      			<div class="row">
			      			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			        			<button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;&nbsp;Sim</button>
			      			</div>
			      			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
			        			<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;&nbsp;Cancelar</button>
			        		</div>
		        		</div>
	        		</div>
	      		</div>	
      		</form>
    	</div>
  	</div>
</div>

<script type="text/javascript">

	function aprovaReprovaPost(tipo, id_postagem, nome_usuario) {

		$("#tipo").val(tipo);
		$("#id_postagem").val(id_postagem);

		if(tipo == 1) {
			$("#txt_tipo").html('aprovar');
			$("#titulo_modal").html('Aprovar postagem');
			
			$("#nome_usuario").html(nome_usuario);
			$("#modalAprovar").modal('show');

		} else if(tipo == 2) {

			$("#txt_tipo").html('reprovar');
			$("#titulo_modal").html('Reprovar postagem');

			$("#nome_usuario").html(nome_usuario);
			$("#modalAprovar").modal('show');
	
		} else {

			alert('Operação inválida');
		}
	}
</script>