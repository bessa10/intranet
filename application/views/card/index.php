<link rel="stylesheet" href="https://use.typekit.net/gin3fzc.css">
<div class="bg_card text-center">
	<img class="img-fluid logo_cartao" src="/assets/img/cartao/logo_movimento.png">
</div>
<div class="container">
	<div class="row">
		<div class="col-md-4 cond_card">
			<h2 class="title_card">Olá a todos!</h2>
			<br>
			<p>Você tem encontrado os valores do Cetrus no seu dia a dia? Queremos te propor a identifica-los em seus colegas e a contar isso a eles, propagando nossa cultura para todos aqui! É simples participar:</p>
			<p>1. Passe o mouse sobre cada um dos cartões para saber mais sobre os valores;</p>
			<p>2. Selecione qual deles quer enviar;</p>
			<p>3. Digite o nome da pessoa que irá receber o cartão;</p>
			<p>4. Escreva uma mensagem explicando porque ela te lembra esse valor;</p>
			<p>5. Clique no botão “gerar o cartão” para conferir o resultado final.</p>
		</div>
	</div>
	<form method="post">
		<div class="row">
			<div class="form-group col-lg-4 form_style">
				<label for="">Nome de quem vai receber</label>
				<input type="text" class="form-control" id="nome_destinarario" required="obrigatorio" name="nome_destinatario">
				<!--<label for="">E-mail de quem vai receber</label>
				<input type="text" class="form-control" id="nome_destinarario" name="email_destinatario"> -->
				<label for="">Mensagem personalizada</label> 
				 <textarea class="form-control" id="exampleFormControlTextarea1" required="obrigatorio" name="mensagem_personalizada" maxlength="200" rows="3"></textarea>
				 <div class="form-group text-center">
					<button type="submit" class="btn btn-primary btn-card">Gerar Cartão</button>
				</div>
			</div>
			<div class="form-group col-lg-8 alinhar_cartoes">
				<h2 class="text-center title_card mb-4">Cartões</h2>
				<div class="row">
					<div class="col-md-6">
						<div class="form-check">
							<div class="row">
  								<label class="form-check-label" for="exampleRadios1">
  								  <img src="assets/img/cartao/comprometimento.png" class="img-fluid">
  								</label>
  							</div>
  							<div class="text-center">
  								<input class="form-check-input" type="radio" required="obrigatorio" name="modelo_cartao" value="comprometimento">
  							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-check">
							<div class="row">
  								<label class="form-check-label" for="exampleRadios1">
  								  <img src="assets/img/cartao/confianca.png" class="img-fluid">
  								</label>
  							</div>
  							<div class="text-center">
  								<input class="form-check-input" type="radio" required="obrigatorio" name="modelo_cartao" value="confianca">
  							</div>
						</div>
					</div>
				</div>
				<div class="row espacamento_cartao">
					<div class="col-md-6">
						<div class="form-check">
							<div class="row">
  								<label class="form-check-label" for="exampleRadios1">
  								  <img src="assets/img/cartao/etica.png" class="img-fluid">
  								</label>
  							</div>
  							<div class="text-center">
  								<input class="form-check-input" type="radio" required="obrigatorio" name="modelo_cartao" value="etica">
  							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-check">
							<div class="row">
  								<label class="form-check-label" for="exampleRadios1">
  								  <img src="assets/img/cartao/excelencia.png" class="img-fluid">
  								</label>
  							</div>
  							<div class="text-center">
  								<input class="form-check-input" type="radio" required="obrigatorio" name="modelo_cartao" value="excelencia">
  							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
