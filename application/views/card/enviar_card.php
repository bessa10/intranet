<link rel="stylesheet" href="https://use.typekit.net/gin3fzc.css">
<div class="bg_card text-center">
	<img class="img-fluid logo_cartao" src="/assets/img/cartao/logo_movimento.png">
</div>
<div class="container">
	<div class="col-md-8 text-center mx-auto">
		<?php include 'card.php'; ?>
		<p class="warning_card">Agora que você personalizou seu cartão, clique no botão de download e envie para quem você escolheu!</p>
		<div class="row alinhar_btn">
			<a id="save" href="#" class="btn btn-card2">download</a>
			<a href="/cartao" class="btn btn-card2">voltar</a>
		</div>
		<!--
		<div class="text-center">
			<a href="#" class="btn btn-card">voltar</a>
		</div>
		-->
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://superal.github.io/canvas2image/canvas2image.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-alpha1/html2canvas.min.js"></script>
<script>
	$('#save').click(function(){
		var elm = $('.bg_cartao').get(0);
		var lebar = "600";
		var tinggi = "450";
		var type = "jpeg";
		var filename = "cartao_<?=$dados_card->nome_remetente?>";
		html2canvas(elm).then(function(canvas){
			var canWidth = canvas.width;
			var canHeight = canvas.height;
			var img = Canvas2Image.convertToImage(canvas, canWidth, canHeight);
			$('#preview').after(img);
			Canvas2Image.saveAsImage(canvas, lebar, tinggi, type, filename);
		})
	})
</script>