<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="row">
		<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
			<h3>Meu perfil</h3><hr>
			<div class="card">
				<form action="<?=base_url().'usuario/perfil'?>" method="post">
					<div class="card-body">
						<?php if($usuario->img_perfil != null): ?>


						<?php endif ?>

						<div class="row">
							<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<label><strong>Nome completo</strong></label>
								<input type="text" class="form-control <?=(form_error('nome'))?'is-invalid':''?>" id="nome" name="nome" placeholder="Nome completo" value="<?=$usuario->nome?>">
		        				<div class="invalid-feedback">
		        					<?= form_error('nome') ?>
		    					</div>
							</div>
							<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<label><strong>Saudação</strong></label>
								<input type="text" class="form-control <?=(form_error('saudacao'))?'is-invalid':''?>" id="saudacao" name="saudacao" placeholder="Saudação" value="<?=$usuario->saudacao?>">
		        				<div class="invalid-feedback">
		        					<?= form_error('saudacao') ?>
		    					</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<label><strong>Usuário</strong></label>
								<input type="text" class="form-control <?=(form_error('usuario'))?'is-invalid':''?>" id="usuario" name="usuario" value="<?=$usuario->usuario?>" readonly="readonly">
		        				<div class="invalid-feedback">
		        					<?= form_error('nome') ?>
		    					</div>
							</div>
							<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<label><strong>E-mail</strong></label>
								<input type="text" class="form-control <?=(form_error('email'))?'is-invalid':''?>" id="email" name="email" placeholder="E-mail" value="<?=$usuario->email?>">
		        				<div class="invalid-feedback">
		        					<?= form_error('email') ?>
		    					</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><strong>Data de nascimento</strong></label>
								<input type="date" class="form-control <?=(form_error('data_nascimento'))?'is-invalid':''?>" id="data_nascimento" name="data_nascimento" value="<?=$usuario->data_nascimento?>">
		        				<div class="invalid-feedback">
		        					<?= form_error('data_nascimento') ?>
		    					</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>