<style type="text/css">
	.tabela-portal thead tr{
		font-size:26px;
		color:#ffffff;
		background-color:#000000;
	}

	.tabela-portal td {
		padding-left: 30px;
	}

	.tabela-portal tbody tr {
		font-size:20px;
	}
</style>
<div class="row">
	<div class="col-lg-12">
		<img src="<?= base_url().'assets/img/logo.png' ?>" class="img-responsive" style="height:50%;margin-left:20px;margin-top:20px; " loading="lazy">
	</div>
	<div class="col-lg-12">
		<table class="table tabela-portal">
			<thead>
				<tr>
					<td>Dashboards</td>
					<td>Requisições</td>
					<td>Pesquisas de Satisfação</td>
					<td>Notícias</td>
					<td>Treinamentos Internos</td>
				</tr>
			</thead>
			<tr>
				<td>
					Atendimento ao Aluno<br>
					Comercial<br>
					Financeiro
				</td>
				<td>
					Facilities<br>
					TI<br>
					Sistemas<br>
					Férias<br>
					Reembolso despesas<br>
					Recebimento fiscal<br>
					Recrutamento e seleção<br>
					Compras
				</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
</div>