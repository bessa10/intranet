<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/dash.css'?>">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

		Highcharts.chart('container', {
		  chart: {
		    type: 'bar',
		    backgroundColor:'#325694'
		  },
		  title: {
		    text: ''
		  },
		  subtitle: {
		    text: ''
		  },
		  xAxis: {
		    categories: [<?= $dados['notas_graf'] ?>],
		    title: {
		      text: null
		    },
		    labels: {
        		style: {
            		color: '#FFFFFF',
            		fontSize:'14px'
        		}
    		}
		  },
		  yAxis: {
		    min: 0,
		    title: {
		      text: '',
		      align: 'high'
		    },
		    labels: {
		      overflow: 'justify',
		      style: {
            		color: 'transparent',
            		fontSize:'14px'
        		}
		    },
		    gridLineColor: 'transparent'
		  },
		  tooltip: {
    		headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    		pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}%</b> dos tickets<br/>'
  			},
		  plotOptions: {
		    bar: {
		      dataLabels: {
		        enabled: true
		      }
		    },
		    series: {
      			borderWidth: 0,
      			dataLabels: {
        			enabled: true,
        			format: '{point.y:.0f}%'
      			}
    		}
		  },
		  legend: {
		    layout: 'vertical',
		    align: 'right',
		    backgroundColor:'#325694',
		    verticalAlign: 'top',
		    x: -30,
		    y: 300,
		    floating: true,
		    borderWidth: 1,
		    itemStyle:{"color": "#FFFFFF", "cursor": "pointer", "font-size" : "14px"},
		    shadow: true
		  },
		  credits: {
		    enabled: false
		  },
		  series: [{
		    name: 'Mês passado',
		    data: [<?= $dados['dados_graf_mp'] ?>],
		    color: '#a9a8b0',
		    borderColor: "#a9a8b0",
			dataLabels:[{
				color: "#FFFFFF",
				borderColor:"#FFFFFF",
				style: {
                	fontSize: '18px',
                	borderColor: "#FFFFFF"
                }
			}]
		  }, {
		    name: 'Mês atual',
		    data: [<?= $dados['dados_graf_ma'] ?>],
		    color: "#4473c5",
		    borderColor: "#4473c5",
		    dataLabels:[{
				color: "#FFFFFF",
				borderColor:"#FFFFFF",
				style: {
                	fontSize: '18px',
                	borderColor: "#FFFFFF",
                }
			}]
		  }]
		});
	});
</script>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-azul-1">
	<div class="row">
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 ">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-azul-2 div-parte-full full-height-tkt-sla">
				<div class="font-1 part-now2 bg-text-now text-center">
					<br><h1>SATISFAÇÃO</h1><hr class="hr_divisor">
					<div class="row">
						<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 text-center">
							<table class="table_sla">
								<tr>
									<td class="number_last3 bg-text-last text-right"><?= $dados['total_tickets_mp'] ?></td>
									<td class="number_now2 bg-text-now"><?= $dados['total_tickets_ma'] ?></td>
								</tr>
								<tr>
									<td colspan="2"><span class="subtitle_sla">TICKETS</span></td>
								</tr>
							</table>
						</div>
						<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 text-center">
							<table class="table_sla">
								<tr>
									<td class="number_last3 bg-text-last text-right"><?= $dados['total_avaliado_mp'] ?></td>
									<td class="number_now2 bg-text-now"><?= $dados['total_avaliado_ma'] ?></td>
								</tr>
								<tr>
									<td colspan="2"><span class="subtitle_sla">AVALIADOS</span></td>
								</tr>
							</table>
						</div>
						<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 text-center">
							<table class="table_sla">
								<tr>
									<td class="number_last3 bg-text-last text-right"><?= formata_porc2($dados['porc_avaliado_mp']).'%' ?></td>
									<td class="number_now2 bg-text-now"><?= formata_porc2($dados['porc_avaliado_ma']).'%' ?></td>
								</tr>
								<tr>
									<td colspan="2"><span class="subtitle_sla">% AVALIADOS</span></td>
								</tr>
							</table>
						</div>
					</div>
					<hr class="hr_divisor">
					<div class="row">
						<div class="col-lg-12">
							<h4 class="text-center">AVALIADOS</h4>
							<figure class="highcharts-figure">
  								<div id="container"></div>
							</figure>
						</div>
					</div>
					<hr class="hr_divisor">
					<div class="row">
						<!-- AVALIADOS -->
						<div class="col-lg-4 text-center">
							<table class="table_sla">
								<tr class="number_now2">
									<td><?= formata_porc2($dados['diferenca_av']).'%' ?></td>
									<td class="text-left"><i class="<?= $dados['icone_av'].' '.$dados['color_av'] ?> "></i></td>
								</tr>
								<tr>
									<td><span class="subtitle_sla">% AVALIADOS</span></td>
									<td></td>
								</tr>
							</table>
						</div>
						<!-- NOTAS 3,4,5 -->
						<div class="col-lg-4 text-center">
							<table class="table_sla">
								<tr class="number_now2">
									<td><?= formata_porc2($dados['diferenca_n1']).'%' ?></td>
									<td class="text-left"><i class="<?= $dados['icone_n1'].' '.$dados['color_n1'] ?> "></i></td>
								</tr>
								<tr>
									<td><span class="subtitle_sla">NOTAS</span><br><strong>5, 4 e 3</strong></td>
									<td></td>
								</tr>
							</table>
						</div>
						<!-- NOTAS 1 e 2 -->
						<div class="col-lg-4 text-center">
							<table class="table_sla">
								<tr class="number_now2">
									<td><?= formata_porc2($dados['diferenca_n2']).'%' ?></td>
									<td class="text-left"><i class="<?= $dados['icone_n2'].' '.$dados['color_n2'] ?> "></i></td>
								</tr>
								<tr>
									<td><span class="subtitle_sla">NOTAS</span><br><strong>1 e 2</strong></td>
									<td></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-azul-2 div-parte-full full-height-tkt-sla">
						<div class="font-1 part-now2 bg-text-now">
							<br><h1 class="text-center">SLA</h1><hr class="hr_divisor">
							<div class="row">
								<div class="col-lg-4 text-center">
									<table class="table_sla">
										<tr>
											<td class="number_last3 bg-text-last text-right"><?= $dados['total_tickets_mp'] ?></td>
											<td class="number_now2 bg-text-now"><?= $dados['total_tickets_ma'] ?></td>
										</tr>
										<tr>
											<td colspan="2">TICKETS</td>
										</tr>
									</table>
								</div>

								<div class="col-lg-4 text-center">
									<table class="table_sla">
										<tr>
											<td class="number_last3 bg-text-last text-right"><?= formata_porc2($dados['porc_tickets_in_mp']).'%' ?></td>
											<td class="number_now2 bg-text-now"><?= formata_porc2($dados['porc_tickets_in_ma']).'%' ?></td>
										</tr>
										<tr>
											<td colspan="2">IN</td>
										</tr>
									</table>
								</div>
								<div class="col-lg-4 text-center">
									<table class="table_sla">
										<tr>
											<td class="number_last3 bg-text-last text-right"><?= formata_porc2($dados['porc_tickets_out_mp']).'%' ?></td>
											<td class="number_now2 bg-text-now"><?= formata_porc2($dados['porc_tickets_out_ma']).'%' ?></td>
										</tr>
										<tr>
											<td colspan="2">OUT</td>
										</tr>
									</table>
								</div>
							</div>
							<hr class="hr_divisor">
							<div class="row">
								<div class="col-lg-12">
									<div class="table-responsive">
										<table class="tabela_values_hh">
											<tr>
												<td></td>
												<td class="text-center bg-text-last">tkt</td>
												<td class="text-center bg-text-last">in</td>
												<td class="text-center bg-text-last border-right">out</td>
												<td class="text-center">tkt</td>
												<td class="text-center">in</td>
												<td class="text-center">out</td>
											</tr>

											<?php 
											foreach ($dados['tabela_cat'] as $row_cat) { 
												if($row_cat['tickets_mp'] > 0 || $row_cat['tickets_ma'] > 0) {
											?>
												<tr class="border-sup" onclick="ticketsCategory('<?=$row_cat['name']?>', <?= $row_cat['cid'] ?>)">
													<td><span class="c-pointer" data-toggle="tooltip" data-placement="right" title="<?= $row_cat['name'] ?>"><?= mb_strtoupper($row_cat['name_abrev'], 'UTF-8') ?></span></td>
													<td class="text-center bg-text-last"><strong><?= $row_cat['tickets_mp'] ?></strong></td>
													<td class="text-center bg-text-last"><strong><?= $row_cat['tickets_in_mp'] ?></strong></td>
													<td class="text-center bg-text-last border-right"><strong><?= $row_cat['tickets_out_mp'] ?></strong></td>
													<td class="text-center"><strong><?= $row_cat['tickets_ma'] ?></strong></td>
													<td class="text-center"><strong><?= $row_cat['tickets_in_ma'] ?></strong></td>
													<td class="text-center"><strong><?= $row_cat['tickets_out_ma'] ?></strong></td>
												</tr>
											<?php
												}
											}
											?>
										</table>
									</div>
								</div>
							</div>
							<hr class="hr_divisor">
							<div class="row">
								<!-- AVALIADOS -->
								<div class="col-lg-4 text-center">
									<table class="table_sla">
										<tr class="number_now2">
											<td><?= formata_porc2($dados['diferenca_tkt']).'%' ?></td>
											<td class="text-left"><i class="<?= $dados['icone_tkt'].' '.$dados['color_tkt'] ?> "></i></td>
										</tr>
										<tr>
											<td><span class="">TICKETS</span></td>
											<td></td>
										</tr>
									</table>
								</div>
								<!-- NOTAS 3,4,5 -->
								<div class="col-lg-4 text-center">
									<table class="table_sla">
										<tr class="number_now2">
											<td><?= formata_porc2($dados['diferenca_in']).'%' ?></td>
											<td class="text-left"><i class="<?= $dados['icone_in'].' '.$dados['color_in'] ?> "></i></td>
										</tr>
										<tr>
											<td><span class="">IN</span></td>
											<td></td>
										</tr>
									</table>
								</div>
								<!-- NOTAS 1 e 2 -->
								<div class="col-lg-4 text-center">
									<table class="table_sla">
										<tr class="number_now2">
											<td><?= formata_porc2($dados['diferenca_out']).'%' ?></td>
											<td class="text-left"><i class="<?= $dados['icone_out'].' '.$dados['color_out'] ?> "></i></td>
										</tr>
										<tr>
											<td><span class="">OUT</span><br></td>
											<td></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>