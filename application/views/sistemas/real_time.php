<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/dash.css' ?>">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-azul-1" style="margin:10px;">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
			<div class="bg-azul-2 div-title-dash mg-rg1 bg-text-now font-1 text-center">
				<div class="title-dash">REAL TIME</div>
			</div>
		</div>
	</div>

	<div class="row mg-rg">
		<div class="list-equipe">
			<?php foreach($dados_geral['dados_pessoa'] as $row_geral) { ?>
			<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 ">
				<div class="bg-azul-2 div-title-dash div-alt-full bg-text-now font-1">
					<div class="row">
						<div class="col-lg-12 text-center"><br>
							<div class="img-equipe" style="background-image: url('<?= base_url().'assets/uploads/usuarios/'.$row_geral['img_perfil'] ?>');">
							</div>
						</div>
					</div><br>
					<div class="row mg-left-equipe">
						<div class="col-lg-12">
							<span class="name_equipe"><i class="fas fa-caret-right"></i>&nbsp;<?= strtoupper($row_geral['name']) ?></span><br>
							<span class="name_equipe"><i class="fas fa-caret-right"></i>&nbsp;<?= strtoupper($row_geral['funcao']) ?></span><br>
							<div class="mg-tkt">
								<span class="tkt_equipe"><?= 'TKT #'.$row_geral['ult_tid'].' - ' . substr($row_geral['ult_title'], 0,70) ?></span>
								<br>
								<?php
									$hs = ($row_geral['dados_hora'] > 1) ? 'HS' : 'H';

									$hs_hj = ($row_geral['dados_hora_hj'] > 1) ? 'HS' : 'H';
								?>
								<span class="tkt_equipe"><?= 'HÁ '.$row_geral['dados_hora'].$hs.' - '.$row_geral['dados_hora_hj'].$hs_hj. ' HOJE';  ?></span>
							</div>
							<br>

							<span class="name_equipe"><i class="fas fa-caret-right"></i>&nbsp;<?= $row_geral['ult_local'] ?></span>
						</div>
					</div>
				</div>
				<div class="bg-azul-2 div-title-dash bg-text-now font-1">
					<div class="mg-left-equipe2 tkt_equipe">
						<?= $row_geral['tickets_abertos'] ?> TKTS OPEN<br>
						<?= $row_geral['tickets_worker_progress'] ?> TKTS WIP<br>
						<?= $row_geral['tickets_atrasados'] ?> TKTS LATE<br>
						<?= $row_geral['dev_worker_progress'] ?> DEV WIP
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>