<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
		<div class="bg-azul-2 div-title-dash bg-text-now font-1 text-center">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 mg-txt-tkt">
					<span class="number_tkt"><?= $dados_totais['total_tickets_abertos'] ?></span><br>
					<span class="title_number_tkt">TKTS OPEN</span>
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6 mg-txt-tkt">
					<span class="number_tkt"><?= $dados_totais['total_tickets_progress'] ?></span><br>
					<span class="title_number_tkt">TKTS WIP</span>
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6 mg-txt-tkt">
					<span class="number_tkt"><?= $dados_totais['total_tickets_atrasados'] ?></span><br>
					<span class="title_number_tkt">TKTS LATE</span>
				</div>

				<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6 mg-txt-tkt">
					<span class="number_tkt"><?= $dados_totais['total_devs_progress'] ?></span><br>
					<span class="title_number_tkt">DEVS WIP</span>
				</div>

				<div class="col-lg-3 col-md-12 col-sm-6 col-xs-12 mg-txt-tkt">
					<span class="number_tkt"><?= $dados_totais['total_fechados_mes'] ?></span><br>
					<span class="title_number_tkt">CLOSED IN THE MONTH</span>
				</div>
			</div>
		</div>
	</div>
</div>
</div>