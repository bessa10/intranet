<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/dash.css' ?>">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-azul-1">
	<div class="row">
		<?php foreach($dados_geral as $row_geral) { ?>
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-azul-2 div-parte-full full-height-equipe">
				<!-- LINHA 1 -->
				<div class="row">
					<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 part-last bg-text-last text-center">
						<span class="title_last font-1">LAST MONTH</span><br>
						<span class="number_last font-1"><?= $row_geral['total_worker_tkt_mp']->total_worker ?></span><br>
						<span class="subtitle_last font-1"><?= $row_geral['funcao'] ?></span><br>
						<div class="float-left pdl-10 font-1">
							<div class="text-center ln_hh">
								<span class="value_hh"><?= $row_geral['total_horas_mp'] ?></span><br>
								<span class="txt_hh">hh full</span>
							</div>
						</div>
						<div class="float-right pdr-10 font-1">
							<div class="text-center ln_hh">
								<span class="value_hh"><?= formata_hs($row_geral['total_horas_prod_mp']) ?></span><br>
								<span class="txt_hh">hh prod</span>
							</div>
						</div>
						<br><br>
						<div class="float-left pdl-10 mgt-5 font-1">
							<div class="text-center ln_hh">
								<span class="signal_value">$</span><span class="valor_rs"><?= formata_preco_dash($row_geral['total_valor_full_mp']) ?></span><span class="signal_value">k</span>
							</div>
						</div>
						<div class="float-right pdr-10  mgt-5 font-1">
							<div class="text-center ln_hh">
								<span class="signal_value">$</span><span class="valor_rs"><?= formata_preco_dash($row_geral['total_valor_prod_mp']) ?></span><span class="signal_value">k</span>
							</div>
						</div>	
						<br>
						<div class="text-center mgt-5">
							<span class="valor_rs"><?= formata_porc($row_geral['porc_geral_mp']) ?></span><span class="signal_value">%</span>
						</div>
						<hr class="d-lg-none d-md-none divisor-hr">
					</div>

					<div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 part-now bg-text-now text-center">
						<span class="title_now">NOW</span><br>
						<span class="number_now"><?= $row_geral['total_worker_tkt_ma']->total_worker ?></span><br>
						<span class="subtitle_now font-1"><?= $row_geral['funcao'] ?></span><br>
						<div class="float-left pdl-10 font-1">
							<div class="text-center">
								<span class="value_hh_now"><?= $row_geral['total_horas_ma'] ?></span><br>
								<span class="txt_hh_now">hh full</span>
							</div>
						</div>
						<div class="float-right pdr-10 font-1">
							<div class="text-center">
								<span class="value_hh_now"><?= formata_hs($row_geral['total_horas_prod_ma']) ?></span><br>
								<span class="txt_hh_now">hh prod</span>
							</div>
						</div>
						<br><br><br>
						<div class="float-left pdl-5 font-1">
							<div class="text-center">
								<span class="signal_value_now">$</span><span class="valor_rs_now"><?= formata_preco_dash($row_geral['total_valor_full_ma']) ?></span><span class="signal_value_now">k</span>
							</div>
						</div>
						<div class="float-right pdr-5 font-1">
							<div class="text-center">
								<span class="signal_value_now">$</span><span class="valor_rs_now"><?= formata_preco_dash($row_geral['total_valor_prod_ma']) ?></span><span class="signal_value_now">k</span>
							</div>
						</div>	
					</div>
				</div>
				<br><br>
				<!-- FIM LINHA 1 -->

				<!-- LINHA 2 -->
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 part-last bg-text-now font-1">
						<small>PRODUCTIVE *</small> 
						<div class="table-responsive">
							<table class="tabela_values_hh">
								<tr>
									<td></td>
									<td class="text-center bg-text-last">$</td>
									<td class="text-center bg-text-last">hs</td>
									<td class="text-center bg-text-last border-right">%</td>
									<td class="text-center">$</td>
									<td class="text-center">hs</td>
									<td class="text-center">%</td>
								</tr>

								<?php 
								foreach ($row_geral['tabela_dash'] as $row_dash) { 
									if($row_dash['porc_hs_mp'] > 0 || $row_dash['porc_hs_ma'] > 0) {
								?>
									<tr class="border-sup">
										<td>
											<?= $row_dash['nome_dash_class'] ?>
											<?= ($row_dash['produtivo'] == 1) ? '<sup>*</sup>' : '' ?>
										</td>
										<td class="text-center bg-text-last"><strong><?= formata_preco_dash($row_dash['dash_valor_mp']) ?></strong></td>
										<td class="text-center bg-text-last"><strong><?= $row_dash['dash_hs_mp'] ?></strong></td>
										<td class="text-center bg-text-last border-right"><strong><?= formata_porc($row_dash['porc_hs_mp']) ?></strong></td>
										<td class="text-center"><strong><?= formata_preco_dash($row_dash['dash_valor_ma']) ?></strong></td>
										<td class="text-center"><strong><?= formata_hs($row_dash['dash_hs_ma']) ?></strong></td>
										<td class="text-center"><strong><?= formata_porc($row_dash['porc_hs_ma'])?></strong></td>
									</tr>
								<?php
									} 
								} 
								?>
								<tr class="border-sup">
									<td>UNPRODUCTIVE</td>
									<td class="text-center bg-text-last"><strong><?= formata_preco_dash($row_geral['valor_imp_mp']) ?></strong></td>
									<td class="text-center bg-text-last"><strong><?= $row_geral['horas_imp_mp'] ?></strong></td>
									<td class="text-center bg-text-last border-right"><strong><?= formata_porc($row_geral['porc_imp_mp']) ?></strong></td>
									<td class="text-center"><strong><?= formata_preco_dash($row_geral['valor_imp_ma']) ?></strong></td>
									<td class="text-center"><strong><?= $row_geral['horas_imp_ma'] ?></strong></td>
									<td class="text-center"><strong><?= formata_porc($row_geral['porc_imp_ma']) ?></strong></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<!-- FIM LINHA 2 -->

				<!-- LINHA 3 -->
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center part-last bg-text-now font-1">
						<span class="title_prod">PRODUTIVIDADE</span><br><br>
						<span class="porc_prod"><?= formata_porc($row_geral['porcentagem_total_ma']) ?></span><span class="signal_porc_prod">%</span>

					</div>
				</div>
				<!-- FIM LINHA 3 -->
			</div>
		</div>
		<?php } ?>
	</div>
</div>