<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/dash.css' ?>">
<link rel="stylesheet" type="text/css" href="../../../assets/css/slick.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/css/slick-theme.css"/>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>


<script>
  $(document).ready(function(){
const chart = Highcharts.chart('container', {
  title: {
    text: 'Monitoramento mensal de oportunidades'
  },
 
  xAxis: {
    categories: ['Total Google','Log Site','Total Adm', 'Total Mkt', 'Total CRM', 'Qtd Erro mkt', 'Qtd Erro CRM']
  },
  series: [{
    type: 'column',
    colorByPoint: true,
    data: [<?= $grafico_pizza['dados'][0]['QTD_OPORTUNIDADES_GOOGLE'] ?>,<?= $grafico_pizza['dados'][0]['TOTAL_DE_LOGS'] ?>, <?= $grafico_pizza['dados'][0]['TOTAL_DE_OPORTUNIDADES'] ?>, <?= $grafico_pizza['dados'][0]['QTD_MKT_NO_MES'] ?>, <?= $grafico_pizza['dados'][0]['QTD_CRM_NO_MES'] ?>],
    showInLegend: false
  }]
});

Highcharts.chart('container2', {

  title: {
    text: 'Monitoramento diário de oportunidades'
  },

  yAxis: {
    title: {
      text: 'Número de oportunidades'
    }
  },

  xAxis: {
    accessibility: {
      rangeDescription: 'Range: 1 a 30'
    }
  },

   xAxis: {
        categories: [
        <?
        for($i = 0; $i < count($grafico_detalhado['dados']); $i++){
      $string_dia = $grafico_detalhado['dados'][$i]['DIA'];
      $string = $string_dia;
      $dia_convertido = strtotime($string);
      $somente_dia = date("d",$dia_convertido);
      echo $somente_dia.',';
      
      }

    ?>

        ]
    },

  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle'
  },

  plotOptions: {
    series: {
      label: {
        connectorAllowed: false
      }
    }
  },

  series: [{
    name: 'Total Google',
    data: [<?php 
    for($i = 0; $i < count($grafico_detalhado['dados']); $i++){
    echo $grafico_detalhado['dados'][$i]['QTD_GOOGLE'].',';
  } ?>]
  },{
    name: 'Log Site',
    data: [<?php 
    for($i = 0; $i < count($grafico_detalhado['dados']); $i++){
    echo $grafico_detalhado['dados'][$i]['QTD_LOG'].',';
  } ?>]
  }, {
    name: 'Total Adm',
    data: [<?php 
    for($i = 0; $i < count($grafico_detalhado['dados']); $i++){
    echo $grafico_detalhado['dados'][$i]['QTD_FORM'].',';
  } ?>]
  }, {
    name: 'Total Mkt',
    data: [<?php 
    for($i = 0; $i < count($grafico_detalhado['dados']); $i++){
    echo $grafico_detalhado['dados'][$i]['QTD_MKT'].',';
  } ?>]
  }, {
    name: 'Total CRM',
    data: [<?php 
    for($i = 0; $i < count($grafico_detalhado['dados']); $i++){
    echo $grafico_detalhado['dados'][$i]['QTD_CRM'].',';
  } ?>]
  },],

  responsive: {
    rules: [{
      condition: {
        maxWidth: 500
      },
      chartOptions: {
        legend: {
          layout: 'horizontal',
          align: 'center',
          verticalAlign: 'bottom'
        }
      }
    }]
  }

});
	  setTimeout(function() {
		  window.location.reload(1);
	  }, 360000);
  });
  </script>
<style>
  .alinhamento_dash{
    width: 100%;
    justify-content: space-between;
    margin: 0 auto; 
  }
  .div_alinhamento{
    width: 19%;
  }
 #container {
  height: 400px; 
}
#container rect.highcharts-background {
    fill: #325694 !important;
}

#container2 rect.highcharts-background {
    fill: #ffffff !important;
}
#container text.highcharts-title,#container text{
    color: #fff !important;
    fill: #fff !important;
}

#container2 text.highcharts-title,#container2 text{
    color: #000 !important;
    fill: #000 !important;
}


.highcharts-figure, .highcharts-data-table table {
  min-width: 100%; 
  max-width: 100%;
  margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
.highcharts-credits{
  display: none;
}
path.highcharts-label-box.highcharts-tooltip-box.highcharts-shadow {
    fill: #000 !important;
    opacity: 1 !important;
}

.highcharts-figure2, .highcharts-data-table table {
    min-width: 100%;
    max-width: 100%;
    margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
.modal-dialog{
    max-width: 50% !important;
    margin: 1.75rem auto;
}
</style>
<body>

<!--<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel"> -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="container-fluid">
       <h2 class="text-center mt-4" style="color: #fff; margin-bottom: 40px;">Monitoramento real-time de oportunidades</h2>
       <div class="row alinhamento_dash">
         <div class="bg-text-now div_alinhamento text-center">
          <div class="div-parte-full bg-azul-2 alinhar_font" style="background-color: #fff">
            <img class="img-fluid" style="width: 50%" src="../../../assets/img/google-logo.png">
            <iframe width="238" height="132" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTNiyeI-MB1q5gmpZ5h98BenSyZYdSW6wZ_BpiC72PJOvnOdHYw_e4HmQ85ad5zRSjoNwVU8qLVY4ij/pubchart?oid=341547035&amp;format=interactive"></iframe>
          </div>
        </div>
        <div class="bg-text-now div_alinhamento text-center">
          <div class="div-parte-full bg-azul-2 alinhar_font">
            <span class="number_dash font-1"><?= $consul_dados['dados'][0]['QTD_LOG'] ?></span>
            <?= $validacao_value; ?>
            <p class="title_dash">Log Site</p>
          </div>
        </div>
        <div class="bg-text-now div_alinhamento text-center">
          <div class="div-parte-full bg-azul-2 alinhar_font">
            <span class="number_last2 text-warning font-1" data-toggle="tooltip" data-placement="bottom" title="Oportunidades Fakes"><?= $oportunidades_erro->total ?></span>
            <a data-toggle="modal" data-target="#log_site"><span class="number_dash font-1"><?= $consul_dados['dados'][0]['QTD_FORM'] ?></span></a>
              <?= $validacao_value1; ?>
            <p class="title_dash">Adm Site</p>
          </div>
        </div>
        <div class="bg-text-now div_alinhamento text-center">
          <div class="div-parte-full bg-azul-2 alinhar_font">
            <a data-toggle="modal" data-target="#log_mkt"><span class="number_dash font-1"><?= $consul_dados['dados'][0]['QTD_MKT'] ?></span></a>
             <?= $validacao_value2; ?>
            <p class="title_dash">RD Marketing</p>
          </div>
        </div>
        <div class="bg-text-now div_alinhamento text-center">
          <div class="div-parte-full bg-azul-2 alinhar_font">
            <span class="number_last2 bg-text-last font-1" data-toggle="tooltip" data-placement="bottom" title="Oportunidades Manuais"><?= $oportunidades_manuais->total ?></span>
            <a data-toggle="modal" data-target="#log_crm"><span class="number_dash font-1"><?= $consul_dados['dados'][0]['QTD_CRM'] ?></span></a>
              <?= $validacao_value3; ?>
            <p class="title_dash">RD CRM</p>
          </div>
        </div>
       </div>
        <div class="row">
          <div class="col-md-8">
              <figure class="highcharts-figure2">
                <div id="container2"></div>  
              </figure>
          </div>
          <div class="col-md-4">
              <figure class="highcharts-figure">
                <div id="container"></div>  
              </figure>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="container-fluid">
         <h2 class="text-center" style="color: #fff">Monitoramento real-time dos serviços</h2>
        <div class="row">
          <div class="col-12 col-md-6 offset-md-3">
            <div class="bg-text-now text-center">
              <div class="div-parte-full bg-azul-2 alinhar_font">

                <?
                for($i = 0; $i < count($retorno_servicos['dados']); $i++){?>
                <div class="espacamento-servicos">
                  <div class="servico">
                    <h2 class="text-left title_dash"><?= $retorno_servicos['dados'][$i]['SERVICO'] ?></h2>
                  </div>
                  <div class="verificar">
                    <? if($retorno_servicos['dados'][$i]['STATUS'] == 'ONLINE'){
                      $cor_status = 'green';
                    }else{
                      $cor_status = 'red';
                    }?>
                    <i class="fas fa-circle pulse status_dash" style="color: <?= $cor_status ?>"></i>
                  </div>
                </div> 
                <? } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
        
</body>


<!-- Modal -->
<div class="modal fade" id="log_transations" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="log_site" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<table class="table">
  <thead>
    <tr>
      <th scope="col">Nome</th>
      <th scope="col">Email</th>
      <th scope="col">Curso</th>
      <th scope="col">Turma</th>
    </tr>
  </thead>
  <tbody>
  	<?php for($i = 0; $i < count($retorno_site); $i++){ ?>
    <tr>
      <th scope="row"><?= $retorno_site[$i]->nome ?></th>
      <td><?= $retorno_site[$i]->email ?></td>
      <td><?= $retorno_site[$i]->nome_pt ?></td>
      <td><?= $retorno_site[$i]->turma ?></td>
    </tr>
    <?php } ?>   
  </tbody>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="log_mkt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<table class="table">
  <thead>
    <tr>
      <th scope="col">Nome</th>
      <th scope="col">Email</th>
      <th scope="col">Curso</th>
      <th scope="col">Turma</th>
    </tr>
  </thead>
  <tbody>
  	<?php for($i = 0; $i < count($retorno_mkt); $i++){ ?>
    <tr>
      <th scope="row"><?= $retorno_site[$i]->nome ?></th>
      <td><?= $retorno_mkt[$i]->email ?></td>
      <td><?= $retorno_mkt[$i]->nome_pt ?></td>
      <td><?= $retorno_mkt[$i]->turma ?></td>
    </tr>
    <?php } ?>   
  </tbody>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="log_crm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<table class="table">
  <thead>
    <tr>
      <th scope="col">Nome</th>
      <th scope="col">Email</th>
      <th scope="col">Curso</th>
      <th scope="col">Turma</th>
    </tr>
  </thead>
  <tbody>
  	<?php for($i = 0; $i < count($retorno_crm); $i++){ ?>
    <tr>
      <th scope="row"><?= $retorno_site[$i]->nome ?></th>
      <td><?= $retorno_crm[$i]->email ?></td>
      <td><?= $retorno_crm[$i]->nome_pt ?></td>
      <td><?= $retorno_crm[$i]->turma ?></td>
    </tr>
    <?php } ?>   
  </tbody>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="mkt_error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
  <thead>
    <tr>
      <th scope="col">Nome</th>
      <th scope="col">Email</th>
      <th scope="col">Curso</th>
      <th scope="col">Delay</th>
    </tr>
  </thead>
  <tbody>
    <?php for($i = 0; $i < count($log_erro_mkt); $i++){ ?>
    <tr>
      <th scope="row"><?= $retorno_site[$i]->nome ?></th>
      <td><?= $log_erro_mkt[$i]->email ?></td>
      <td><?= $log_erro_mkt[$i]->nome_pt ?></td>
      <td><?= $log_erro_mkt[$i]->Delay_Oportunidade ?></td>
    </tr>
    <?php } ?>   
  </tbody>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="crm_error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
  <thead>
    <tr>
      <th scope="col">Nome</th>
      <th scope="col">Email</th>
		<th scope="col">Nome do curso</th>
      <th scope="col">Curso</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($erro_crm_relacao_mkt as $crm){ ?>
    <tr>
      <th scope="row"><?= $crm->nome; ?></th>
      <td><?= $crm->email; ?></td>
      <td><?= $crm->nome_pt; ?></td>
		<td><?= $crm->curso; ?></td>
    </tr>
    <?php } ?>   
  </tbody>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>


	
