<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/dash.css'?>">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

		Highcharts.chart('container', {
		  chart: {
		    type: 'bar',
		    backgroundColor:'#325694'
		  },
		  title: {
		    text: ''
		  },
		  subtitle: {
		    text: ''
		  },
		  xAxis: {
		    categories: [<?= $dados['sigla_prj_graf'] ?>],
		    title: {
		      text: null
		    },
		    labels: {
        		style: {
            		color: '#FFFFFF',
            		fontSize:'14px'
        		}
    		}
		  },
		  yAxis: {
		    min: 0,
		    title: {
		      text: '',
		      align: 'high'
		    },
		    labels: {
		      overflow: 'justify',
		      style: {
            		color: 'transparent',
            		fontSize:'14px'
        		}
		    },
		    gridLineColor: 'transparent'
		  },
		  tooltip: {
    		headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    		pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.1f}k</b> dos tickets<br/>'
  			},
		  plotOptions: {
		    bar: {
		      dataLabels: {
		        enabled: true
		      }
		    },
		    series: {
		    	shadow:false,
                borderWidth:.8,
                pointPadding:.1,
                groupPadding:0,
      			borderWidth: 0,
      			dataLabels: {
        			enabled: true,
        			format: '${point.y:.1f}k'
      			}
    		}
		  },
		  legend:false,
		  credits: {
		    enabled: false
		  },
		  series: [
		  {
		    name: 'Mês atual',
		    data: [<?= $dados['valor_dev_graf'] ?>],
		    color: "#4473c5",
		    borderColor: "#4473c5",
		    dataLabels:[{
				color: "#FFFFFF",
				borderColor:"#FFFFFF",
				style: {
                	fontSize: '16px',
                	borderColor: "#FFFFFF",
                }
			}]
		  }]
		});
	});
</script>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-azul-1">
	<div class="row">
		<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 ">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-azul-2 div-parte-full full-height-tkt-dev">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 part-last bg-text-last text-center">
						<span class="title_last font-1">LAST MONTH</span><br>
						<span class="number_last2 font-1"><?= $dados['total_tkt_dep_mp'] ?></span>
						<br><br>
						<span class="number_last2 font-1"><?= $dados['total_hs_dep_mp'] ?></span>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-6 col-xs-6 part-now2 bg-text-now text-center">
						<span class="title_now2 font-1">NOW</span><br>
						<span class="number_now font-1"><?= $dados['total_tkt_dep_ma'] ?></span>
						<br><span class="subtitle font-1">tickets</span><br><br>
						<span class="number_now font-1"><?= $dados['total_hs_dep_ma'] ?></span><br>
						<span class="font-1">horas</span>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 part-now bg-text-now">
						<span class="font-1"><small>DEPARTAMENTOS</small></span>
						<div class="table-responsive">
							<table class="tabela_values_hh">
								<tr>
									<td></td>
									<td class="text-center bg-text-last">tkt</td>
									<td class="text-center bg-text-last border-right">hs</td>
									<td class="text-center">tkt</td>
									<td class="text-center">hs</td>
								</tr>
								<?php 
								foreach ($dados['tabela_dep'] as $row_dep) { 
									// Calculando porcentagem
									$porc_hs_mp = ($row_dep['hs_mp'] * 100) / $dados['total_hs_dep_mp'];

									$porc_hs_ma = ($row_dep['hs_ma'] * 100) / $dados['total_hs_dep_ma'];

									if($porc_hs_mp > 0 || $porc_hs_ma > 0) {
								?>
									<tr class="border-sup" onclick="ticketsDepartment('<?= $row_dep['nome_dep'] ?>', <?= $row_dep['id_department'] ?>)">
										<td><span class="c-pointer" data-toggle="tooltip" data-placement="right" title="<?= $row_dep['nome_dep'] ?>"><?= mb_strtoupper($row_dep['sigla_dep'], 'UTF-8') ?></span></td>
										<td class="text-center bg-text-last"><strong><?= $row_dep['tickets_mp'] ?></strong></td>
										<td class="text-center bg-text-last border-right"><strong><?= $row_dep['hs_mp'] ?></strong></td>
										<td class="text-center"><strong><?= $row_dep['tickets_ma'] ?></strong></td>
										<td class="text-center"><strong><?= formata_hs($row_dep['hs_ma'] ) ?></strong></td>
									</tr>
								<?php
									}
								} 
								?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-azul-2 div-parte-full bg-text-now font-1 full-height-tkt-dev-2">
				<div class="row">
					<div class="col-lg-4  part-last bg-text-last text-center">
						<span class="title_last font-1">LAST MONTH</span><br>
						<span class="signal_value">$</span><span class="valor_rs"><?= formata_preco_dash($dados['total_valor_dep_mp'])?></span><span class="signal_value">k</span>
					</div>
					<div class="col-lg-4 part-now2 bg-text-now text-center">
						<span class="title_now2 font-1">NOW</span><br>
						<span class="signal_value_now2">$</span><span class="valor_rs_now2"><?= formata_preco_dash($dados['total_valor_dep_ma']) ?></span><span class="signal_value_now2">k</span><br>
						<span class="font-1">hh interno</span><br>
						<span class="font-1">TICKETS</span>
					</div>
				</div><br>
				<div class="row">
					<div class="col-lg-12 text-center">
						<span class="font-1">departamentos</span><hr class="hr_divisor">
						<div class="table-responsive">
							<table class="tabela_values_hh">
								<tr>
									<?php 
										foreach($dados['tabela_top_dep'] as $top_dep){ 

											$porc_hs = ($top_dep['hs_total'] * 100) / $dados['total_hs_dep_ma'];

											$porc_valor = ($top_dep['valor_total'] * 100) / $dados['total_valor_dep_ma'];
									?>
										<td style="line-height:25px;">
											<span class="c-pointer font-1" data-toggle="tooltip" data-placement="right" title="<?= $top_dep['name_dep'] ?>"><?= $top_dep['sigla_dep'] ?></span><br>
											<strong><?= formata_porc($porc_hs).'%' ?></strong>
										</td>
									<?php } ?>
								</tr>
							</table>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12 text-center">
						<span class="font-1">Sistemas</span><hr class="hr_divisor">
						<div class="table-responsive">
							<table class="tabela_values_hh">
								<tr>
									<?php 
										foreach($dados['tabela_top_prj'] as $top_prj) {

											$porc_hs = ($top_prj['hs_total'] * 100) / $dados['total_hs_prj'];
										 
											$porc_valor = ($top_prj['valor_total'] * 100) / $dados['total_valor_prj'];
									?>
										<td style="line-height:25px;">
											<span class="c-pointer font-1" data-toggle="tooltip" data-placement="right" title="<?= $top_prj['name_project'] ?>"><?= $top_prj['sigla_project'] ?></span><br>
											<strong><?= formata_porc($porc_hs).'%' ?></strong>
										</td>
									<?php } ?>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-azul-2 div-parte-full bg-text-now font-1 full-height-tkt-dev-2">
				<div class="row">
					<div class="col-lg-12 part-now bg-text-now">
						<h4 class="text-center">Tipos de tickets</h4>
						<div class="table-responsive">
							<table class="tabela_values_hh">
								<tr>
									<td></td>
									<td class="text-center bg-text-last">tkt</td>
									<td class="text-center bg-text-last">$</td>
									<td class="text-center bg-text-last border-right">hs</td>
									<td class="text-center">tkt</td>
									<td class="text-center">$</td>
									<td class="text-center">hs</td>
								</tr>

								<?php 
								foreach ($dados['tabela_cat'] as $row_cat) { 

									//var_dump($row_cat);die;
									if($row_cat['hs_mp'] > 0 || $row_cat['hs_ma'] > 0) {
								?>
									<tr class="border-sup" onclick="ticketsCategory('<?=$row_cat['name']?>', <?= $row_cat['cid'] ?>)">
										<td><span class="c-pointer" data-toggle="tooltip" data-placement="right" title="<?= $row_cat['name'] ?>"><?= mb_strtoupper($row_cat['name_abrev'], 'UTF-8') ?></span></td>
										<td class="text-center bg-text-last"><strong><?= $row_cat['tickets_mp'] ?></strong></td>
										<td class="text-center bg-text-last"><strong><?= formata_preco_dash($row_cat['valor_mp']) ?></strong></td>
										<td class="text-center bg-text-last border-right"><strong><?= $row_cat['hs_mp'] ?></strong></td>
										<td class="text-center"><strong><?= $row_cat['tickets_ma'] ?></strong></td>
										<td class="text-center"><strong><?= formata_preco_dash($row_cat['valor_ma']) ?></strong></td>
										<td class="text-center"><strong><?= formata_hs($row_cat['hs_ma']) ?></strong></td>
									</tr>
								<?php
									}
								} 
								?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 full-height">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-azul-2 div-parte-full bg-text-now font-1 full-height-tkt-dev">
				<div class="row">
					<div class="col-lg-4 part-last bg-text-last text-center">
						<span class="title_last">LAST MONTH</span><br>
						<span class="signal_value">$</span><span class="valor_rs"><?= formata_preco_dash($dados['total_valor_dev_mp'])?></span><span class="signal_value">k</span><br>

						<span class="number_last2"><?= $dados['total_horas_dev_mp'] ?></span><br>
						<span class="font-1">horas</span>
					</div>

					<div class="col-lg-4 part-now2 bg-text-now text-center">
						<span class="title_now2 font-1">NOW</span><br>
						<span class="signal_value_now2">$</span><span class="valor_rs_now2"><?= formata_preco_dash($dados['total_valor_dev_ma']) ?></span><span class="signal_value_now2">k</span><br>
						<span class="font-1 txt_lbl_tkt">hh interno</span><br>
					</div>

					<div class="col-lg-4 part-now2 bg-text-now text-center">
						<div class="mg-hour-tkt point" onclick="devsEntreguesMes('<?=$dados_data["mes_atual"] ?>', '<?=$dados_data["ano_mes_atual"] ?>')">
							<span class="hour_now_tkt"><?= $dados['total_horas_dev_ma'] ?></span><br>
							<span class="font-1 txt_lbl_tkt">horas</span>
						</div>
						<div class="mg-hour-tkt point" onclick="devsEntreguesMes('<?=$dados_data["mes_atual"] ?>', '<?=$dados_data["ano_mes_atual"] ?>')">
							<span class="hour_now_tkt"><?= $dados['devs_entregues_ma'] ?></span><br>
							<span class="font-1 txt_lbl_tkt">entregue mês</span>
						</div>
					</div>
				</div>
				<hr class="hr_divisor">
				<div class="row">
					<div class="col-lg-12 bg-text-now">
						<div class="text-center">
							<span class="font-1">demandas X entregas</span>
						</div>

						<div class="table-responsive">
							<table class="tabela_values_hh text-center">
								<?php foreach ($dados['list_tickets_status'] as $row_status) { ?>
									<td class="text-center">
										<span class="number_now2"><?= $row_status->total ?></span><br>
										<span class="font-1"><?= $row_status->label_dash ?></span>
									</td>
								<?php } ?>

							</table><hr class="hr_divisor">
							<br><p class="text-left">NÃO ENTREGUES ATÉ O MOMENTO</p><br>

							<figure class="highcharts-figure">
  								<div id="container"></div>
							</figure>


							<!--
							<table class="tabela_values_hh">
							<?php foreach ($dados['tabela_tkt_dev_fila'] as $row_tkt) { ?>
							<tr>
								<td style="width:8%">
									<span data-toggle="tooltip" data-placement="right" title="<?= $row_tkt['name_project'] ?>"><?= $row_tkt['sigla_project'] ?></span>
								</td>
								<td class="text-right" style="width:100%">
									<?php $widht_hs = (($row_tkt['hs']*100) / $dados['total_horas_tkt_dev'])+40; ?>
									<div class="barra_horas" style="padding:0.5rem;background-color:#4473c5;width:<?= $widht_hs.'%'?>">
										<span data-toggle="tooltip" data-placement="right" title="horas previstas"><?= $row_tkt['hs']?>hh</span>
										<span style=""><?= formata_preco_dash($row_tkt['valor_total']).'k' ?></span>
									</div>
								</td>
							</tr>
							<?php } ?>
							</table>
							-->
						</div>
						<!--
						<div class="w3-light-grey">
  							<div class="w3-grey" style="height:24px;width:50%"></div>
						</div>
						-->
						<br><br>
						<div class="text-center">
							<span class="signal_value_now2">$</span><span class="valor_rs_now2"><?= formata_preco_dash($dados['total_valor_tkt_dev']) ?></span><span class="signal_value_now2">k</span><br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bd-example-modal-lg" id="modalTickets" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            	<h5><span id="txt_modal_tkt"></span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div id="div_tickets_table"></div>
            </div>
        </div>
    </div>
</div>