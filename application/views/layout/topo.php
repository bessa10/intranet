<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="<?=base_url().'assets/img/favicon/favicon.ico'?>" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/vendor/bootstrap4.5/css/bootstrap.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/style.css' ?>">
    <link href="<?=base_url().'assets/vendor/fontawesome/'?>css/fontawesome.css" type="text/css" rel="stylesheet"/>
    <!--<link href="<?=base_url().'assets/vendor/animate/'?>animate.css" rel="stylesheet" type="text/css"> --> 
	  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@yaireo/tagify@3.1.0/dist/tagify.css" />
	  <!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->
	  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">-->
	  <!--<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet"/>-->

    <link href="<?= base_url() ?>assets/vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/vendor/owl-carousel/owl.theme.default.min.css" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/css/datatables.css' ?>">
    <title>Intranet Cetrus</title>
  </head>
  <body>
