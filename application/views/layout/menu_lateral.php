<ul  class="nav nav-pills" style="flex-direction: column;width: 70%;box-shadow: 4px 0 6px -5px #808080;">
	<li class="header-menu">
		Novo Curso
	</li>
	<li class="block-menu curso-menu">
		<table>
			<tr>
				<td><a href="add_curso">1 - Nomenclatura e versão</a></td>
				<td><span id="ok-curso"><i class="fa fa-check text-white"></i></span></td>
			</tr>
		</table>
	</li>
	<li class="block-menu docente-menu">
		<table>
			<tr>
				<td><a href="add_docente">2 - Coordenadores</a></td>
				<td><span id="ok-docente"><i class="fa fa-check text-white"></i></span></td>
			</tr>
		</table>
	</li>
	<li class="block-menu cat-menu">
		<table>
			<tr>
				<td><a href="#3a">3 - Categorização</a></td>
				<td><span id="ok-categoria"><i class="fa fa-check text-white"></i></span></td>
			</tr>
		</table>
	</li>
	<li class="block-menu mkt-menu">
		<div class="dropdown show">
		<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			4 - Marketing e Comercial
		</a>
		<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
			<a class="block-menu dropdown-item pa-menu" href="#">4.1 Publico Alvo</a>
			<a class="block-menu dropdown-item persona-menu" href="#">4.2 Persona</a>
			<a class="block-menu dropdown-item relevancia-menu" href="#">4.3 Relevância</a>
		</div>
		</div>
	</li>
	<li class="block-menu acad-menu">
		<a href="#5a">5 - Acadêmico</a>
	</li>
	<li class="block-menu planejamento-menu">
		<a href="#6a">6 - Planejamento</a>
	</li>
	<li class="block-menu plano-menu">
		<a href="#7a">7 - Plano de ensino</a>
	</li>
	<li class="block-menu custos-menu">
		<a href="#8a">8 - Custos</a>
	</li>
	<li class="block-menu precificacao-menu">
		<a href="#9a">9 - Precificação</a>
	</li>
</ul>
