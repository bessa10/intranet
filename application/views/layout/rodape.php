</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="<?=base_url().'assets/vendor/fontawesome/'?>js/fontawesome.js"></script>
	<script src="<?php echo base_url().'assets/'?>vendor/ckeditor_basic/ckeditor.js"></script>
	<script src="<?=base_url().'assets/vendor/fontawesome/'?>js/be1cab109c.js"></script>
	<script src="<?=base_url().'assets/js/functions.js'?>"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="<?=base_url().'assets/js/helper.js'?>"></script>
	<script src="<?=base_url().'assets/js/modal_tickets.js'?>"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/@yaireo/tagify@3.1.0/dist/tagify.min.js"></script>

	<!-- jQuery library -->
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<!--<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>-->

	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="<?= base_url() ?>assets/vendor/owl-carousel/owl.carousel.min.js"></script>

	<script src="<?=base_url().'assets/js/datatables.js'?>"></script>
	<script src="<?=base_url().'assets/js/tables.js'?>"></script>
	<script src="<?=base_url().'assets/js/dash.js'?>"></script>

	<script src="<?=base_url().'assets/vendor/ckeditor/ckeditor.js'?>"></script>

	<script type="text/javascript">
  		$(function () {
  			$('[data-toggle="tooltip"]').tooltip();

  			$('.carousel').carousel();
		});
		$('.ui.dropdown').dropdown();
		$(document).ready( function () {
			$('#myTable').DataTable();
		} );

		CKEDITOR.replace('texto_publicacao',{
			height:200
		});
	</script>
  


  </body>
</html>
