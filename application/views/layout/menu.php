<?php $dados_usuario = get_usuario(); ?>
<style type="text/css">
    .btn-link-drop {
      font-size:18px;
      font-weight:bold;
      font-weight:500;
    }
</style>
<?php
    $bg_menu = (ENVIRONMENT == 'production') ? 'bg-dark' : 'bg-secondary';

    if(!isset($filtro)) {
        $filtro['mes_ano'] = date('m/Y');
    }

    $departamentos = get_menu();
?>

<div class="menu-top">
    <nav class="navbar navbar-expand-lg navbar-dark <?= $bg_menu ?> d-none d-sm-block" id="navbar">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button> 
        <div class="collapse navbar-collapse menu-top" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto text-center">
                <li class="nav-item active">
                    <h5><a class="nav-link" href="<?= base_url() ?>"><img id="logo" src="<?=base_url().'assets/img/logo_branco_cetrus.png'?> " height="50"></a></h5>
                </li>
                
                <?php if($departamentos): ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle btn-link-drop" href="#" id="navbarDashboards" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="icon-menu" id="icon-sticky">
                            <i class="fas fa-tachometer-alt"></i>
                        </span>
                    </a>
                    <span class="label-icon-menu" id="text-sticky">Dashboards</span>
                    <div class="dropdown-menu" aria-labelledby="navbarDashboards">
                    <?php foreach($departamentos as $row): ?>
                        <a class="dropdown-item" href="<?= base_url().$row->url_dep ?>"><?= $row->nome_dep ?></a>
                    <?php endforeach ?>
                    </div>
                </li>
                <?php endif ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle btn-link-drop" href="#" id="navbarRequisicoes" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="icon-menu" id="icon-sticky2">
                            <i class="fas fa-paste"></i>
                        </span>
                    </a>
                    <span class="label-icon-menu" id="text-sticky2">Requisições</span>
                    <div class="dropdown-menu" aria-labelledby="navbarRequisicoes">
                        <a class="dropdown-item" target="_blank" href="http://177.47.1.123:9011/ContractOne.App/">Compras</a>
                        <a class="dropdown-item" target="_blank" href="<?= URL_FACILITIES ?>">Facilities</a>
                        <a class="dropdown-item" target="_blank" href="<?= URL_PROJ_ESTR ?>">Gestão e Projetos</a>
                        <a class="dropdown-item" target="_blank" href="<?= URL_INT_DADOS ?>">Inteligência de dados</a>
                        <a class="dropdown-item" target="_blank" href="<?= URL_TI ?>">TI</a>
                        <a class="dropdown-item" target="_blank" href="<?= URL_SISTEMAS ?>">Sistemas</a>
                    </div>
                </li>
                 <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle btn-link-drop" href="#" id="navbarColaboradores" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="icon-menu" id="icon-sticky2">
                            <i class="fas fa-user-friends"></i>
                        </span>
                    </a>
                    <span class="label-icon-menu" id="text-sticky2">Colaboradores</span>
                    <div class="dropdown-menu" aria-labelledby="navbarColaboradores">
                        <a class="dropdown-item" target="_blank" href="/cartao">Cartão</a>
                    </div>
                </li>
                <?php if (isset($dados_usuario)): ?>
                <li class="nav-item dropdown" style="display:none">
                    <a class="nav-link dropdown-toggle btn-link-drop" href="#" id="navbarPesquisas" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="icon-menu" id="icon-sticky3">
                            <i class="fas fa-file-signature"></i>
                        </span>
                    </a>
                    <span class="label-icon-menu" id="text-sticky3">Pesquisas</span>
                    <div class="dropdown-menu" aria-labelledby="navbarPesquisas">
                        <?php if($dados_usuario->id_departamento==4): ?>
                        <a class="dropdown-item" href="<?= URL_SISTEMAS.'research/all' ?>">Sistemas</a>
                        <?php endif ?>
                    </div>
                </li>
                <?php endif ?>

                <?php if(ENVIRONMENT != 'production'): ?>
                    <?php if(isset($dados_usuario)): ?>
        				<?php if($dados_usuario->id_grupo==1 || $dados_usuario->id_grupo==5): ?>
        				<li class="nav-item dropdown">
        					<a class="nav-link dropdown-toggle btn-link-drop" href="#" id="navbarPlanejamento" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="icon-menu" id="icon-sticky4">
                                    <i class="fas fa-calendar"></i>
                                </span>
        					</a>
        					<span class="label-icon-menu" id="text-sticky4">Produtos</span>
        					<div class="dropdown-menu" aria-labelledby="navbarPlanejamento">
        						<a class="dropdown-item" href="<?=base_url().'produto/cursos'?>">Cursos</a>
        						<a class="dropdown-item" href="<?=base_url().'produto/disciplinas'?>">Disciplinas</a>
        					</div>
        				</li>
        				<?php endif ?>
                    <?php endif ?>
                <?php endif ?>
                <!--
                <li class="nav-item menu-item">
                    <a class="nav-link btn-link-drop" href="#" data-toggle="tooltip" data-placement="bottom" title="Treinamentos internos"><span class="icon-menu"><i class="fas fa-graduation-cap"></i></span></a>
                </li>
                -->
            </ul>
            
            <div class="form-inline my-2 my-lg-0 adjust-menu">
                <ul class="navbar-nav mr-auto text-center">
                    
                    <?php if($dados_usuario->id_grupo==1||$dados_usuario->id_grupo==4): ?>
                    <!--
                    <li class="nav-item menu-item">
                        <a class="nav-link btn-link-drop " href="<?=base_url().'postagem/aguardando'?>">
                        <span class="icon-menu" id="icon-sticky6"><i class="fas fa-check-square"></i></span>
                        <?php if(countPostagensAprovar()>0){ ?>
                        <span data-toggle="tooltip" data-placement="top" title="<?= countPostagensAprovar() ?> Postagen(s) aguardando aprovação"><sup><span class="badge badge-success"><?= countPostagensAprovar() ?></span></sup></span>
                        <?php } ?>
                        </a>
                        <span class="label-icon-menu" id="text-sticky6">Aprovação</span>
                    </li>
                    -->
                    <?php endif ?>
                    <?php if(ENVIRONMENT != 'production'): ?>
                    <!--
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle btn-link-drop" href="#" id="navbarPesquisas" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="icon-menu" id="icon-sticky5">
                                <i class="fas fa-cog"></i>
                            </span>
                        </a>
                        <span class="label-icon-menu" id="text-sticky5">Configurações</span>
                        <div class="dropdown-menu" aria-labelledby="navbarPesquisas">
                            <a class="dropdown-item" href="#">Usuários</a>
                            <a class="dropdown-item" href="#">Grupos</a>
                            <a class="dropdown-item" href="#">Departamentos</a>
                            <a class="dropdown-item" href="#">Menus</a>
                            <a class="dropdown-item" href="#">Dashboards</a>
                        </div>
                    </li>
                    -->
                    <!--
                    <li class="nav-item menu-item">
                        <a class="nav-link btn-link-drop" href="<?=base_url().'usuario/perfil'?>">
                            <span class="icon-menu" id="icon-sticky7"><i class="fas fa-user"></i></span>
                        </a>
                        <span class="label-icon-menu" id="text-sticky7">Meu perfil</span>
                    </li>
                    -->
                    <?php endif ?>
                </ul>
            </div>
            
        </div>
    </nav>
</div>
<?php
if(isset($id_departamento)) {

    $menus_departamentos = menus_departamentos($id_departamento);

} else {

    $menus_departamentos = null;
}

if($menus_departamentos) {
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light menu_dash">
    <button class="navbar-toggler pull-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <?php if ($menus_departamentos) { ?>
            <h3><li class="nav-link"><strong><span class="badge badge-secondary"><?= $menus_departamentos[0]->nome_dep ?></span></strong></li></h3>
                <?php 
                    foreach ($menus_departamentos as $row) { 
                        if($row->id_menu_dep == 15) {

                            if (usuarios_acesso_monitoramento()) {

                                echo '<h5><a class="nav-item nav-link" href="'.base_url().$row->url_menu.'">'.$row->nome_menu.'</a></h5>';
                            }

                        } else {

                            echo '<h5><a class="nav-item nav-link" href="'.base_url().$row->url_menu.'">'.$row->nome_menu.'</a></h5>';
                        }
                    }
                } 
                ?>

            <?php if ($id_departamento == 4 && isset($datas_select)) { ?>
            <form class="form-inline" id="frm_filt_data" name="frm_filt_data" style=" margin-top:10px;margin-left:10px;" method="POST">
                <div class="form-group mb-2">
                    <?= selectDB('mes_ano', null, $datas_select, 'txt_mes_ano', 'mes_ano', $filtro['mes_ano'], null, 'JS', 'MES/ANO') ?>
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                </div>
            </form>
            <?php } ?>
        </div>
    </div>
</nav>

<?php } ?>
<br>
<div class="conteudo">
    <?php if($this->session->flashdata('msg')): ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-<?=$this->session->flashdata('type')?>" id="alert">
                <strong><?= $this->session->flashdata('msg') ?></strong>
            </div>
        </div>
    <?php endif ?>