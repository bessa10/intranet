<?php $this->load->view('layout/topo') ?>
<?php $this->load->view('layout/menu') ?>
<style>
    .lateral-fixed {
        height: 100%;
        background: #fff;
        overflow-x: hidden;

    }

    .lateral-fixed a {
        padding: 6px 8px 6px 16px;
        text-decoration: none;
        font-size: 20px;
        color: #fff;
        display: block;
    }
	.lateral-fixed ul .active {
		background: #064579 !important;
	}
	.lateral-fixed ul li .active {
		color: #fff !important;
	}

    .lateral-fixed a:hover {
        color: #064579;
    }
	.lateral-fixed li {
		background: gray;
		padding: 20px;
		margin-bottom: 6px;
		width: 80%;
	}
	.lateral-fixed li:first-child {

		/*border-top-right-radius: 70px;*/
	}
	.lateral-fixed li:last-child {

		/*border-bottom-right-radius: 70px;*/
	}
    .main {
        /* Same width as the sidebar + left position in px */
        font-size: 28px; /* Increased text to enable scrolling */

    }
	.disabled{
		pointer-events: none;
	}
	.ui.multiple.dropdown>.label{
		font-size: 15px !important;
	}
	.ui.multiple.dropdown>.label{
		padding: 3px !important;
	}
	.ui.selection.dropdown>.delete.icon, .ui.selection.dropdown>.dropdown.icon, .ui.selection.dropdown>.search.icon{
		top: 6px !important;
	}
	.ui.selection.dropdown{
		min-height: 40px !important;
		min-width: 100% !important;
	}

</style>





<div class="col-sm-12" style="margin-top: 100px;">
    <h1>Novo Curso</h1>

    <?php if($this->session->flashdata('error')): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $this->session->flashdata('error') ?>
        </div>
    <?php endif ?>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="lateral-fixed col-md-3">
			<ul  class="nav nav-pills" style="flex-direction: column">
				<li class="active block-menu">
					<a  href="#1a" data-toggle="tab">1 - Nomenclatura e versão</a>
				</li>
				<li class="block-menu"><a href="#2a" data-toggle="tab">2 - Coordenadores</a>
				</li>
				<li class="block-menu"><a href="#3a" data-toggle="tab">3 - Categorização</a>
				</li>
				<li class="block-menu"><a href="#4a" data-toggle="tab">4 - Marketing e Comercial</a>
				</li>
				<li class="block-menu"><a href="#5a" data-toggle="tab">5 - Acadêmico</a>
				</li>
				<li class="block-menu"><a href="#6a" data-toggle="tab">6 - Planejamento</a>
				</li>
				<li class="block-menu"><a href="#7a" data-toggle="tab">7 - Plano de ensino</a>
				</li>
				<li class="block-menu"><a href="#8a" data-toggle="tab">8 - Custos</a>
				</li>
				<li class="block-menu"><a href="#9a" data-toggle="tab">9 - Precificação</a>
				</li>
			</ul>
        </div>

        <div class="main col-md-9">
			<div class="tab-content clearfix">
				<div class="tab-pane active" id="1a">
					<form method="post">
						<div class="row">
							<div class="form-group col-lg-6">
								<label for="">Nome do curso</label>
								<input type="text" class="form-control" id="name" name="curso" placeholder="Nome do curso">
							</div>
						</div>
						<br>
							<h5>Versão-01</h5>
							<h5>Criado por: <?=$user_created ?> em <?= date("d-m-Y") ?></h5>


						<div class="row">
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6">
								<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
							</div>
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6 text-right">
								<a href="/homolog"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
							</div>
						</div>
					</form>
				</div>
				<div class="tab-pane" id="2a">
					<div class="row">
						<?php if(isset($nomeCurso)) { ?>
						<p><?= $nomeCurso->curso ?></p>
						<?php }?>
						<br>
					</div>
					<form method="post">
						<div class="row">
							<div class="form-group col-lg-6">
								<label for="">Coordenadores</label>
								<br>
								<select name="display" class="ui dropdown" multiple>
									<option value="1">Item 1</option>
									<option value="2">Item 2</option>
									<option value="3">Item 3</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-lg-3  col-md-3 col-sm-6 col-xs-6">
								<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
							</div>
							<div class="form-group col-lg-3  col-md-3 col-sm-6 col-xs-6 text-right">
								<a href="/homolog"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
							</div>
						</div>
					</form>
				</div>
				<div class="tab-pane" id="3a">
					<form method="post">
						<div class="row">
							<div class="form-group col-lg-6">
								<label for="">Categorização</label>
								<br>
								<select name="display" class="ui dropdown" multiple>
									<?php foreach ($consul_modalidades as $key => $modalidade): ?>
									<option value="<?= $modalidade ?>"><?= $modalidade?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-lg-3  col-md-3 col-sm-6 col-xs-6">
								<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
							</div>
							<div class="form-group col-lg-3  col-md-3 col-sm-6 col-xs-6 text-right ">
								<a href="/homolog"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
							</div>
						</div>
					</form>
				</div>
				<div class="tab-pane" id="4a">
					<form method="post">
						<div class="row">
							<div class="form-group col-lg-6">
								<label for="">Marketing e Comercial</label>
								<br>
								<select name="display" class="ui dropdown" multiple>
									<option value="1">Item 1</option>
									<option value="2">Item 2</option>
									<option value="3">Item 3</option>
								</select>
							</div>
						</div><br>
						<div class="row">
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6">
								<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
							</div>
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6 text-right">
								<a href="/homolog"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
							</div>
						</div>
					</form>
				</div>
				<div class="tab-pane" id="5a">
					<form method="post">
						<div class="row">
							<div class="form-group col-lg-6">
								<label for="">Acadêmico</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Acadêmico">

							</div>
						</div><br>
						<div class="row">
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6">
								<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
							</div>
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6 text-right">
								<a href="/homolog"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
							</div>
						</div>
					</form>
				</div>
				<div class="tab-pane" id="6a">
					<form method="post">
						<div class="row">
							<div class="form-group col-lg-6">
								<label for="">Planejamento</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Acadêmico">

							</div>
						</div><br>
						<div class="row">
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6">
								<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
							</div>
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6 text-right">
								<a href="/homolog"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
							</div>
						</div>
					</form>
				</div>
				<div class="tab-pane" id="7a">
					<form method="post">
						<div class="row">
							<div class="form-group col-lg-6">
								<label for="">Plano de ensino</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Acadêmico">

							</div>
						</div><br>
						<div class="row">
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6">
								<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
							</div>
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6 text-right">
								<a href="/homolog"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
							</div>
						</div>
					</form>
				</div>
				<div class="tab-pane" id="8a">
					<form method="post">
						<div class="row">
							<div class="form-group col-lg-6">
								<label for="">Custos</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Acadêmico">

							</div>
						</div><br>
						<div class="row">
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6">
								<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
								<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
							</div>
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6 text-right">
								<a href="/homolog"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
							</div>
						</div>
					</form>
				</div>
				<div class="tab-pane" id="9a">
					<form method="post">
						<div class="row">
							<div class="form-group col-lg-6">
								<label for="">Precificação</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Acadêmico">

							</div>
						</div><br>
						<div class="row">
							<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6">
								<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
							</div>
							<div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-6 text-right">
								<a href="/homolog"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
							</div>
						</div>
					</form>
				</div>
    </div>
</div>
<?php $this->load->view('layout/rodape') ?>
