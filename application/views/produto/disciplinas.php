<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h4 class="text-center">LISTA DE DISCIPLINAS</h4><br>
		<form action="<?=base_url().'produto/disciplinas'?>" method="POST">
			<div class="row ">
				<div class=" col-lg-3">
					<?= selectDB('modalidade', null, $modalidades, 'nome_modalidade', 'modalidade', $filtro['modalidade'], null, null, 'MODALIDADES') ?>
				</div>
				<div class=" col-lg-4">
					<button type="submit" class="btn btn-primary"><i class="fa fa-filter"></i>&nbsp;&nbsp;FILTRAR</button>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?=base_url().'produto/disciplinas'?>" class="btn btn-secondary"><i class="fa fa-trash"></i>&nbsp;&nbsp;LIMPAR</a>
				</div>
			</div>
		</form>
		<!--
		<div class="col-lg-12">
			<a href="#" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp;&nbsp;Cadastrar nova disciplina</a>
		</div>
		-->
	</div></div>
	<br>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<nav>
  				<div class="nav nav-tabs" id="nav-tab" role="tablist">
    				<a class="nav-item nav-link active item-tabs" id="nav-home-tab" data-toggle="tab" href="#nav-comp" role="tab">COMPLETAS</a>
    				<!--
    				<a class="nav-item nav-link item-tabs" id="nav-profile-tab" data-toggle="tab" href="#nav-novos" role="tab">NOVOS</a>
    				<a class="nav-item nav-link item-tabs" id="nav-profile-tab" data-toggle="tab" href="#nav-vers" role="tab">VERSIONADOS</a>
    				-->
  				</div>
			</nav>
			<div class="tab-content" id="nav-tabContent">
  				<div class="tab-pane fade show active" id="nav-comp" role="tabpanel" aria-labelledby="nav-home-tab">
  					<table class="table table-striped table-bordered" id="tabela-disciplinas">
						<thead>
							<tr>
								<th class="text-center">CÓDIGO</th>
								<th class="text-center">MNEMÔNICO</th>
								<th>TÍTULO</th>
								<th class="text-center">MODALIDADE</th>
								<th class="text-center">TOTAL HORAS</th>
								<th class="text-center">VERSÃO</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($disciplinas['dados'] as $row): ?>
							<tr>
								<td class="text-center"><?= $row['CODIGO'] ?></td>
								<td class="text-center" width="150"><?= $row['MNEMONICO'] ?></td>
								<td><?= $row['TITULO'] ?></td>
								<td class="text-center"><?= $row['MODALIDADE'] ?></td>
								<td class="text-center" width="150"><?= $row['TOTAL_HORAS'] ?></td>
								<td class="text-center"><?= $row['VERSAO'] ?></td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
  				</div>
  				<!--
  				<div class="tab-pane fade" id="nav-novos" role="tabpanel" aria-labelledby="nav-profile-tab">
  					<br>
  					<table class="table table-striped table-bordered" id="tabela-cursos-incomp">
						<thead>
							<tr>
								<th>MNEMÔNICO</th>
								<th>TÍTULO</th>
								<th class="text-center">TIPO</th>
								<th class="text-center">MODALIDADE</th>
								<th class="text-center">VERSÃO</th>
								<th class="text-center">ETAPA</th>
								<th class="text-center" class="text-center">DATA CRIAÇÃO</th>
							</tr>
						</thead>
					</table>
  				</div>
  				<div class="tab-pane fade" id="nav-vers" role="tabpanel" aria-labelledby="nav-profile-tab">
  					<p>Versionados</p>
  				</div>
  				-->
			</div>
		</div>
	</div>
</div>