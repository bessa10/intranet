<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<h4 class="text-center">LISTA DE CURSOS</h4><br>
	<div class="row">
		<div class="col-lg-12">
			<a href="<?= base_url().'produto/add_curso' ?>" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;CADASTRAR NOVO CURSO</a>
		</div>
	</div><br>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<nav>
  				<div class="nav nav-tabs" id="nav-tab" role="tablist">
    				<a class="nav-item nav-link active item-tabs" id="nav-home-tab" data-toggle="tab" href="#nav-comp" role="tab">COMPLETOS</a>
    				<a class="nav-item nav-link item-tabs" id="nav-profile-tab" data-toggle="tab" href="#nav-novos" role="tab">NOVOS</a>
    				<a class="nav-item nav-link item-tabs" id="nav-profile-tab" data-toggle="tab" href="#nav-vers" role="tab">VERSIONADOS</a>
  				</div>
			</nav>
			<div class="tab-content" id="nav-tabContent">
  				<div class="tab-pane fade show active" id="nav-comp" role="tabpanel" aria-labelledby="nav-home-tab">
  					<table class="table table-striped table-bordered" id="tabela-cursos-comp">
						<thead>
							<tr>
								<th class="text-center">CÓDIGO</th>
								<th class="text-center">MNEMÔNICO</th>
								<th>TÍTULO</th>
								<th>TIPO</th>
								<th class="text-center">MODALIDADE</th>
								<th class="text-center">ORIENTAÇÃO</th>
								<th class="text-center">VERSÃO</th>
								<th class="text-center">DATA DOU</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($cursos_completos['dados'] as $row): ?>
							<tr>
								<td class="text-center"><?= $row['CODIGO'] ?></td>
								<td class="text-center"><?= $row['MNEMONICO'] ?></td>
								<td><?= $row['TITULO'] ?></td>
								<td class="text-center"><?= $row['TIPO'] ?></td>
								<td class="text-center"><?= $row['MODALIDADE'] ?></td>
								<td class="text-center"><?= $row['ORIENTACAO'] ?></td>
								<td class="text-center"><?= $row['VERSAO'] ?></td>
								<td class="text-center"><?= $row['DATA DOU'] ?></td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
  				</div>
  				<div class="tab-pane fade" id="nav-novos" role="tabpanel" aria-labelledby="nav-profile-tab">
  					<table class="table table-striped table-bordered" id="tabela-cursos-incomp">
						<thead>
							<tr>
								<th>MNEMÔNICO</th>
								<th>TÍTULO</th>
								<th class="text-center">TIPO</th>
								<th class="text-center">MODALIDADE</th>
								<th class="text-center">VERSÃO</th>
								<th class="text-center">ETAPA</th>
								<th class="text-center">AÇÕES</th>
								<th class="text-center" class="text-center">DATA CRIAÇÃO</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($cursos_incomp as $row): ?>
							<tr>
								<td><?= $row->mnemonico ?></td>
								<td><?= $row->nome ?></td>
								<td class="text-center"><?= $row->tipo ?></td>
								<td class="text-center"><?= $row->modalidade ?></td>
								<td class="text-center"><?= $row->versao ?></td>
								<td class="text-center"><?= $row->nome_etapa ?></td>
								<td class="text-center"><a href="<?= base_url().$row->url_etapa.'/'.$row->id_curso ?>" class="btn btn-sm btn-info"><i class="fas fa-share-square"></i>&nbsp;&nbsp;Continuar cadastro</a></td>
								<td class="text-center"></td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
  				</div>
  				<div class="tab-pane fade" id="nav-vers" role="tabpanel" aria-labelledby="nav-profile-tab">
  					<br><p>Versionados</p>
  				</div>
			</div>
		</div>
	</div>
</div>