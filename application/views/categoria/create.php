<?php $this->load->view('layout/topo') ?>
<?php $this->load->view('layout/menu') ?>


<div class="col-sm-12" style="margin-top: 100px;">
	<?php if($this->session->flashdata('error')){ ?>
		<div class="alert alert-danger" role="alert">
			<?php echo $this->session->flashdata('error') ?>
		</div>
	<?php } ?>
	<?php if ($this->session->flashdata('success')){ ?>
		<div class="alert alert-success" role="alert">
			<?php echo $this->session->flashdata('success') ?>
		</div>
	<?php } ?>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="lateral-fixed col-md-3">
			<?php $this->load->view('layout/menu_lateral') ?>
		</div>
		<div class="main col-md-6">
			<?php if(isset($curso)) { ?>
				<h3>Nome do curso: <span style="font-weight: lighter"><?= $curso->nome ?></span></h3>
			<?php }?>
			<h6 class="sep-text">Criado por:<span style="font-weight: lighter"> <?=$user_created ?> em <?= date("d-m-Y") ?></span> - Versão: <span style="font-weight: lighter">01</span></h6>
			<form method="post">
				<div class="row">
					<div class="form-group" style="width: 50%;">
						<label for="exampleFormControlSelect1">Modalidade</label>
						<select class="form-control" name="modalidade" id="exampleFormControlSelect1">
							<?php foreach ($consul_modalidades['dados'] as $modalidade){ ?>
								<option value="<?= $modalidade['DESCRICAO'] ?>"><?= $modalidade['DESCRICAO'] ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group"style="width: 50%;">
						<label for="exampleFormControlSelect1">Categoria</label>
						<select class="form-control" name="grupo_curso" id="exampleFormControlSelect1">
							<?php foreach ($consul_grupos['dados'] as $grupo){ ?>
								<option value="<?= $grupo['GRUPO_CURSO'] ?>"><?= $grupo['DESCRICAO'] ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group"style="width: 50%;">
						<label for="exampleFormControlSelect1">Organização</label>
						<select class="form-control" name="tipo" id="exampleFormControlSelect1">
							<?php foreach ($consul_organizacao['dados'] as $organizacao){ ?>
							<option value="<?= $organizacao['ORGANIZAÇÃO'] ?>"><?= $organizacao['ORGANIZAÇÃO'] ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group"style="width: 50%;">
						<label for="">Mnemonico</label>
						<input type="text" maxlength="7" class="form-control mnemonico <?= (form_error('mnemonico'))?'is-invalid':'' ?>" id="mnemonico" name="mnemonico" placeholder="ex: US_TDCG" value="">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6">
						<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar categoria</button>
					</div>
					<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6 text-right">
						<a href="<?=base_url().'produto/add_docente/'.$idCurso ?>"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>
<?php $this->load->view('layout/rodape') ?>