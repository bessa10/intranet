<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 100px">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<?php 
				//if(file_exists('assets/uploads/usuarios/'.$dados_usuario->img_perfil)) {
				if(isset($dados_usuario)) {
					if($dados_usuario->img_perfil!= null) {

						$img_usuario = $dados_usuario->img_perfil;

					} else {

						$img_usuario = 'user.png';
					}
				} else {

					$img_usuario = 'user.png';
				}	
			?>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<ul id="img-user">
						<li><div class="user-profile" style="background-image: url(<?= base_url().'assets/uploads/usuarios/'.$img_usuario ?>)"></div></li>
						<li><h4 class="text-center"><?= (isset($dados_usuario)) ? $dados_usuario->nome : 'User não encontrado' ?></h4></li>
					</ul>
				</div><br>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h5><i class="fas fa-code"></i>&nbsp;Requisições Sistemas</h5>
					<?php if($chamados_sistemas): ?>
						<div class="row">
							<ul class="list-group list-chamados col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<?php foreach($chamados_sistemas as $row): ?>
									<li class="list-group-item">
										<a href="<?=URL_SISTEMAS.'ticket/view/'.$row->tid ?>" target="blank"><strong><?= $row->tid ?> - </strong><?=$row->title?></a><br>
										<span class="float-right"><small><?=formata_data_hora($row->created)?></small></span>
										<br>
									</li>
								<?php endforeach ?>
							</ul>
						</div>
					<?php else: ?>	
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<br><a href="<?=URL_SISTEMAS?>" target="_blank" class="btn btn-info"><i class="fas fa-edit"></i>&nbsp;&nbsp;Cadastrar nova requisição</a>
						</div>
					</div><br>
					<?php endif ?>
					<br>
					<h5><i class="fas fa-desktop"></i>&nbsp;Requisições TI</h5>
					<?php if($chamados_ti): ?>
					<div class="row">
						<ul class="list-group list-chamados col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php foreach($chamados_ti as $row): ?>
								<li class="list-group-item">
									<a href="<?=URL_TI.'ticket/view/'.$row->tid ?>" target="blank"><strong><?= $row->tid ?> - </strong><?=$row->title?></a><br>
									<span class="float-right"><small><?=formata_data_hora($row->created)?></small></span>
									<br>
								</li>
							<?php endforeach ?>
						</ul>
					</div>
					<?php else: ?>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<br><a href="<?=URL_TI?>" target="_blank" class="btn btn-info"><i class="fas fa-edit"></i>&nbsp;&nbsp;Cadastrar nova requisição</a>
						</div>
					</div><br>
					<?php endif ?>
					<br>
					<h5><i class="fas fa-tools"></i>&nbsp;Requisições Facilities</h5>
					<?php if($chamados_facilities): ?>
					<div class="row">
						<ul class="list-group list-chamados col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php foreach($chamados_facilities as $row): ?>
								<li class="list-group-item">
									<a href="<?=URL_FACILITIES.'ticket/view/'.$row->tid ?>" target="blank"><strong><?= $row->tid ?> - </strong><?=$row->title?></a><br>
									<span class="float-right"><small><?=formata_data_hora($row->created)?></small></span>
									<br>
								</li>
							<?php endforeach ?>
						</ul>
					</div>
					<?php else: ?>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<br><a href="<?=URL_FACILITIES?>" target="_blank" class="btn btn-info"><i class="fas fa-edit"></i>&nbsp;&nbsp;Cadastrar nova requisição</a>
						</div>
					</div><br>
					<?php endif ?>
					
					<br>
					<h5><i class="fas fa-database"></i>&nbsp;Requisições Inteligência de Dados</h5>
					<?php if($chamados_int_dados): ?>
					<div class="row">
						<ul class="list-group list-chamados col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php foreach($chamados_int_dados as $row): ?>
								<li class="list-group-item">
									<a href="<?=URL_INT_DADOS.'ticket/view/'.$row->tid ?>" target="blank"><strong><?= $row->tid ?> - </strong><?=$row->title?></a><br>
									<span class="float-right"><small><?=formata_data_hora($row->created)?></small></span>
									<br>
								</li>
							<?php endforeach ?>
						</ul>
					</div>
					<?php else: ?>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<br><a href="<?=URL_INT_DADOS?>" target="_blank" class="btn btn-info"><i class="fas fa-edit"></i>&nbsp;&nbsp;Cadastrar nova requisição</a>
						</div>
					</div><br>
					<?php endif ?>
					<br>
					<h5><i class="fas fa-project-diagram"></i>&nbsp;Requisições Gestão e Projetos Estratégicos</h5>
					<?php if($chamados_gestao_proj): ?>
					<div class="row">
						<ul class="list-group list-chamados col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php foreach($chamados_gestao_proj as $row): ?>
								<li class="list-group-item">
									<a href="<?=URL_PROJ_ESTR.'ticket/view/'.$row->tid ?>" target="blank"><strong><?= $row->tid ?> - </strong><?=$row->title?></a><br>
									<span class="float-right"><small><?=formata_data_hora($row->created)?></small></span>
									<br>
								</li>
							<?php endforeach ?>
						</ul>
					</div>
					<?php else: ?>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<br><a href="<?=URL_PROJ_ESTR?>" target="_blank" class="btn btn-info"><i class="fas fa-edit"></i>&nbsp;&nbsp;Cadastrar nova requisição</a>
						</div>
					</div><br>
					<?php endif ?>


				</div>


			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<br>
			<div class="row">
		        <?php if($this->session->flashdata()): ?>
		        <div id="alert-publicacao">	
		            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                <div class="alert alert-<?=$this->session->flashdata('type')?>" id="alert">
		                    <strong><?= $this->session->flashdata('msg') ?></strong>
		                </div>
		            </div>
	        	</div>
		        <?php endif ?>
		        <?php if(isset($dados_usuario)): ?>
			        <?php if($dados_usuario->id_grupo == 1 || $dados_usuario->id_grupo == 3): ?>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<form method="post" enctype="multipart/form-data">
		  						<div class="card-body">
		  							<div id="btn-txt-publicacao" class="row">
		  								<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		    								<button type="button" onclick="txtPublicacao()" class="btn btn-block btn-lg btn-light text-left">Nos conte uma novidade da sua área</button>
		    							</div>
		    						</div>
		    						<div id="txt-publicacao" class="row" style="display:none;">
		    							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                		<a href="javascript:" onclick="cancelaTxtPublicacao()" class="float-right text-danger"><i class="fa fa-times"></i></a><br>
			    							<textarea id="texto_publicacao" name="texto_publicacao"></textarea>
					                	</div>
				                	</div>
				                	<div id="txt-incorpora" class="row" style="display:none;">
				                		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
				                			<label><strong>Incorporar vídeo do youtube</strong></label><a href="javascript:" onclick="cancelaIncorporaVideo()" class="float-right text-danger"><i class="fa fa-times"></i></a><br>
				                			<textarea id="incorpora_video" name="incorpora_video" rows="3" class="form-control"></textarea>
				                		</div>
				                	</div>
				                	<div class="row">
				                		<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
		                        			<label class="btn btn-light">
		                            			<i class="fas fa-photo-video"></i><input type="file" hidden name="attachments[]"/ style="font-size:20px;" multiple="">&nbsp;Foto / Vídeo
		                        			</label>&nbsp;&nbsp;
		                            		<button type="button" class="btn btn-light" id="btn-incorpora-video" onclick="txtIncorporaVideo()" style="margin-bottom:5px;"><i class="fab fa-youtube"></i>&nbsp;Incorporar vídeo do youtube</button>
		                    			</div>
		                    			<div class="form-group col-lg-6 text-right">
		                    				<button type="submit" class="btn btn-info"><i class="fas fa-check"></i>&nbsp;&nbsp;Publicar</button>
		                    			</div>
				                	</div>
		  						</div>
	  						</form>
						</div><br>
					</div>
					<?php endif ?>
				<?php endif ?>

				<!-- POSTAGEM DE TESTE -->
				<?php if(ENVIRONMENT == 'production'): ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 postagens">	
					<div class="card box-postagem">
						<div class="card-body">
							<div class="row">
								<ul class="ul-user-post">
									<li><div class="box-img-post" style="background-image: url(<?= base_url().'assets/uploads/usuarios/ricardo.png'?>)"></div></li>
									<li><p>Ricardo de Almeida Ferreira<br><small>08/10/2020 13:57</small></p></li>
								</ul>
							</div>
							<img src="<?= base_url().'assets/img/intranet_dev.png'?>" class="img-fluid">
						</div>
					</div>
				</div>
				<?php else : ?>
					<!-- POSTAGENS APROVADAS -->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 postagens">	
					<?php foreach($postagens_aprovadas as $postagem): ?>
						<?php 
							$anexos_img = anexosImgPostagem($postagem->id_postagem);
							$anexos_video = anexosVideoPostagem($postagem->id_postagem);
							$anexos_outros = anexosOutrosPostagem($postagem->id_postagem);
						?>
						<div class="card box-postagem">
							<div class="card-body">
								<?php
								if($postagem->img_perfil!= null) {
									$img_usuario = $postagem->img_perfil;
								} else {
									$img_usuario = 'user.png';
								}
								?>
								<div class="row">
									<ul class="ul-user-post">
										<li><div class="box-img-post" style="background-image: url(<?= base_url().'assets/uploads/usuarios/'.$img_usuario ?>)"></div></li>
										<li><p><?= $postagem->nome ?><br><small><?=formata_data_hora($postagem->dth_insert)?></small></p></li>
									</ul>
								</div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<?php if($postagem->texto_publicacao): ?>
										<p><?= $postagem->texto_publicacao ?></p>
										<?php endif ?>
										<?php if($postagem->incorpora_video): ?>
										<div class="embed-responsive embed-responsive-16by9">
											<?= $postagem->incorpora_video ?>
										</div>
										<?php endif ?>
										<?php if(count($anexos_img) > 0): ?>
											<?php if(count($anexos_img) > 1): ?>
											<div id="carouselImg<?=$postagem->id_postagem?>" class="carousel slide" data-ride="carousel">
												<div class="carousel-inner">
												<?php foreach($anexos_img as $key => $value): ?>
													<div class="carousel-item <?=($key==0)?'active':''?>" data-interval="3000">
		      											<img src="<?= base_url().'attachments/'.$value->filename?>" class="d-block w-100">
		    										</div>
												<?php endforeach ?>
												</div>
												<a class="carousel-control-prev" href="#carouselImg<?=$postagem->id_postagem?>" role="button" data-slide="prev">
	    											<span class="carousel-control-prev-icon" aria-hidden="true"></span>
	    											<span class="sr-only">Previous</span>
	  											</a>
	  											<a class="carousel-control-next" href="#carouselImg<?=$postagem->id_postagem?>" role="button" data-slide="next">
	    											<span class="carousel-control-next-icon" aria-hidden="true"></span>
	    											<span class="sr-only">Next</span>
	  											</a>
											</div>
											<?php else : ?>
												<?php foreach($anexos_img as $img): ?>
		      										<img src="<?= base_url().'attachments/'.$img->filename?>" class="img-fluid">
												<?php endforeach ?>
											<?php endif ?>
										<?php endif ?>

										<?php if(count($anexos_video)>0): ?>
											<?php foreach($anexos_video as $key => $value): ?>
								    		<div class="embed-responsive embed-responsive-16by9" style="margin-top:20px;margin-bottom:20px;">
												<video controls>
													<source src="<?= base_url().'attachments/'.$value->filename?>" type="<?= $value->file_type ?>">
												</video>
											</div>
								    		<?php endforeach ?>
											
										<?php endif ?>
									</div>
								</div><hr>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
										<button type="button" class="btn btn-lg btn-block btn-light"><i class="far fa-grin"></i>&nbsp;&nbsp;Gostei</button>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
										<button type="button" class="btn btn-lg btn-block btn-light"><i class="far fa-comment-alt"></i>&nbsp;&nbsp;Comentar</button>
									</div>

								</div>
							</div>
						</div>
					<?php endforeach ?>
				<?php endif ?>
				</div>
			</div>
		</div>
		<!--
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<h4 class="text-center">ANIVERSARIANTES DO MÊS</h4>
			<div class="card">
				<div class="card-body">
					<?php foreach($aniversarios as $row): ?>
						<?php
						if($row->img_perfil!= null) {
							$img_usuario = $row->img_perfil;
						} else {
							$img_usuario = 'user.png';
						}
						?>
						<?php if(formata_dia($row->data_nascimento) == date('d')): ?>
						<div class="row">
							<ul class="ul-user-post">
								<li><div class="box-img-post" style="background-image: url(<?= base_url().'assets/uploads/usuarios/'.$img_usuario ?>)"></div></li>
								<li><h6><?= $row->nome ?><br><span class="text-danger"><i class="fas fa-birthday-cake"></i>&nbsp;Aniversariante do dia</span></h6></li>
							</ul>
						</div>
						<hr>
						<?php else: ?>
						<div class="row">
							<ul class="ul-user-post">
								<li><div class="box-img-post" style="background-image: url(<?= base_url().'assets/uploads/usuarios/'.$img_usuario ?>)"></div></li>
								<li><p><?= $row->nome ?><br><?=formata_dataMes($row->data_nascimento)?></p></li>
							</ul>
						</div>
						<?php endif ?>
					<?php endforeach ?>
				</div>
			</div>
		</div>
		-->
	</div>
</div>