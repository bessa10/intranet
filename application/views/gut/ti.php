<style type="text/css">
	
	.bg-table-azul {
		background-color:#354B5E;
		color:#FFFFFF; 
	}

	.bg-light {
		font-weight: bold;
		font-weight: 500;
	}

	label {
		font-weight: bold;
		font-weight: 500;
	}

</style>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="text-center">
		<br><h1><strong>GUT</strong></h1>
		<h3>Gravidade - Urgência - Tendência</h3>
	</div><hr>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<br><h5 class="text-center"><strong>O conceito da Matriz GUT</strong></h5><br>
			<p class="text-center" style="line-height:40px;">
				A Matriz GUT, da sigla: Gravidade, Urgência e Tendência é uma ferramenta utilizada para definir a priorização de atividades.<br>
				Com ela você lista e classifica suas atividades utilizando a razão entre os três elementos: gravidade, urgência e tendência.<br>
				A ferramenta foi desenvolvida por Kepner e Tregoe e vem sendo utilizada no mundo todo para resolver problemas complexos em organizações.<br>
				A Matriz GUT é com frequência apresentada junto a ferramentas de gestão da qualidade.
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 offset-lg-3 col-md-12 col-sm-12 col-xs-12">
			<table class="table table-bordered">
				<tr>
					<th colspan="3" class="text-center bg-table-azul">
						SLA CETRUS COM BASE NA MATRIZ GUT
					</th>
				</tr>
				<tr>
					<td width="100" class="bg-table-azul"><strong>GUT</strong></td>
					<td class="bg-light">Entre 80 e 125</td>
					<td class="bg-light">Resolver em até 4horas úteis e no mesmo dia</td>
				</tr>
				<tr>
					<td width="100" class="bg-table-azul"><strong>GUT</strong></td>
					<td class="bg-light">Entre 40 e 79</td>
					<td class="bg-light">Resolver em até 9horas úteis</td>
				</tr>
				<tr>
					<td width="100" class="bg-table-azul"><strong>GUT</strong></td>
					<td class="bg-light">Entre 25 e 39</td>
					<td class="bg-light">Resolver em até 18 horas úteis</td>
				</tr>
				<tr>
					<td width="100" class="bg-table-azul"><strong>GUT</strong></td>
					<td class="bg-light">Abaixo de 25</td>
					<td class="bg-light">Resolver dentro dos prazos estabelecidos no catalogo de serviços</td>
				</tr>
			</table>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<br><p class="text-center">Agora que sabemos o que é a Matriz GUT, conceito e definição é hora de aprender como utiliza-la.</p> 
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<br><h5 class="text-center"><strong>Simulador de GUT</strong></h5><br>
			<p class="text-center">Através das 3 opções abaixo, combine gravidade, urgência e tendência para saber como seu chamado será atendido.</p><br>
		</div>
		<div class="col-lg-8 offset-lg-2 col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<label>Gravidade</label>
					<select class="form-control" name="gravidade" id="gravidade">
						<option value="">Selecione</option>
						<option value="1">1 - sem gravidade</option>
						<option value="2">2 - pouco grave</option>
						<option value="3">3 - grave</option>
						<option value="4">4 - muito grave</option>
						<option value="5">5 - extremanente grave</option>
					</select>
					<span class="text-danger" id="msg-gravidade"></span>
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<label>Urgência</label>
					<select class="form-control" name="urgencia" id="urgencia">
						<option value="">Selecione</option>
						<option value="1">1 - pode esperar</option>
						<option value="2">2 - pouco urgente</option>
						<option value="3">3 - o mais rápido possível</option>
						<option value="4">4 - urgente</option>
						<option value="5">5 - atenção imediata</option>
					</select>
					<span class="text-danger" id="msg-urgencia"></span>
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<label>Tendência</label>
					<select class="form-control" name="tendencia" id="tendencia">
						<option value="">Selecione</option>
						<option value="1">1 - não mudará</option>
						<option value="2">2 - piora a longo prazo</option>
						<option value="3">3 - irá piorar</option>
						<option value="4">4 - piora em pouco tempo se nada for feito</option>
						<option value="5">5 - piora rápida se nada for feito</option>
					</select>
					<span class="text-danger" id="msg-tendencia"></span>
				</div>
			</div><br>
			<div class="row">
				<div class="form-group col-lg-4 offset-lg-4 col-md-4 col-sm-12 col-xs-12">
					<button type="button" class="btn btn-lg btn-success btn-block" onclick="calcularGUT()">CALCULAR GUT</button>
				</div>
			</div><br>

			<div class="row">
				<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
					<h1><span id="valor-gut"></span></h1>
					<h5><span id="txt-gut"></span></h5>
				</div>
			</div>


			<div class="row" id="catalogos_servicos" style="">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<br><h5 class="text-center"><strong>Catálogo de serviços</strong></h5><br>
					<table class="table table-bordered">
						<thead>
							<tr class="bg-table-azul text-center">
								<th>Projeto</th>
								<th>Categoria</th>
								<th>Prazo para 1º atendimento</th>
								<th>Prazo para solução</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($projects as $row): ?>
							<tr class="bg-light">
								<td colspan="4"><?= $row->name ?></td>
							</tr>
								<?php foreach($catalogos as $row2): ?>
									<?php if($row2->pid == $row->pid): ?>
									<tr>
										<td></td>
										<td><?= $row2->nome_categoria ?></td>
										<td class="text-center"><?= $row2->atendimento ?></td>
										<td class="text-center"><?= $row2->solucao ?></td>
									</tr>
									<?php endif ?>
								<?php endforeach ?>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<br><br><br><br><br><br><br>

<script type="text/javascript">
	
	function calcularGUT() {

		var gravidade = $("#gravidade").val();
		var urgencia = $("#urgencia").val();
		var tendencia = $("#tendencia").val();
		var erro = false;
		var valor1 = 0;
		var valor_gut = 0;
		var texto_gut = 0;

		$("#txt-gut").html('');
		$("#valor-gut").html('');
		$(".text-danger").html('');
		//$(".is-invalid").removeClass();

		if(gravidade == '') {
			//$("#gravidade").addClass('is-invalid');
			$("#msg-gravidade").html('selecione a gravidade');
			erro = true;
		}

		if(urgencia == '') {
			//$("#urgencia").addClass('is-invalid');
			$("#msg-urgencia").html('selecione a urgência');
			erro = true;
		}

		if(tendencia == '') {
			//$("#tendencia").addClass('is-invalid');
			$("#msg-tendencia").html('selecione a gravidade');
			erro = true;
		}

		if(erro == false) {

			valor1 = gravidade * urgencia;

			valor_gut = valor1 * tendencia;

			if(valor_gut < 25) {
        
		        texto_gut = 'Resolver dentro dos prazos estabelecidos no catalogo de serviços';
		        $("#catalogos_servicos").show(500);
		    
		    } else if(valor_gut >= 25 && valor_gut <= 39) {

		        texto_gut = 'Resolver em até 18 horas úteis';

		    } else if (valor_gut > 39 && valor_gut <= 79) {
		        
		        texto_gut = 'Resolver em até 9horas úteis';
		    
		    } else if (valor_gut > 79 && valor_gut <= 125) {
		    
		        texto_gut = 'Resolver em até 4horas úteis e no mesmo dia';

		    } else {

		    	valor_gut = '';
		        texto_gut = '';
		    }


			$("#valor-gut").html('GUT ' + valor_gut);

		    $("#txt-gut").html(texto_gut);

		}
	}
</script>