<?php $this->load->view('layout/topo') ?>
<?php $this->load->view('layout/menu') ?>


<div class="col-sm-12" style="margin-top: 100px;">
	<?php if($this->session->flashdata('error')){ ?>
		<div class="alert alert-danger" role="alert">
			<?php echo $this->session->flashdata('error') ?>
		</div>
	<?php } ?>
	<?php if ($this->session->flashdata('success')){ ?>
		<div class="alert alert-success" role="alert">
			<?php echo $this->session->flashdata('success') ?>
		</div>
	<?php } ?>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="lateral-fixed col-md-3">
			<?php $this->load->view('layout/menu_lateral') ?>
		</div>
		<div class="main col-md-9">
					<?php if(isset($curso)) { ?>
						<h3>Nome do curso: <span style="font-weight: lighter"><?= $curso->nome ?></span></h3>
					<?php }?>
					<h6 class="sep-text">Criado por:<span style="font-weight: lighter"> <?=$user_created ?> em <?= date("d-m-Y") ?></span> - Versão: <span style="font-weight: lighter">01</span></h6>
			<h4>Personas criadas:</h4>
			<?php if ($receber_personas){ ?>
			<table class="table table-bordered">
				<thead class="thead-dark">
				<tr>
					<th scope="col">Id Persona</th>
					<th scope="col">Nome</th>
					<th scope="col">Idade</th>
					<th scope="col">Especialidade</th>
					<th scope="col">Cargo de ocupação</th>
					<th scope="col">Onde trabalha</th>
					<th scope="col">Nível de especialização</th>
					<th scope="col">Meios de comunicação</th>
					<th scope="col">Principais objetivos</th>
					<th scope="col">Principais problemas</th>
					<th scope="col">Como o curso pode ajudar?</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($receber_personas as $persona){ ?>
				<tr>
					<th scope="row"><?= $persona->id_persona ?></th>
					<td><?= $persona->nome ?></td>
					<td><?= $persona->idade ?></td>
					<td><?= $persona->especialidade ?></td>
					<td><?= $persona->cargo_ocupacao ?></td>
					<td><?= $persona->onde_trabalha ?></td>
					<td><?= $persona->nivel_especializacao ?></td>
					<td><?= $persona->meios_comunicacao ?></td>
					<td><?= $persona->principais_objetivos ?></td>
					<td><?= $persona->principais_problemas ?></td>
					<td><?= $persona->como_curso_pode_ajudar ?></td>
				</tr>
				<?php }} ?>
				</tbody>
			</table>
			<h6>Criar persona</h6>
			<form method="post">
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Nome</label>
						<input type="text" class="form-control <?= (form_error('nome'))?'is-invalid':'' ?>" id="name" name="nome" placeholder="Nome da persona">
					</div>
				</div>
				<div class="form-group col-lg-6">
					<label for="ds_sexo"> Sexo</label>
					<select class="form-control" name="sexo"id="sexo">
						<option value="F">FEMININO</option>
						<option value="M">MASCULINO</option>
						<option value="O">Outro</option>
					</select>
				</div>
				<div class="row">
					<div class="form-group col-lg-2">
						<label for="">Idade</label>
						<input type="text" class="form-control <?= (form_error('idade'))?'is-invalid':'' ?>" id="idade" name="idade" placeholder="Idade">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Especialidade em que atua</label>
						<input type="text" class="form-control <?= (form_error('especialidade'))?'is-invalid':'' ?>" id="especialidade" name="especialidade" placeholder="Especialidade em que atua">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Cargo/Ocupação:</label>
						<input type="text" class="form-control <?= (form_error('cargo_ocupacao'))?'is-invalid':'' ?>" id="cargo" name="cargo_ocupacao" placeholder="Cargo ou ocupação">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Onde trabalha:</label>
						<input type="text" class="form-control <?= (form_error('onde_trabalha'))?'is-invalid':'' ?>" id="onde_trabalha" name="onde_trabalha" placeholder="Onde trabalha">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Nível de Especialização:</label>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="nivel_especializacao" value="Residência Médica">
							<label class="form-check-label" for="exampleRadios1">
								Residência Médica
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="nivel_especializacao"  value="Pós-Graduação">
							<label class="form-check-label" for="exampleRadios2">
								Pós-Graduação
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="nivel_especializacao" value="Fellowship">
							<label class="form-check-label" for="exampleRadios3">
								Fellowship
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="nivel_especializacao" value="Mestrado">
							<label class="form-check-label" for="exampleRadios3">
								Mestrado
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="nivel_especializacao" value="Doutorado">
							<label class="form-check-label" for="exampleRadios3">
								Doutorado
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="nivel_especializacao" value="Pós-Doutorado">
							<label class="form-check-label" for="exampleRadios3">
								Pós-Doutorado
							</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Meios de comunicação que utiliza:</label>
						<input type="text" class="form-control <?= (form_error('meios_comunicacao'))?'is-invalid':'' ?>" id="meios_comunicacao" name="meios_comunicacao" placeholder="Meios de comunicação que utiliza">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Principais objetivos ao realizar o curso:</label>
						<input type="text" class="form-control <?= (form_error('principais_objetivos'))?'is-invalid':'' ?>" id="principais_objetivos" name="principais_objetivos" placeholder="Principais objetivos ao realizar o curso">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Principais problemas no dia-a-dia:</label>
						<input type="text" class="form-control <?= (form_error('principais_problemas'))?'is-invalid':'' ?>" id="principais_problemas" name="principais_problemas" placeholder="Principais problemas no dia-a-dia">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Como o curso pode ajudar com esses problemas:</label>
						<input type="text" class="form-control <?= (form_error('como_curso_pode_ajudar'))?'is-invalid':'' ?>" id="como_curso_pode_ajudar" name="como_curso_pode_ajudar" placeholder="Como o curso pode ajudar com esses problemas">
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6">
						<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar persona</button>
					</div>
				</div>
			</form>
			<hr>
			<div class="row">
				<div class="form-group col-lg-3  col-md-3 col-sm-6 col-xs-6">
					<?php if($receber_personas){ ?>
						<a href="<?= base_url().'produto/relevancia/'.$idCurso ?>"><button type="button" class="btn btn-success">Próxima etapa&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></button></a>
					<?php } ?>
				</div>
				<div class="form-group col-lg-3  col-md-3 col-sm-6 col-xs-6 text-right">
					<a href="<?= base_url().'produto/marketing_publico_alvo/'.$idCurso ?>"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
				</div>
			</div>
		</div>

	</div>
</div>
<?php $this->load->view('layout/rodape') ?>
