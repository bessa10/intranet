<?php $this->load->view('layout/topo') ?>
<?php $this->load->view('layout/menu') ?>


<div class="col-sm-12" style="margin-top: 100px;">
	<?php if($this->session->flashdata('error')){ ?>
		<div class="alert alert-danger" role="alert">
			<?php echo $this->session->flashdata('error') ?>
		</div>
	<?php } ?>
	<?php if ($this->session->flashdata('success')){ ?>
		<div class="alert alert-success" role="alert">
			<?php echo $this->session->flashdata('success') ?>
		</div>
	<?php } ?>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="lateral-fixed col-md-3">
			<?php $this->load->view('layout/menu_lateral') ?>
		</div>
		<div class="main col-md-6">
			<?php if(isset($curso)) { ?>
				<h3>Nome do curso: <span style="font-weight: lighter"><?= $curso->nome ?></span></h3>
			<?php }?>
			<h6 class="sep-text">Criado por:<span style="font-weight: lighter"> <?=$user_created ?> em <?= date("d-m-Y") ?></span> - Versão: <span style="font-weight: lighter">01</span></h6>
			
			<form method="post">
				<div class="scroll">
					<div class="row">
					<div class="col-md-6">
						<h6>Especialidades</h6>
					<?php foreach ($especialidades as $especialidade){ ?>
				<div class="form-check">
					<input class="form-check-input" type="checkbox" value="<?= $especialidade->id_especialidade ?>" name="especialidade[]" id="defaultCheck1">
					<label class="form-check-label" for="defaultCheck1">
						<?= $especialidade->especialidade ?>
					</label>
				</div>
					<?php } ?>
				</div>
				<div class="col-md-6">
					<h6>Area de atuações</h6>
					<?php foreach($area_atuacoes as $area){ ?>
					<div class="form-check">
					<input class="form-check-input" type="checkbox" value="<?= $area->id_area ?>" name="area_atuacao[]" id="defaultCheck1">
					<label class="form-check-label" for="defaultCheck1">
						<?= $area->area_atuacao ?>
					</label>
				</div>
				<?php } ?>
				</div>
			</div>
		</div>
				<br><br>
				<div class="row">
					<div class="form-group col-lg-5  col-md-6 col-sm-6 col-xs-6">
						<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
					</div>
					<div class="form-group col-lg-5  col-md-6 col-sm-6 col-xs-6 text-right">
						<a href="<?= base_url().'produto/add_categoria/'.$idCurso ?>"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>
<?php $this->load->view('layout/rodape') ?>
