<?php $this->load->view('layout/topo') ?>
<?php $this->load->view('layout/menu') ?>
<style>
.some_class_name{
	min-height: 43px;
}
</style>
<div class="col-sm-12" style="margin-top: 100px;">
	<?php if($this->session->flashdata('error')){ ?>
		<div class="alert alert-danger" role="alert">
			<?php echo $this->session->flashdata('error') ?>
		</div>
	<?php } ?>
	<?php if ($this->session->flashdata('success')){ ?>
		<div class="alert alert-success" role="alert">
			<?php echo $this->session->flashdata('success') ?>
		</div>
	<?php } ?>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="lateral-fixed col-md-3">
			<?php $this->load->view('layout/menu_lateral') ?>
		</div>
		<div class="main col-md-9">
			<?php if(isset($curso)) { ?>
				<h3>Nome do curso: <span style="font-weight: lighter"><?= $curso->nome ?></span></h3>
			<?php }?>
			<h6 class="sep-text">Criado por:<span style="font-weight: lighter"> <?=$user_created ?> em <?= date("d-m-Y") ?></span> - Versão: <span style="font-weight: lighter">01</span></h6>

			<form method="post">
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Relevância (Epidemiologia/Estatística/Porque esse curso é importante/ Venda esse curso):</label>
						<textarea class="form-control" name="relevancia" id="relevancia" rows="3"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Objetivos Especificos (ao final do curso o aluno estará apto a):</label>
						<textarea class="form-control" name="objetivos_especificos" id="objetivos_especificos" rows="3"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Diferenciais determinantes (o que este curso tem que a concorrência não tem):</label>
						<textarea class="form-control" name="diferenciais_determinantes" id="diferenciais_determinantes" rows="3"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="ds_sexo">Inserir palavra chave</label>
						<input name="key_words" class="some_class_name" placeholder="Insira sua palavra chave	" value="">
						<span style="font-size: 13px;color :blue">Dica: ao escolher a palavra desejada, apertar a tecla ENTER para habilitar sua palavra chave.</span>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Benchmark /Concorrentes:</label>
						<textarea class="form-control" name="concorrentes" id="concorrentes" rows="3"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-6">
						<label for="">Congressos e Eventos relacionados ao tema:</label>
						<textarea class="form-control" name="congresso_eventos" id="congresso_eventos" rows="3"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6">
						<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
					</div>
					<div class="form-group col-lg-3  col-md-6 col-sm-6 col-xs-6 text-right">
						<a href="/"><button type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
					</div>
				</div>
			</form>
			<hr>
		</div>

	</div>
</div>
<?php $this->load->view('layout/rodape') ?>
