<div class="container-fluid">
	<div class="col-lg-12">
	    <div class="row">
	        <div class="lateral-fixed col-md-3">
				<?php $this->load->view('layout/menu_lateral') ?>
	        </div>
	        <div class="main col-md-9 box-cad-produto">
				<form method="post">
					<input type="hidden" name="id_curso" value="<?=($curso)?$curso->id_curso:''?>">
					<div class="row">
						<div class="form-group col-lg-8">
							<label for="">Nome do curso</label>
							<input type="text" class="form-control <?= (form_error('nome'))?'is-invalid':'' ?>" id="name" name="nome" placeholder="Nome do curso" value="<?= ($curso)?$curso->nome: set_value('nome') ?>">
							<div class="invalid-feedback"><?= form_error('nome') ?></div>
						</div>
					</div><br>
					<div class="row">
						<div class="form-group col-lg-8">
							<h6 class="sep-text">Criado por:<span style="font-weight: lighter"> <?=$user_created ?> em <?= date("d-m-Y") ?></span> - Versão: <span style="font-weight: lighter">01</span></h6>
						</div>
					</div><br>

						<div class="row">
							<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
								<?php $tipo = ($curso) ? 'Alterar' : 'Salvar' ?>
								<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;<?= $tipo ?></button>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-right">
								<a href="<?= base_url().'produto/cursos'?>"><button type="button" class="btn btn-secondary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
							</div>
						</div><br>

						<?php if($curso) { ?>
						<div class="row rodape">
							<div class="col-lg-12 col-md-12 col-xs-12">
								<a href="<?= base_url().'produto/add_docente/'.$curso->id_curso ?>" class="btn btn-info">Próxima etapa&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a>
							</div>
						</div>
						<?php } ?>


				</form>
			</div>
		</div>
	</div>
</div>
