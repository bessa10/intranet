
<?php $this->load->view('layout/topo') ?>
<?php $this->load->view('layout/menu') ?>
<div class="main">
<div class="container-fluid container_form" style="margin-top: 100px;margin-bottom: 100px">
<form class="docente_form" method="POST">
  <div class="row">
  <div class="form-group col-md-4">
    <label class="" for="exampleFormControlSelect1">Tipo Pessoa</label>
    <select class="form-control <?= (form_error('tipo_pessoa'))?'is-invalid':'' ?>"  name="tipo_pessoa" id="tipo_pessoa" value="<?= set_value('tipo_pessoa') ?>">
       <option value=""  >Selecione o tipo</option>
      <option value="PJ"  >Pessoa Juridica</option>
      <option value="PS" >Pessoa Física</option>
    </select>
  </div>
    <div class="form-group col-md-8">   
      <label>Nome Completo</label>
      <input id="nome_compl"  class="form-control <?= (form_error('nome_compl'))?'is-invalid':'' ?>" name="nome_compl" value="<?= set_value('nome_compl') ?>">
  </div>
</div>
 <div id="pessoa-fisica">
<div class="row">
   <div class="form-group col-md-2">
    <label class="" for="exampleFormControlSelect1">Ativo ?</label>
    <select class="form-control <?= (form_error('ativo'))?'is-invalid':'' ?> " name="ativo" id="exampleFormControlSelect1">
      <option value="S" >S</option>
      <option value="N" >N</option>
    </select>
  </div>
  <div class="form-group col-md-5">
      <label class="label-text">Nome abreviado</label>
      <input id="nome_abrev" class="form-control" name="nome_abrev" value="<?= set_value('nome_abrev') ?>">
       
   
  </div>
   <div class="form-group col-md-5">
      <label class="label-text">Nome social</label>
      <input id="nome_social" class="form-control" name="nome_social" value="<?= set_value('nome_social') ?>">
   </div>
 </div>
<div class="row">
   <div class="form-group col-md-2">
      <label class="label-text">Data de nascimento</label>
      <input class="form-control date <?= (form_error('dt_nasc'))?'is-invalid':'' ?>"  id="" name="dt_nasc" value="<?= set_value('dt_nasc') ?>"> 
  </div>
  <div class="form-group col-md-2">
    <label class="" for="exampleFormControlSelect1">Sexo</label>
    <select class="form-control" name="sexo" id="exampleFormControlSelect1">
      <option value="M" >M</option>
      <option value="F" >F</option>
    </select>
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">Estado Civil</label>
    <input class="write form-control <?= (form_error('est_civil'))?'is-invalid':'' ?>"  name="est_civil" value="<?= set_value('est_civil') ?>">    
  </div>
  <div class="form-group col-md-2">
      <label class="label-text">Ano ingresso</label>
      <input class="write form-control" name="ano_ingresso" value="<?= set_value('ano_ingresso') ?>">
  </div>
  <div class="form-group col-md-3">
      <label class="label-text">CPF</label>
      <input class="write form-control <?= (form_error('cpf'))?'is-invalid':'' ?>"  name="cpf" id="cpf"  value="<?= set_value('cpf') ?>">
  </div>
</div>
<div class="row">
  <div class="form-group col-md-2">
      <label class="label-text">RG Tipo</label>
      <input class="write form-control <?= (form_error('rg_tipo'))?'is-invalid':'' ?>"  name="rg_tipo" value="<?= set_value('rg_tipo') ?>">    
  </div>
  <div class="form-group col-md-4">
      <label class="label-text">Número do RG</label>
      <input class="write form-control <?= (form_error('rg_num'))?'is-invalid':'' ?>"  name="rg_num" value="<?= set_value('rg_num') ?>">     
  </div>
  <div class="form-group col-md-2">
    <label class="label-text">RG UF</label>
     <select class="form-control <?= (form_error('rg_uf'))?'is-invalid':'' ?>" name="rg_uf" id="exampleFormControlSelect1">
      <option value="AC">Acre</option>
      <option value="AL">Alagoas</option>
      <option value="AP">Amapá</option>
      <option value="AM">Amazonas</option>
      <option value="BA">Bahia</option>
      <option value="CE">Ceará</option>
      <option value="DF">Distrito Federal</option>
      <option value="ES">Espírito Santo</option>
      <option value="GO">Goiás</option>
      <option value="MA">Maranhão</option>
      <option value="MT">Mato Grosso</option>
      <option value="MS">Mato Grosso do Sul</option>
      <option value="MG">Minas Gerais</option>
      <option value="PA">Pará</option>
      <option value="PB">Paraíba</option>
      <option value="PR">Paraná</option>
      <option value="PE">Pernambuco</option>
      <option value="PI">Piauí</option>
      <option value="RJ">Rio de Janeiro</option>
      <option value="RN">Rio Grande do Norte</option>
      <option value="RS">Rio Grande do Sul</option>
      <option value="RO">Rondônia</option>
      <option value="RR">Roraima</option>
      <option value="SC">Santa Catarina</option>
      <option value="SP">São Paulo</option>
      <option value="SE">Sergipe</option>
      <option value="TO">Tocantins</option>
    </select>
      
  </div>
  <div class="form-group col-md-2">
      <label class="label-text">RG data Expedição</label>
      <input class="write date form-control <?= (form_error('rg_dtexp'))?'is-invalid':'' ?>"  name="rg_dtexp" value="<?= set_value('rg_dtexp') ?>">
  </div>
   <div class="form-group col-md-2">
      <label class="label-text">Emissor RG</label>
      <input class="write form-control <?= (form_error('rg_emissor'))?'is-invalid':'' ?>"  name="rg_emissor" value="<?= set_value('rg_emissor') ?>">
  </div>
</div>
<div class="row">
  <div class="form-group col-md-4">
      <label class="label-text">CEP</label>
      <input class="cep form-control <?= (form_error('cep'))?'is-invalid':'' ?>"  id="cep" name="cep" value="<?= set_value('cep') ?>">
  </div>
  <div class="form-group col-md-6">
      <label class="label-text">Endereço</label>
      <input class="form-control"  id="rua" name="endereco" value="<?= set_value('endereco') ?>">
  </div>
  <div class="form-group col-md-2">
      <label class="label-text">Número</label>
      <input class="form-control"  name="end_num" value="<?= set_value('end_num') ?>">
  </div>
</div>
<div class="row">
  <div class="form-group col-md-4">
      <label class="label-text">Complemento de endereço</label> 
      <input class="form-control" name="end_compl" value="<?= set_value('end_compl') ?>"> 
  </div>
   <div class="form-group col-md-4">
      <label class="label-text">Bairro</label>
      <input class="form-control"  id="bairro" name="bairro" value="<?= set_value('bairro') ?>">
  </div>
  <div class="form-group col-md-4">
     <label class="label-text">Cidade</label>
      <input  class="form-control" id="cidade" name="cidade" value="<?= set_value('cidade') ?>"> 
  </div>
</div>
<div class="row">
  <div class="form-group col-md-2">
      <label class="label-text">UF</label>
      <input class="form-control"  id="uf" name="uf" value="<?= set_value('uf') ?>">
  </div>
<div class="form-group col-md-5">
     <label class="label-text">Telefone</label>
     <input class="phone form-control" name="fone" value="<?= set_value('fone') ?>">
  </div>
  <div class="form-group col-md-5">
    <label class="label-text">Celular</label> 
      <input class="phone form-control <?= (form_error('celular'))?'is-invalid':'' ?>"  name="celular" value="<?= set_value('celular') ?>">       
  </div>
</div>
<div class="row">
  <div class="form-group col-md-6">
    <label class="label-text">Email</label> 
      <input class="form-control <?= (form_error('e_mail'))?'is-invalid':'' ?>"  name="e_mail" value="<?= set_value('e_mail') ?>">   
  </div>
   <div class="form-group col-md-6">
    <label class="label-text">Titulação</label>
      <input class="form-control" name="titulacao" value="<?= set_value('titulacao') ?>">    
  </div>
</div>
<div class="row">
  <div class="form-group col-md-3">
    <label class="label-text">Atuação profissional</label>
    <input class="form-control" name="atuacao_profis" value="<?= set_value('atuacao_profis') ?>">
  </div>
  <div class="form-group col-md-2">
    <label class="" for="exampleInputEmail1">Trabalha atualmente ?</label>
    <select class="form-control" name="trabalha_atualmente" id="exampleFormControlSelect1">
      <option value="S" >S</option>
      <option value="N" >N</option>
    </select>
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">Onde trabalha?</label>
      <input class="form-control"  name="onde_trabalha" value="<?= set_value('onde_trabalha') ?>">     
  </div>
  <div class="form-group col-md-4">
     <label class="label-text">Url particular</label>
      <input class="form-control" name="url_particular" value="<?= set_value('url_particular') ?>">     
  </div>
</div>
<div class="row">
  <div class="form-group col-md-3">
    <label class="label-text">Banco</label>
      <input class="form-control" name="banco" value="<?= set_value('banco') ?>">     
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">Agência</label>
      <input class="form-control"  name="agencia" value="<?= set_value('agencia') ?>">    
  </div>
   <div class="form-group col-md-3">
    <label class="label-text">Conta banco</label>
      <input class="form-control" name="conta_banco" value="<?= set_value('conta_banco') ?>">      
  </div>
  <div class="form-group col-md-3">
     <label class="label-text">Razão social</label>
      <input class="form-control" name="razao_social"  value="<?= set_value('razao_social') ?>">
  </div>
</div>
<div class="row">
  <div class="form-group col-md-12">
    <label for="exampleInputEmail1">OBS </label>
    <textarea class="form-control" name="obs" rows="3" value="<?= set_value('obs') ?>"></textarea>
  </div>
  </div>
</div>
<div id="pessoa-juridica">
    <h2 class="text-center mb-4" style="margin:0 auto">Empresa</h2>
    <div class="row">
    <div class="form-group col-md-3">
      <label class="label-text">CEP</label> 
        <input class="cep form-control" id="cep_empresa"  name="endemp_cep" value="<?= set_value('endemp_cep') ?>">         
  </div>
  <div class="form-group col-md-6">
    <label class="label-text">Endereço empresa</label>
       <input class="form-control" id="rua_empresa" name="endemp"  value="<?= set_value('endemp') ?>">     
  </div>
   <div class="form-group col-md-3">
    <label class="label-text">Número</label>
       <input class="form-control" name="endemp_num"  value="<?= set_value('endemp_num') ?>">  
  </div>
</div>
<div class="row">
  <div class="form-group col-md-4">
    <label class="label-text">Complemento</label>
      <input class="form-control" name="endemp_compl"  value="<?= set_value('endemp_compl') ?>">
  </div>
  <div class="form-group col-md-4">
    <label class="label-text">Bairro</label> 
      <input class="form-control" id="bairro_empresa"  name="endemp_bairro" value="<?= set_value('endemp_bairro') ?>">  
  </div>
  <div class="form-group col-md-4">
      <label class="label-text">Município</label>
      <input class="form-control"  id="cidade_empresa" name="endemp_municipio" value="<?= set_value('endemp_municipio') ?>">
  </div>
</div>
<div class="row">
  <div class="form-group col-md-3">
    <label class="label-text">Telefone empresa</label>
      <input class="phone form-control" name="fone_emp" value="<?= set_value('fone_emp') ?>">   
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">Email empresa</label>
      <input class="form-control" name="e_mail_emp"  value="<?= set_value('e_mail_emp') ?>">
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">CNPJ</label>
      <input class="form-control cnpj" name="cnpj" value="<?= set_value('cnpj') ?>">
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">Url profissional</label>  
       <input  class="form-control" name="url_profissional"  value="<?= set_value('url_profissional') ?>">   
  </div>
</div>
</div>
<div class="row">
<button type="submit" class="btn btn-success" style="margin-left: 15px;">Atualizar</button>
<a class="btn btn-info ml-4" href="<?=base_url().'produto/add_docente/'.$idCurso ?>">Voltar</a>
</div>
</form>
</div>
</div>
<?php $this->load->view('layout/rodape') ?>