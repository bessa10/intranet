
<?php $this->load->view('layout/topo') ?>
<?php $this->load->view('layout/menu') ?>
<div class="main">
<div class="container container_form" style="margin-top: 100px;margin-bottom: 100px">
<form class="docente_form" method="POST">
	<?php foreach($consul_docentes['dados'] as $docente) ?>
 
  <div class="row">
  	 <div class="form-group col-md-3">     
          <label>Número Funcionário</label>
          <input id="num_func" class="form-control" readonly name="num_func" value="<?= $docente['NUM_FUNC'] ?>">
      </div>
   <div class="form-group col-md-3">
      <label>Número Pessoa</label>
      <input id="pessoa" readonly class="form-control" name="pessoa" value="<?= $docente['PESSOA'] ?>">
  </div>
  <div class="form-group col-md-2">
    <label class="" for="exampleFormControlSelect1">Tipo Pessoa</label>
    <select class="form-control <?= (form_error('tipo_pessoa'))?'is-invalid':'' ?>"  name="tipo_pessoa" id="tipo_pessoa">
      <option value=""   >Selecione o tipo</option>
      <option value="PJ" <?= ($docente['TIPO_PESSOA'] == 'PJ' ? 'selected' : '') ?>  >Pessoa Juridica</option>
      <option value="PS" <?= ($docente['TIPO_PESSOA'] == 'PS' ? 'selected' : '') ?> >Pessoa Física</option>
    </select>
  </div>
    <div class="form-group col-md-4">   
      <label>Nome Completo</label>
      <input id="nome_compl"  class="form-control <?= (form_error('nome_compl'))?'is-invalid':'' ?>" name="nome_compl" value="<?= $docente['NOME_COMPL'] ?>">
  </div>
</div>
 <div id="pessoa-fisica">
<div class="row">
   <div class="form-group col-md-2">
    <label class="" for="exampleFormControlSelect1">Ativo ?</label>
    <select class="form-control <?= (form_error('ativo'))?'is-invalid':'' ?> " name="ativo" id="exampleFormControlSelect1">
      <option value="S" <?= ($docente['ATIVO'] == 'S' ? 'selected' : '') ?> >S</option>
      <option value="N" <?= ($docente['ATIVO'] == 'N' ? 'selected' : '') ?> >N</option>
    </select>
  </div>
  <div class="form-group col-md-5">
      <label class="label-text">Nome abreviado</label>
      <input id="nome_abrev" class="form-control" name="nome_abrev" value="<?= $docente['NOME_ABREV'] ?>">
       
   
  </div>
   <div class="form-group col-md-5">
      <label class="label-text">Nome social</label>
      <input id="nome_social" class="form-control" name="nome_social" value="<?= $docente['NOME_SOCIAL'] ?>">
   </div>
 </div>
<div class="row">
   <div class="form-group col-md-2">
      <label class="label-text">Data de nascimento</label>
      <input class="form-control date <?= (form_error('dt_nasc'))?'is-invalid':'' ?>"  id="" name="dt_nasc" value="<?= $docente['DT_NASC'] ?>"> 
  </div>
  <div class="form-group col-md-2">
    <label class="" for="exampleFormControlSelect1">Sexo</label>
    <select class="form-control" name="sexo" id="exampleFormControlSelect1">
      <option value="M" <?= ($docente['SEXO'] == 'M' ? 'selected' : '') ?> >M</option>
      <option value="F" <?= ($docente['SEXO'] == 'F' ? 'selected' : '') ?> >F</option>
    </select>
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">Estado Civil</label>
    <input class="write form-control <?= (form_error('est_civil'))?'is-invalid':'' ?>"  name="est_civil" value="<?= $docente['EST_CIVIL'] ?>">    
  </div>
  <div class="form-group col-md-2">
      <label class="label-text">Ano ingresso</label>
      <input class="write form-control" name="ano_ingresso" value="<?= $docente['ANO_INGRESSO'] ?>">
  </div>
  <div class="form-group col-md-3">
      <label class="label-text">CPF</label>
      <input class="write form-control <?= (form_error('cpf'))?'is-invalid':'' ?>"  name="cpf" id="cpf"  value="<?= $docente['CPF'] ?>">
  </div>
</div>
<div class="row">
  <div class="form-group col-md-2">
      <label class="label-text">RG Tipo</label>
      <input class="write form-control <?= (form_error('rg_tipo'))?'is-invalid':'' ?>"  name="rg_tipo" value="<?= $docente['RG_TIPO'] ?>">    
  </div>
  <div class="form-group col-md-4">
      <label class="label-text">Número do RG</label>
      <input class="write form-control <?= (form_error('rg_num'))?'is-invalid':'' ?>"  name="rg_num" value="<?= $docente['RG_NUM'] ?>">     
  </div>
  <div class="form-group col-md-2">
    <label class="label-text">RG UF</label>
     <select class="form-control <?= (form_error('rg_uf'))?'is-invalid':'' ?>" name="rg_uf" id="exampleFormControlSelect1">
      <option value="AC" <?= ($docente['RG_UF'] == 'AC' ? 'selected' : '') ?>>Acre</option>
      <option value="AL" <?= ($docente['RG_UF'] == 'AL' ? 'selected' : '') ?>>Alagoas</option>
      <option value="AP" <?= ($docente['RG_UF'] == 'AP' ? 'selected' : '') ?>>Amapá</option>
      <option value="AM" <?= ($docente['RG_UF'] == 'AM' ? 'selected' : '') ?>>Amazonas</option>
      <option value="BA" <?= ($docente['RG_UF'] == 'BA' ? 'selected' : '') ?>>Bahia</option>
      <option value="CE" <?= ($docente['RG_UF'] == 'CE' ? 'selected' : '') ?>>Ceará</option>
      <option value="DF" <?= ($docente['RG_UF'] == 'DF' ? 'selected' : '') ?>>Distrito Federal</option>
      <option value="ES" <?= ($docente['RG_UF'] == 'ES' ? 'selected' : '') ?>>Espírito Santo</option>
      <option value="GO" <?= ($docente['RG_UF'] == 'GO' ? 'selected' : '') ?>>Goiás</option>
      <option value="MA" <?= ($docente['RG_UF'] == 'MA' ? 'selected' : '') ?>>Maranhão</option>
      <option value="MT" <?= ($docente['RG_UF'] == 'MT' ? 'selected' : '') ?>>Mato Grosso</option>
      <option value="MS" <?= ($docente['RG_UF'] == 'MS' ? 'selected' : '') ?>>Mato Grosso do Sul</option>
      <option value="MG" <?= ($docente['RG_UF'] == 'MG' ? 'selected' : '') ?>>Minas Gerais</option>
      <option value="PA" <?= ($docente['RG_UF'] == 'PA' ? 'selected' : '') ?>>Pará</option>
      <option value="PB" <?= ($docente['RG_UF'] == 'PB' ? 'selected' : '') ?>>Paraíba</option>
      <option value="PR" <?= ($docente['RG_UF'] == 'PR' ? 'selected' : '') ?>>Paraná</option>
      <option value="PE" <?= ($docente['RG_UF'] == 'PE' ? 'selected' : '') ?>>Pernambuco</option>
      <option value="PI" <?= ($docente['RG_UF'] == 'PI' ? 'selected' : '') ?>>Piauí</option>
      <option value="RJ" <?= ($docente['RG_UF'] == 'RJ' ? 'selected' : '') ?>>Rio de Janeiro</option>
      <option value="RN" <?= ($docente['RG_UF'] == 'RN' ? 'selected' : '') ?>>Rio Grande do Norte</option>
      <option value="RS" <?= ($docente['RG_UF'] == 'RS' ? 'selected' : '') ?>>Rio Grande do Sul</option>
      <option value="RO" <?= ($docente['RG_UF'] == 'RO' ? 'selected' : '') ?>>Rondônia</option>
      <option value="RR" <?= ($docente['RG_UF'] == 'RR' ? 'selected' : '') ?>>Roraima</option>
      <option value="SC" <?= ($docente['RG_UF'] == 'SC' ? 'selected' : '') ?>>Santa Catarina</option>
      <option value="SP" <?= ($docente['RG_UF'] == 'SP' ? 'selected' : '') ?>>São Paulo</option>
      <option value="SE" <?= ($docente['RG_UF'] == 'SE' ? 'selected' : '') ?>>Sergipe</option>
      <option value="TO" <?= ($docente['RG_UF'] == 'TO' ? 'selected' : '') ?>>Tocantins</option>
    </select>
      
  </div>
  <div class="form-group col-md-2">
      <label class="label-text">RG data Expedição</label>
      <input class="write date form-control <?= (form_error('rg_dtexp'))?'is-invalid':'' ?>"  name="rg_dtexp" value="<?= $docente['RG_DTEXP'] ?>">
  </div>
   <div class="form-group col-md-2">
      <label class="label-text">Emissor RG</label>
      <input class="write form-control <?= (form_error('rg_emissor'))?'is-invalid':'' ?>"  name="rg_emissor" value="<?= $docente['RG_EMISSOR'] ?>">
  </div>
</div>
<div class="row">
  <div class="form-group col-md-4">
      <label class="label-text">CEP</label>
      <input class="cep form-control <?= (form_error('cep'))?'is-invalid':'' ?>"  id="cep" name="cep" value="<?= $docente['CEP'] ?>">
  </div>
  <div class="form-group col-md-6">
      <label class="label-text">Endereço</label>
      <input class="form-control"  id="rua" name="endereco" value="<?= $docente['ENDERECO'] ?>">
  </div>
  <div class="form-group col-md-2">
      <label class="label-text">Número</label>
      <input class="form-control"  name="end_num" value="<?= $docente['END_NUM'] ?>">
  </div>
</div>
<div class="row">
  <div class="form-group col-md-4">
      <label class="label-text">Complemento de endereço</label> 
      <input class="form-control" name="end_compl" value="<?= $docente['END_COMPL'] ?>"> 
  </div>
   <div class="form-group col-md-4">
      <label class="label-text">Bairro</label>
      <input class="form-control"  id="bairro" name="bairro" value="<?= $docente['BAIRRO'] ?>">
  </div>
  <div class="form-group col-md-4">
     <label class="label-text">Cidade</label>
      <input  class="form-control" id="cidade" name="cidade" value="<?= $docente['CIDADE'] ?>"> 
  </div>
</div>
<div class="row">
  <div class="form-group col-md-2">
      <label class="label-text">UF</label>
      <input class="form-control"  id="uf" name="uf" value="<?= $docente['UF'] ?>">
  </div>
<div class="form-group col-md-5">
     <label class="label-text">Telefone</label>
     <input class="phone form-control" name="fone" value="<?= $docente['FONE'] ?>">
  </div>
  <div class="form-group col-md-5">
    <label class="label-text">Celular</label> 
      <input class="phone form-control <?= (form_error('celular'))?'is-invalid':'' ?>"  name="celular" value="<?= $docente['CELULAR'] ?>">       
  </div>
</div>
<div class="row">
  <div class="form-group col-md-6">
    <label class="label-text">Email</label> 
      <input class="form-control <?= (form_error('e_mail'))?'is-invalid':'' ?>"  name="e_mail" value="<?= $docente['E_MAIL'] ?>">   
  </div>
   <div class="form-group col-md-6">
    <label class="label-text">Titulação</label>
      <input class="form-control" name="titulacao" value="<?= $docente['TITULACAO'] ?>">     
  </div>
</div>
<div class="row">
  <div class="form-group col-md-3">
    <label class="label-text">Atuação profissional</label>
    <input class="form-control" name="atuacao_profis" value="<?= $docente['ATUACAO_PROFIS'] ?>">
  </div>
  <div class="form-group col-md-2">
    <label class="" for="exampleInputEmail1">Trabalha atualmente ?</label>
    <select class="form-control" name="trabalha_atualmente" id="exampleFormControlSelect1">
      <option value="S" <?= ($docente['TRABALHA_ATUALMENTE'] == 'S' ? 'selected' : '') ?> >S</option>
      <option value="N" <?= ($docente['TRABALHA_ATUALMENTE'] == 'N' ? 'selected' : '') ?> >N</option>
    </select>
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">Onde trabalha?</label>
      <input class="form-control"  name="onde_trabalha" value="<?= $docente['ONDE_TRABALHA'] ?>">     
  </div>
  <div class="form-group col-md-4">
     <label class="label-text">Url particular</label>
      <input class="form-control" name="url_particular" value="<?= $docente['URL_PARTICULAR'] ?>">     
  </div>
</div>
<div class="row">
  <div class="form-group col-md-3">
    <label class="label-text">Banco</label>
      <input class="form-control" name="banco" value="<?= $docente['BANCO'] ?>">     
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">Agência</label>
      <input class="form-control"  name="agencia" value="<?= $docente['AGENCIA'] ?>">    
  </div>
   <div class="form-group col-md-3">
    <label class="label-text">Conta banco</label>
      <input class="form-control" name="conta_banco" value="<?= $docente['CONTA_BANCO'] ?>">      
  </div>
  <div class="form-group col-md-3">
     <label class="label-text">Razão social</label>
      <input class="form-control" name="razao_social"  value="<?= $docente['RAZAO_SOCIAL'] ?>">
  </div>
</div>
<div class="row">
  <div class="form-group col-md-12">
    <label for="exampleInputEmail1">OBS </label>
    <textarea class="form-control" name="obs" rows="3" value="<?= $docente['OBS'] ?>"></textarea>
  </div>
  </div>
</div>
<div id="pessoa-juridica">
    <h2 class="text-center mb-4" style="margin:0 auto">Empresa</h2>
    <div class="row">
    <div class="form-group col-md-3">
      <label class="label-text">CEP</label> 
        <input class="cep form-control" id="cep_empresa"  name="endemp_cep" value="<?= $docente['ENDEMP_CEP'] ?>">         
  </div>
  <div class="form-group col-md-6">
    <label class="label-text">Endereço empresa</label>
       <input class="form-control" id="rua_empresa" name="endemp"  value="<?= $docente['ENDEMP'] ?>">     
  </div>
   <div class="form-group col-md-3">
    <label class="label-text">Número</label>
       <input class="form-control" name="endemp_num"  value="<?= $docente['ENDEMP_NUM'] ?>">  
  </div>
</div>
<div class="row">
  <div class="form-group col-md-4">
    <label class="label-text">Complemento</label>
      <input class="form-control" name="endemp_compl"  value="<?= $docente['ENDEMP_COMPL'] ?>">
  </div>
  <div class="form-group col-md-4">
    <label class="label-text">Bairro</label> 
      <input class="form-control" id="bairro_empresa"  name="endemp_bairro" value="<?= $docente['ENDEMP_BAIRRO'] ?>">  
  </div>
  <div class="form-group col-md-4">
      <label class="label-text">Município</label>
      <input class="form-control"  id="cidade_empresa" name="endemp_municipio" value="<?= $docente['ENDEMP_MUNICIPIO'] ?>">
  </div>
</div>
<div class="row">
  <div class="form-group col-md-3">
    <label class="label-text">Telefone empresa</label>
      <input class="phone form-control" name="fone_emp" value="<?= $docente['FONE_EMP'] ?>">   
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">Email empresa</label>
      <input class="form-control" name="e_mail_emp"  value="<?= $docente['E_MAIL_EMP'] ?>">
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">CNPJ</label>
      <input class="form-control cnpj" name="cnpj" value="<?= $docente['CNPJ'] ?>">
  </div>
  <div class="form-group col-md-3">
    <label class="label-text">Url profissional</label>  
       <input  class="form-control" name="url_profissional"  value="<?= $docente['URL_PROFISSIONAL'] ?>">   
  </div>
</div>
</div>
<div class="row">
<button type="submit" class="btn btn-success" style="margin-left: 15px;">Atualizar</button>
<a class="btn btn-info ml-4" href="<?=base_url().'produto/add_docente/'.$idCurso ?>">Voltar</a>
</div>
</form>
</div>
</div>



<?php $this->load->view('layout/rodape') ?>