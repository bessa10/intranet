<style>
	.fade:not(.show) {
		opacity: 1;
	}
</style>
<div class="container-fluid">
	<div class="col-lg-12">
	    <div class="row">
	        <div class="lateral-fixed col-md-3">
				<?php $this->load->view('layout/menu_lateral') ?>
	        </div>
	        <div class="main col-md-9 box-cad-produto">
				<div class="row">
					<div class="col-md-12">
						<?php if(isset($curso)) { ?>
							<h5>NOME DO CURSO: <span style="font-weight: lighter"><?= $curso->nome ?></span></h5>
						<?php }?>
						<h6 class="sep-text">Criado por:<span style="font-weight: lighter"> <?=$user_created ?> em <?= date("d-m-Y") ?></span> - Versão: <span style="font-weight: lighter">01</span></h6>
						<form method="post">
							<input id="text_docente" onkeyup="desabilitarBotao()" type="text" placeholder="Buscar docente" name="nome_docente">
							<button id="buscar_docente" type="submit"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div><br>
				<div class="row">
					<?php if(isset($docente)) { ?>
					<div class="col-lg-12">
					<h6>COORDENADORES SELECIONADOS</h6>
					<table class="table table-bordered">
						<thead class="bg-secondary text-white">
							<tr>
								<th>Nome</th>
								<th>Email</th>
								<th class="text-center">Celular</th>
								<th class="text-center">Preenchido</th>
								<th class="text-center">Editar</th>
								<th class="text-center">Desativar</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($receber_docentes as $docente) { ?>
							<tr>
								<th scope="row"><?= $docente->nome_compl ?></th>
								<td><?= $docente->e_mail ?></td>
								<td class="text-center"><?= $docente->fone ?></td>
								<td>
									<div class="progress">
	  									<div class="progress-bar bg-primary" role="progressbar" style="width: <?= $docente->preenchido.'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= $docente->preenchido ?>%</div>
									</div>
								</td>
								<td class="text-center">
									<a href="<?= base_url().'produto/edit_docente/'.$idCurso.'?pessoa='.$docente->pessoa ?>" class="text-primary"><i class="fas fa-edit"></i></a>
								</td>
								<td class="text-center">
									<a href="javascript:" onclick="deletarDocente('<?= $docente->nome_compl ?>',<?= $docente->id_docente ?>)" class="text-danger"><i class="fas fa-trash"></i></a>
								</td>
								<!--
								<td class="text-center">
									<button type="button" onclick="deletarDocente('<?= $docente->nome_compl ?>',<?= $docente->id_docente ?>)" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
								</td>
								-->
							</tr>
							<?php } ?>
						</tbody>
					</table>
					</div>
					<?php } ?>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<a href="<?= base_url().'produto/new_docente/'.$idCurso ?>"><button class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>&nbsp;Cadastrar novo docente</button></a>
					</div>
					<div class="col-md-6 col-md-6 col-sm-6 col-xs-6 text-right">
						<a href="<?= base_url().'produto/add_curso/'.$idCurso ?>"><button type="button" class="btn btn-sm btn-secondary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</button></a>
					</div>
				</div><br>
				<?php if($receber_docentes){ ?>
				<div class="row rodape">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<a href="<?= base_url().'produto/add_categoria/'.$idCurso ?>" class="btn btn-info">Próxima etapa&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a>
					</div>
				</div>
				<?php } ?>
			</div>
		</div><br><br>
		<?php if(isset($consul_docentes['dados'])) { ?>
		<form method="post">
			<div class="row">
				<div class="form-group col-lg-12 box-cad-produto">
					<h6>DOCENTES ENCONTRADOS:</h6>
					<table class="table table-bordered" id="tabela-docentes">
						<thead class="bg-secondary text-white">
							<tr>
								<th style="width: 23%">Nome completo</th>
								<th>Titulação</th>
								<th>Atuação profissional</th>
								<th>E-mail</th>
								<th class="text-center">Celular</th>
								<th class="text-center">Ativo</th>
								<th>Preenchido</th>
								<th>Tornar coordenador</th>
								<th style="width: 17%;text-align: center">Ações</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($consul_docentes['dados'] as $docente) { ?>
							<tr>
								<td><strong><?= $docente['NOME_COMPL'] ?></strong></td>
								<td><?= $docente['TITULACAO'] ?></td>
								<td><?= $docente['ATUACAO_PROFIS'] ?></td>
								<td><?= $docente['E_MAIL'] ?></td>
								<td class="text-center"><?= $docente['CELULAR'] ?></td>
								<td class="text-center"><?= $docente['ATIVO'] ?></td>
								<td>
									<div class="progress">
	  									<div class="progress-bar bg-primary" role="progressbar" style="width: <?= $docente['PREENCHIDO'].'%' ?>;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?= $docente['PREENCHIDO']?>%</div>
									</div>
								</td>
								<!--<td><div class="w3-light-grey w3-round-xlarge"><div class="w3-container w3-blue w3-round-xlarge" style="width:<?= $docente['PREENCHIDO'].'%' ?> "><?= $docente['PREENCHIDO'].'%' ?></div></td>-->
								<td class="text-center">
									<a href="javascript:" onclick="adicionarDocente('<?= $docente['NUM_FUNC'] ?>','<?= $docente['PESSOA'] ?>','<?= $docente['TIPO_PESSOA'] ?>','<?= $docente['ATIVO'] ?>','<?= $docente['NOME_COMPL'] ?>','<?= $docente['NOME_ABREV'] ?>','<?= $docente['NOME_SOCIAL'] ?>','<?= $docente['DT_NASC'] ?>','<?= $docente['SEXO'] ?>','<?= $docente['EST_CIVIL'] ?>','<?= $docente['ANO_INGRESSO'] ?>','<?= $docente['CPF'] ?>','<?= $docente['RG_TIPO'] ?>','<?= $docente['RG_NUM'] ?>','<?= $docente['RG_DTEXP'] ?>','<?= $docente['RG_UF'] ?>','<?= $docente['RG_EMISSOR'] ?>','<?= $docente['ENDERECO'] ?>','<?= $docente['END_NUM'] ?>','<?= $docente['END_COMPL'] ?>','<?= $docente['BAIRRO'] ?>','<?= $docente['CIDADE'] ?>','<?= $docente['UF'] ?>','<?= $docente['CEP'] ?>','<?= $docente['FONE'] ?>','<?= $docente['CELULAR'] ?>','<?= $docente['E_MAIL'] ?>','<?= $docente['URL_PARTICULAR'] ?>','<?= $docente['TITULACAO'] ?>','<?= $docente['ATUACAO_PROFIS'] ?>','<?= $docente['TRABALHA_ATUALMENTE'] ?>','<?= $docente['ONDE_TRABALHA'] ?>','<?= $docente['OBS'] ?>','<?= $docente['BANCO'] ?>','<?= $docente['AGENCIA'] ?>','<?= $docente['CONTA_BANCO'] ?>','<?= $docente['RAZAO_SOCIAL'] ?>','<?= $docente['ENDEMP'] ?>','<?= $docente['ENDEMP_NUM'] ?>','<?= $docente['ENDEMP_COMPL'] ?>','<?= $docente['ENDEMP_BAIRRO'] ?>','<?= $docente['ENDEMP_MUNICIPIO'] ?>','<?= $docente['ENDEMP_CEP'] ?>','<?= $docente['FONE_EMP'] ?>','<?= $docente['E_MAIL_EMP'] ?>','<?= $docente['CNPJ'] ?>','<?= $docente['URL_PROFISSIONAL'] ?>','<?= $docente['PREENCHIDO'] ?>')" class="text-success" data-toggle="tooltip" data-placement="right" title="Tornar docente"><i class="fas fa-user-plus"></i></a>
								</td>
								<td style="display: flex;justify-content: space-around;">
									<div>
										<a href="<?= base_url().'produto/edit_docente/'.$idCurso.'?pessoa='.$docente['PESSOA'] ?>" class="text-primary" data-toggle="tooltip" data-placement="right" title="Editar"><i class="fas fa-edit"></i></a>
									</div>

									<div>
										<a href="javascript:" onclick="desativaDocente('<?= $docente['NUM_FUNC'] ?>','<?= $docente['PESSOA'] ?>','<?= $docente['TIPO_PESSOA'] ?>','N','<?= $docente['NOME_COMPL'] ?>','<?= $docente['NOME_ABREV'] ?>','<?= $docente['NOME_SOCIAL'] ?>','<?= $docente['DT_NASC'] ?>','<?= $docente['SEXO'] ?>','<?= $docente['EST_CIVIL'] ?>','<?= $docente['ANO_INGRESSO'] ?>','<?= $docente['CPF'] ?>','<?= $docente['RG_TIPO'] ?>','<?= $docente['RG_NUM'] ?>','<?= $docente['RG_DTEXP'] ?>','<?= $docente['RG_UF'] ?>','<?= $docente['RG_EMISSOR'] ?>','<?= $docente['ENDERECO'] ?>','<?= $docente['END_NUM'] ?>','<?= $docente['END_COMPL'] ?>','<?= $docente['BAIRRO'] ?>','<?= $docente['CIDADE'] ?>','<?= $docente['UF'] ?>','<?= $docente['CEP'] ?>','<?= $docente['FONE'] ?>','<?= $docente['CELULAR'] ?>','<?= $docente['E_MAIL'] ?>','<?= $docente['URL_PARTICULAR'] ?>','<?= $docente['TITULACAO'] ?>','<?= $docente['ATUACAO_PROFIS'] ?>','<?= $docente['TRABALHA_ATUALMENTE'] ?>','<?= $docente['ONDE_TRABALHA'] ?>','<?= $docente['OBS'] ?>','<?= $docente['BANCO'] ?>','<?= $docente['AGENCIA'] ?>','<?= $docente['CONTA_BANCO'] ?>','<?= $docente['RAZAO_SOCIAL'] ?>','<?= $docente['ENDEMP'] ?>','<?= $docente['ENDEMP_NUM'] ?>','<?= $docente['ENDEMP_COMPL'] ?>','<?= $docente['ENDEMP_BAIRRO'] ?>','<?= $docente['ENDEMP_MUNICIPIO'] ?>','<?= $docente['ENDEMP_CEP'] ?>','<?= $docente['FONE_EMP'] ?>','<?= $docente['E_MAIL_EMP'] ?>','<?= $docente['CNPJ'] ?>','<?= $docente['URL_PROFISSIONAL'] ?>','<?= $docente['PREENCHIDO'] ?>')" class="text-danger" data-toggle="tooltip" data-placement="right" title="Desativar"><i class="fas fa-trash"></i></a>
									</div>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<?php }?>
		
	</div>
</div>
<div id="modal_docente" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Coordenador selecionado:</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h6><span style="color:#000;" id="nome_compl_text"></span></h6>
				</div><br>
				<form method="post">
					<input type="hidden" name="num_func" id="num_func">
					<input type="hidden" name="pessoa" id="pessoa">
					<input type="hidden" name="tipo_pessoa" id="tipo_pessoa">
					<input type="hidden" name="ativo"  id="ativo">
					<input type="hidden" name="nome_compl"  id="nome_compl">
					<input type="hidden" name="nome_abrev"  id="nome_abrev">
					<input type="hidden" name="nome_social"  id="nome_social">
					<input type="hidden" name="dt_nasc"  id="dt_nasc">
					<input type="hidden" name="sexo"  id="sexo">
					<input type="hidden" name="est_civil"  id="est_civil">
					<input type="hidden" name="ano_ingresso"  id="ano_ingresso">
					<input type="hidden" name="cpf"  id="cpf">
					<input type="hidden" name="rg_tipo"  id="rg_tipo">
					<input type="hidden" name="rg_num" id="rg_num">
					<input type="hidden" name="rg_dtexp" id="rg_dtexp">
					<input type="hidden" name="rg_uf"  id="rg_uf">
					<input type="hidden" name="rg_emissor" id="rg_emissor">
					<input type="hidden" name="endereco" id="endereco">
					<input type="hidden" name="end_num" id="end_num">
					<input type="hidden" name="end_compl" id="end_compl">
					<input type="hidden" name="bairro" id="bairro">
					<input type="hidden" name="cidade" id="cidade">
					<input type="hidden" name="uf" id="uf">
					<input type="hidden" name="cep" id="cep">
					<input type="hidden" name="fone" id="telefone">
					<input type="hidden" name="celular" id="celular">
					<input type="hidden" name="e_mail" id="email">
					<input type="hidden" name="url_particular" id="url_particular">
					<input type="hidden" name="titulacao" id="titulacao">
					<input type="hidden" name="atuacao_profis" id="atuacao_profis">
					<input type="hidden" name="trabalha_atualmente" id="trabalha_atualmente">
					<input type="hidden" name="onde_trabalha" id="onde_trabalha">
					<input type="hidden" name="obs" id="obs">
					<input type="hidden" name="banco" id="banco">
					<input type="hidden" name="agencia" id="agencia">
					<input type="hidden" name="conta_banco" id="conta_banco">
					<input type="hidden" name="razao_social" id="razao_social">
					<input type="hidden" name="endemp" id="endemp">
					<input type="hidden" name="endemp_num" id="endemp_num">
					<input type="hidden" name="endemp_compl" id="endemp_compl">
					<input type="hidden" name="endemp_bairro" id="endemp_bairro">
					<input type="hidden" name="endemp_municipio" id="endemp_municipio">
					<input type="hidden" name="endemp_cep" id="endemp_cep">
					<input type="hidden" name="fone_emp" id="fone_emp">
					<input type="hidden" name="e_mail_emp" id="e_mail_emp">
					<input type="hidden" name="cnpj" id="cnpj">
					<input type="hidden" name="url_profissional" id="url_profissional">
					<input type="hidden" name="preenchido" id="preenchido">
					<div class="row">
						<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Adicionar docente</button>
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
							<button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;Fechar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div id="modal_delete" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Deletar docente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<br><p style="font-size:20px;font-weight:bold; font-weight:600; color:black;">Nome do docente: <span style="color:#000;" id="nome_compl_delete"></span></p>
					</div>
				</div>
				<form method="post" action="<?= base_url().'produto/delete_docente/'.$idCurso ?>">
					<input type="hidden" name="id_docente" id="id_docente">
					<button type="submit" class="btn btn-primary btn-avancar"><i class="fa fa-save"></i>&nbsp;&nbsp;Deletar docente do curso</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				</form>
			</div>
		</div>
	</div>
</div>
<div id="modal_desativa" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Desativar docente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<br><p style="font-size:20px;font-weight:bold; font-weight:600; color:black;">Nome do docente: <span style="color:#000;" id="nome_compl_desativa_text"></span></p>
					</div>
				</div>
				<form method="post" action="<?= base_url().'produto/desativa_docente/'.$idCurso ?>">
					<form method="post">
				<input type="hidden" name="num_func" id="num_func_desativa">
				<input type="hidden" name="pessoa" id="pessoa_desativa">
				<input type="hidden" name="tipo_pessoa" id="tipo_pessoa_desativa">
				<input type="hidden" name="ativo"  id="ativo_desativa" value="N">
				<input type="hidden" name="nome_compl"  id="nome_compl_desativa">
				<input type="hidden" name="nome_abrev"  id="nome_abrev_desativa">
				<input type="hidden" name="nome_social"  id="nome_social_desativa">
				<input type="hidden" name="dt_nasc"  id="dt_nasc_desativa">
				<input type="hidden" name="sexo"  id="sexo_desativa">
				<input type="hidden" name="est_civil"  id="est_civil_desativa">
				<input type="hidden" name="ano_ingresso"  id="ano_ingresso_desativa">
				<input type="hidden" name="cpf"  id="cpf_desativa">
				<input type="hidden" name="rg_tipo"  id="rg_tipo_desativa">
				<input type="hidden" name="rg_num" id="rg_num_desativa">
				<input type="hidden" name="rg_dtexp" id="rg_dtexp_desativa">
				<input type="hidden" name="rg_uf"  id="rg_uf_desativa">
				<input type="hidden" name="rg_emissor" id="rg_emissor_desativa">
				<input type="hidden" name="endereco" id="endereco_desativa">
				<input type="hidden" name="end_num" id="end_num_desativa">
				<input type="hidden" name="end_compl" id="end_compl_desativa">
				<input type="hidden" name="bairro" id="bairro_desativa">
				<input type="hidden" name="cidade" id="cidade_desativa">
				<input type="hidden" name="uf" id="uf_desativa">
				<input type="hidden" name="cep" id="cep_desativa">
				<input type="hidden" name="fone" id="telefone_desativa">
				<input type="hidden" name="celular" id="celular_desativa">
				<input type="hidden" name="e_mail" id="email_desativa">
				<input type="hidden" name="url_particular" id="url_particular_desativa">
				<input type="hidden" name="titulacao" id="titulacao_desativa">
				<input type="hidden" name="atuacao_profis" id="atuacao_profis_desativa">
				<input type="hidden" name="trabalha_atualmente" id="trabalha_atualmente_desativa">
				<input type="hidden" name="onde_trabalha" id="onde_trabalha_desativa">
				<input type="hidden" name="obs" id="obs_desativa">
				<input type="hidden" name="banco" id="banco_desativa">
				<input type="hidden" name="agencia" id="agencia_desativa">
				<input type="hidden" name="conta_banco" id="conta_banco_desativa">
				<input type="hidden" name="razao_social" id="razao_social_desativa">
				<input type="hidden" name="endemp" id="endemp_desativa">
				<input type="hidden" name="endemp_num" id="endemp_num_desativa">
				<input type="hidden" name="endemp_compl" id="endemp_compl_desativa">
				<input type="hidden" name="endemp_bairro" id="endemp_bairro_desativa">
				<input type="hidden" name="endemp_municipio" id="endemp_municipio_desativa">
				<input type="hidden" name="endemp_cep" id="endemp_cep_desativa">
				<input type="hidden" name="fone_emp" id="fone_emp_desativa">
				<input type="hidden" name="e_mail_emp" id="e_mail_emp_desativa">
				<input type="hidden" name="cnpj" id="cnpj_desativa">
				<input type="hidden" name="url_profissional" id="url_profissional_desativa">
				<input type="hidden" name="preenchido" id="preenchido_desativa">

				
					<button type="submit" class="btn btn-danger "><i class="fa fa-save"></i>&nbsp;&nbsp;Desativar docente</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				</form>
			</div>
		</div>
	</div>
</div>
