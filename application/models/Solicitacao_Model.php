<?php

class Solicitacao_Model extends CI_Model
{
	public function ultimos_chamados_sistemas($usuario = null)
	{
        if ($usuario != null) {

    	    $db2 = $this->load->database('adicional', TRUE);

    	    $db2->select('tickets.tid, tickets.title, tickets.status, tickets.category, statuses.label, statuses.cor_status, tickets.worker as worker_uid, tickets.created, sid');
            $db2->select('c.name as category, c.cid');
            $db2->select('p.name as project, p.pid');

            $db2->join('users as u', 'tickets.author = u.uid');

            $db2->join('statuses', 'status=sid', 'left');

            $db2->join('categories c', 'category=cid', 'left');
            $db2->join('projects p', 'project=pid', 'left');

            $db2->where('status != 0')->where('u.username', $usuario);

            $db2->order_by('tickets.created', 'desc');

            $db2->limit(2);

            $chamados = $db2->get('tickets')->result();

            return $chamados;

        } else {

            return false;
        }
	}

	public function ultimos_chamados_ti($usuario = null)
	{
        if ($usuario != null) {

    		$db2 = $this->load->database('adicional_ti', TRUE);

    		$db2->select('tickets.tid, tickets.title, tickets.status, tickets.category, statuses.label, statuses.cor_status, tickets.worker as worker_uid, tickets.created, sid');
            $db2->select('c.name as category, c.cid');
            $db2->select('p.name as project, p.pid');

            $db2->join('users as u', 'tickets.author = u.uid');

            $db2->join('statuses', 'status=sid', 'left');

            $db2->join('categories c', 'category=cid', 'left');
            $db2->join('projects p', 'project=pid', 'left');

            $db2->where('status != 0')->where('u.username', $usuario);

            $db2->order_by('tickets.created', 'desc');

            $db2->limit(2);

            $chamados = $db2->get('tickets')->result();

            return $chamados;

        } else {

            return false;
        }
	}


	public function ultimos_chamados_facilities($usuario = null)
	{  
        if ($usuario != null) {

    		$db2 = $this->load->database('adicional_facilities', TRUE);

    		$db2->select('tickets.tid, tickets.title, tickets.status, tickets.category, statuses.label, statuses.cor_status, tickets.worker as worker_uid, tickets.created, sid');
            $db2->select('c.name as category, c.cid');
            $db2->select('p.name as project, p.pid');

            $db2->join('users as u', 'tickets.author = u.uid');

            $db2->join('statuses', 'status=sid', 'left');

            $db2->join('categories c', 'category=cid', 'left');
            $db2->join('projects p', 'project=pid', 'left');

            $db2->where('status != 0')->where('u.username', $usuario);

            $db2->order_by('tickets.created', 'desc');

            $db2->limit(2);

            $chamados = $db2->get('tickets')->result();

            return $chamados;

        } else {

            return false;
        }
	}

    public function ultimos_chamados_int_dados($usuario = null)
    {  
        if ($usuario != null) {

            $db2 = $this->load->database('adicional_int_dados', TRUE);

            $db2->select('tickets.tid, tickets.title, tickets.status, tickets.category, statuses.label, statuses.cor_status, tickets.worker as worker_uid, tickets.created, sid');
            $db2->select('c.name as category, c.cid');
            $db2->select('p.name as project, p.pid');

            $db2->join('users as u', 'tickets.author = u.uid');

            $db2->join('statuses', 'status=sid', 'left');

            $db2->join('categories c', 'category=cid', 'left');
            $db2->join('projects p', 'project=pid', 'left');

            $db2->where('status != 0')->where('u.username', $usuario);

            $db2->order_by('tickets.created', 'desc');

            $db2->limit(2);

            $chamados = $db2->get('tickets')->result();

            return $chamados;

        } else {

            return false;
        }
    }

    public function ultimos_chamados_gestao_projetos($usuario = null)
    {  
        if ($usuario != null) {

            $db2 = $this->load->database('adicional_projetos_estrategicos', TRUE);

            $db2->select('tickets.tid, tickets.title, tickets.status, tickets.category, statuses.label, statuses.cor_status, tickets.worker as worker_uid, tickets.created, sid');
            $db2->select('c.name as category, c.cid');
            $db2->select('p.name as project, p.pid');

            $db2->join('users as u', 'tickets.author = u.uid');

            $db2->join('statuses', 'status=sid', 'left');

            $db2->join('categories c', 'category=cid', 'left');
            $db2->join('projects p', 'project=pid', 'left');

            $db2->where('status != 0')->where('u.username', $usuario);

            $db2->order_by('tickets.created', 'desc');

            $db2->limit(2);

            $chamados = $db2->get('tickets')->result();

            return $chamados;

        } else {

            return false;
        }
    }
}