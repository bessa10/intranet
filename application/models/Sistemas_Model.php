<?php

class Sistemas_Model extends CI_Model
{
	public function total_worker_tickets_mes($mes, $ano, $funcao = null)
	{	
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
			COUNT(uid) AS total_worker,
			SUM(valor_hora) AS soma_valor_hora 
			FROM users 
			WHERE uid IN (
				SELECT 
					DISTINCT tms.uid 
				FROM timesheet AS tms
					JOIN tickets AS tkt ON tkt.tid = tms.tid
				WHERE MONTH(tms.started) = ".$mes." 
					AND YEAR(tms.started) = ".$ano."
					AND tkt.status NOT IN(-1)
			)
		";

		if($funcao != null) {
			$sql .= " AND funcao = '".$funcao."'";
		}

		//echo $sql;die;

		$result = $db2->query($sql)->row();

		return $result;
	}

	public function total_horas_mes($mes, $ano)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT sum(horas) as total 
			FROM dias_ano 
			where month(data) = ".$mes." 
			AND YEAR(data) = ".$ano."
		";

		$result = $db2->query($sql)->row();

		return $result;
	}

	public function total_horas_mes_atual()
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
			SUM(horas) AS total
			FROM dias_ano
			WHERE DATA BETWEEN '".date('Y-m-01')."' AND '".date('Y-m-d')."'
		";

		$result = $db2->query($sql)->row();

		return $result;
	}

	public function list_dash_class($mes, $ano, $funcao)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT 
			dash_class.*,
			(
				SELECT
					SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) AS MIN
				FROM timesheet tms
					JOIN tickets tkt ON tkt.tid = tms.tid
					JOIN categories ctg ON ctg.cid = tkt.category
					JOIN users usr ON usr.uid = tms.uid
					JOIN dash_class dac ON dac.id_dash_class = ctg.id_dash_class
				WHERE tkt.status NOT IN(-1) 
					AND tkt.category NOT IN(57) 
					AND MONTH(tms.started) = ".$mes." 
					AND YEAR(tms.started) = ".$ano." 
					AND usr.funcao = '".$funcao."'
					AND dac.id_dash_class = dash_class.id_dash_class
				LIMIT 1 
			) AS total
			FROM dash_class
			ORDER BY total desc
		";

		$dashs = $db2->query($sql)->result();

		return $dashs;
	}

	public function valor_dash_class($mes, $ano, $id_dash_class, $funcao)
	{
		$db2 = $this->load->database('adicional', TRUE);

		/*
		// consulta antiga
		$sql = "
			SELECT
				SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) AS min,
				SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) / 60 AS hs,
				(SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished))/60) * (SELECT SUM(valor_hora) FROM users WHERE FUNCAO = '".$funcao."') AS valor
			FROM tickets tkt
				JOIN categories ctg ON ctg.cid = tkt.category
				JOIN projects prj ON prj.pid = tkt.project
				JOIN users usr ON usr.uid = tkt.worker
				JOIN timesheet tms ON tms.tid = tkt.tid
				JOIN dash_class dac ON dac.id_dash_class = ctg.id_dash_class
			WHERE tkt.status NOT IN(-1)
				AND tkt.category NOT IN(57)
				AND MONTH(tms.started)= ".$mes."
				AND YEAR(tms.started) = ".$ano."
				AND usr.funcao = '".$funcao."'
				AND ctg.id_dash_class = ".$id_dash_class."
			GROUP BY ctg.id_dash_class
		";
		*/

		$sql = "
			SELECT
				SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) AS min, 
				SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) / 60 AS hs, 
				(SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished))/60) * (
					SELECT SUM(valor_hora) FROM users WHERE FUNCAO = '".$funcao."') AS valor
			FROM timesheet tms
				JOIN tickets tkt ON tkt.tid = tms.tid
				JOIN categories ctg ON ctg.cid = tkt.category 
				JOIN projects prj ON prj.pid = tkt.project 
				JOIN users usr ON usr.uid = tms.uid
				JOIN dash_class dac ON dac.id_dash_class = ctg.id_dash_class
			WHERE tkt.status NOT IN(-1) 
				AND tkt.category NOT IN(57) 
				AND MONTH(tms.started)= ".$mes." 
				AND YEAR(tms.started) = ".$ano." 
				AND usr.funcao = '".$funcao."'
				AND ctg.id_dash_class = ".$id_dash_class."
			GROUP BY ctg.id_dash_class
		";

		//echo $sql;die;

		$result = $db2->query($sql)->row();

		return $result;

	}

	public function retorna_produtividade($funcao, $id_dash_class)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$produtividade = $db2->where('funcao', $funcao)
							->where('id_dash_class', $id_dash_class)
							->get('produtividade')
							->row();

		return $produtividade;
	}

	public function list_departments($mes, $ano)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT 
				departments.*,
				(
				SELECT 
					SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) AS MIN 
				FROM tickets tkt 
					JOIN categories ctg ON ctg.cid = tkt.category 
					JOIN projects prj ON prj.pid = tkt.project 
					JOIN users usr ON usr.uid = tkt.author 
					JOIN timesheet tms ON tms.tid = tkt.tid 
					JOIN departments dep ON dep.id_department = usr.id_department 
				WHERE tkt.status NOT IN(-1) 
					AND tkt.category NOT IN(57,28) 
					AND MONTH(tms.started)= ".$mes." 
					AND YEAR(tms.started) = ".$ano." 
					AND prj.visivel_geral = 1 
					AND ctg.visivel_geral = 1 
					AND usr.id_department = departments.id_department
				GROUP BY usr.id_department
				) AS total
			FROM departments
			ORDER BY total desc
		";

		$deps = $db2->query($sql)->result();

		return $deps;
	}

	public function list_categories($mes, $ano)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				categories.*,
				(
					SELECT
						SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) AS MIN 
					FROM tickets tkt 
						JOIN projects prj ON prj.pid = tkt.project 
						JOIN categories ctg ON ctg.cid = tkt.category 
						JOIN timesheet tms ON tms.tid = tkt.tid 
					WHERE tkt.status NOT IN(-1) 
						AND tkt.category NOT IN(57, 28) 
						AND MONTH(tms.started) = ".$mes." 
						AND YEAR(tms.started) = ".$ano." 
						AND ctg.visivel_geral = 1 
						AND prj.visivel_geral = 1 
						AND tkt.category = categories.cid 
					GROUP BY ctg.name
				) AS total
			FROM categories
			WHERE visivel_geral = 1
				AND cid NOT IN(57,28)
			ORDER BY total desc
		";

		$deps = $db2->query($sql)->result();			

		return $deps;
	}

	public function total_tickets_departments($mes, $ano, $id_department = null)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				dep.id_department,
				CASE
					WHEN dep.sigla_dep IS NULL THEN 'OUT'
					ELSE dep.sigla_dep
				END AS sigla_dep,
				dep.name_dep AS name_dep,
				COUNT(DISTINCT tkt.tid) AS total_tickets,
				SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) AS min
			FROM tickets tkt 
				JOIN categories ctg ON ctg.cid = tkt.category 
				JOIN projects prj ON prj.pid = tkt.project 
				JOIN users usr ON usr.uid = tkt.author 
				JOIN timesheet tms ON tms.tid = tkt.tid 
				LEFT JOIN departments dep ON dep.id_department = usr.id_department
			WHERE 
				tkt.status NOT IN(-1)
				AND tkt.category NOT IN(57,28)
				AND MONTH(tms.started)= ".$mes." 
				AND YEAR(tms.started) = ".$ano."
				AND prj.visivel_geral = 1
				AND ctg.visivel_geral = 1
		";

		if($id_department != null) {

			$sql .= " AND usr.id_department = ".$id_department."";

		} else {

			$sql .= " AND usr.id_department IS NULL";
		}

		$sql .= " GROUP BY usr.id_department ORDER BY COUNT(DISTINCT tkt.tid) DESC";

		//echo $sql;die;

		$result = $db2->query($sql)->row();

		return $result;
	}

	public function total_tickets_categories($mes, $ano, $cid = null)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT 
				ctg.cid,
				ctg.name, 
				COUNT(DISTINCT tkt.tid) AS total_tickets, 
				SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) AS min 
			FROM tickets tkt 
				JOIN projects prj ON prj.pid = tkt.project
				JOIN categories ctg ON ctg.cid = tkt.category
				JOIN timesheet tms ON tms.tid = tkt.tid
			WHERE tkt.status NOT IN(-1)
				AND tkt.category NOT IN(57)
				AND MONTH(tms.started)= ".$mes." 
				AND YEAR(tms.started) = ".$ano."
				AND ctg.visivel_geral = 1 
				AND prj.visivel_geral = 1
		";

		if($cid != null) {

			$sql .= " AND tkt.category = ".$cid."";

		} else {

			$sql .= " AND tkt.category IS NULL";
		}

		$sql .= " GROUP BY ctg.name ORDER BY COUNT(DISTINCT tkt.tid) DESC";

		//echo $sql;die;

		$result = $db2->query($sql)->row();

		return $result;
	}

	public function list_tickets_projects($mes, $ano, $limit = null, $not_in = null)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				prj.name AS name_project,
				prj.sigla_project AS sigla_project,
				COUNT(DISTINCT tkt.tid) AS total_tickets,
				SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) AS MIN
			FROM tickets tkt 
				JOIN projects prj ON prj.pid = tkt.project
				JOIN timesheet tms ON tms.tid = tkt.tid
			WHERE tkt.status NOT IN(-1)
				AND MONTH(tms.started)= ".$mes." 
				AND YEAR(tms.started) = ".$ano." 
		";

		$sql .= "
			GROUP BY tkt.project
			ORDER BY SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) DESC
		";

		if($limit != null) {
			$sql .= " LIMIT " . $limit;
		}

		$result = $db2->query($sql)->row();

		return $result;
	}

	public function list_workers_tickets_departamento($mes, $ano, $id_department = null)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				wk.uid AS 'id_worker',
				wk.name AS 'nome_worker',
				wk.valor_hora AS 'valor_hora'
			FROM tickets tkt
				JOIN users usr ON usr.uid = tkt.author
				JOIN timesheet tms ON tms.tid = tkt.tid
				JOIN users wk ON wk.uid = tms.uid 
			WHERE tkt.status NOT IN(-1)
				AND tkt.category NOT IN(57,28)
				AND MONTH(tms.started)= ".$mes." 
				AND YEAR(tms.started) = ".$ano."
		";

		if($id_department != null) {

			$sql .= " AND usr.id_department = ".$id_department."";

		} else {

			$sql .= " AND usr.id_department IS NULL";
		}

		$sql .= " GROUP BY tms.uid";

		//echo $sql;die;

		$result = $db2->query($sql)->result();

		return $result;
	}

	public function valor_tickets_departamento($mes, $ano, $id_department = null)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				SUM(ROUND(TIMESTAMPDIFF(minute,tms.started,tms.finished) / 60) * wk.valor_hora) AS 'valor_hora'
			FROM tickets tkt
				JOIN users usr ON usr.uid = tkt.author
				JOIN timesheet tms ON tms.tid = tkt.tid
				JOIN users wk ON wk.uid = tms.uid
				JOIN categories ctg ON ctg.cid = tkt.category 
				JOIN projects prj ON prj.pid = tkt.project
			WHERE MONTH(tms.started) = ".$mes." 
			AND tkt.status NOT IN(-1)
			AND tkt.category NOT IN(57,28)
			AND YEAR(tms.started) = ".$ano."
			AND prj.visivel_geral = 1
			AND ctg.visivel_geral = 1
		";

		if($id_department != null) {

			$sql .= " AND usr.id_department = ".$id_department."";

		} else {

			$sql .= " AND usr.id_department IS NULL";
		}

		//echo $sql;die;	

		$result = $db2->query($sql)->row();

		return $result;
	}

	public function valor_tickets_categories($mes, $ano, $cid = null)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT 
				SUM(ROUND(TIMESTAMPDIFF(minute,tms.started,tms.finished) / 60) * wk.valor_hora) AS 'valor_hora' 
			FROM tickets tkt 
				JOIN projects prj ON prj.pid = tkt.project
				JOIN categories ctg ON ctg.cid = tkt.category 
				JOIN timesheet tms ON tms.tid = tkt.tid 
				JOIN users wk ON wk.uid = tms.uid 
			WHERE tkt.status NOT IN(-1)
			AND tkt.category NOT IN(57) 
			AND MONTH(tms.started) = ".$mes." 
			AND YEAR(tms.started) = ".$ano."
			AND prj.visivel_geral = 1
			AND ctg.visivel_geral = 1
		";

		if($cid != null) {

			$sql .= " AND tkt.category = ".$cid."";

		} else {

			$sql .= " AND tkt.category IS NULL";
		}

		$result = $db2->query($sql)->row();

		return $result;

	}

	public function valor_tickets_projects($mes, $ano, $pid = null, $somente_dev = false)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT 
				SUM(ROUND(TIMESTAMPDIFF(minute,tms.started,tms.finished) / 60) * wk.valor_hora) AS 'valor_hora'
			FROM tickets tkt
				JOIN projects prj ON prj.pid = tkt.project
				JOIN categories ctg ON ctg.cid = tkt.category
				JOIN timesheet tms ON tms.tid = tkt.tid 
				JOIN users wk ON wk.uid = tms.uid 
			WHERE tkt.status NOT IN(-1)
				AND MONTH(tms.started) = ".$mes." 
				AND YEAR(tms.started) = ".$ano."
				AND prj.visivel_geral = 1
		";

		if($pid != null) {

			$sql .= " AND prj.pid = ".$pid."";

		} else {

			$sql .= " AND prj.pid IS NULL";
		}

		if($somente_dev != false) {
			$sql .= " AND tkt.category IN(28)";
		} else {
			$sql .= " AND tkt.category NOT IN(57,28)";
		}

		//echo $sql;die;

		$result = $db2->query($sql)->row();

		return $result;
	}


	public function valor_tickets_dev_fila($pid = null)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT 
				/*tkt.tid,
				tkt.title,
				tkt.status,
				tkt.project,
				tkt.category,
				tkt.worker,
				tkt.horas_previstas,
				wk.valor_hora,
				tkt.horas_previstas * wk.valor_hora AS valor_hora,
				*/
				SUM(tkt.horas_previstas * wk.valor_hora) AS valor_total
			FROM tickets tkt 
				JOIN projects prj ON prj.pid = tkt.project 
				JOIN users wk ON wk.uid = tkt.worker
			WHERE tkt.project = ".$pid."
				AND tkt.STATUS IN(1,3) 
				AND tkt.category = 28
				AND prj.visivel_geral = 1
		";

		//echo $sql;die;

		$result = $db2->query($sql)->row();

		return $result;
	}

	public function list_top_departments($mes, $ano)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT 
				dep.id_department, 
				CASE 
					WHEN dep.sigla_dep IS NULL THEN 'OUT' 
					ELSE dep.sigla_dep 
				END AS sigla_dep, 
				dep.name_dep AS name_dep,
				SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) / 60 AS hs
			FROM tickets tkt 
				JOIN categories ctg ON ctg.cid = tkt.category 
				JOIN projects prj ON prj.pid = tkt.project 
				JOIN users usr ON usr.uid = tkt.author 
				JOIN timesheet tms ON tms.tid = tkt.tid 
				LEFT JOIN departments dep ON dep.id_department = usr.id_department 
			WHERE tkt.status NOT IN(-1)
				AND tkt.category NOT IN(57,28)
				AND MONTH(tms.started)= ".$mes."
				AND YEAR(tms.started) = ".$ano."
				AND prj.visivel_geral = 1
				AND ctg.visivel_geral = 1
			GROUP BY usr.id_department 
			ORDER BY SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) DESC
		";

		//echo $sql;die;

		$result = $db2->query($sql)->result();

		return $result;
	}

	public function list_top_projects($mes, $ano, $somente_dev = false)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT 
				prj.pid,
				prj.name AS name_project,
				prj.sigla_project,
				SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) AS MIN
			FROM tickets tkt
				JOIN projects prj ON prj.pid = tkt.project
				JOIN categories ctg ON ctg.cid = tkt.category
				JOIN timesheet tms ON tms.tid = tkt.tid
			WHERE tkt.status NOT IN(-1)
				AND MONTH(tms.started)= ".$mes."
				AND YEAR(tms.started) = ".$ano."
				AND prj.visivel_geral = 1
		";

		if($somente_dev != false) {
			$sql .= " AND tkt.category IN(28)";
		} else {
			$sql .= " AND tkt.category NOT IN(57,28)";
		}

		$sql .= "
			GROUP BY tkt.project 
			ORDER BY SUM(TIMESTAMPDIFF(minute,tms.started,tms.finished)) DESC
		";

		//echo $sql;die;

		$result = $db2->query($sql)->result();

		return $result;
	}

	public function list_tickets_status()
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				sta.label_dash,
				COUNT(DISTINCT tkt.tid) AS 'total'
			FROM tickets tkt
				JOIN statuses sta ON sta.sid = tkt.status 
				JOIN categories ctg ON ctg.cid = tkt.category
			WHERE tkt.status NOT IN(-1, 2)
				AND tkt.category = 28
				AND tkt.category NOT IN(57)
			GROUP BY tkt.status
		";

		$result = $db2->query($sql)->result();

		return $result;

	}

	public function list_tickets_dev_fila()
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT 
				prj.pid, 
				prj.name AS name_project, 
				prj.sigla_project,
				SUM(tkt.horas_previstas) AS hs,
				SUM(tkt.horas_previstas) / 60 AS MIN
			FROM tickets tkt 
				LEFT JOIN projects prj ON prj.pid = tkt.project 
				LEFT JOIN categories ctg ON ctg.cid = tkt.category
			WHERE prj.visivel_geral = 1 
				AND tkt.category = 28 
				AND tkt.status IN(1,3)
				AND prj.visivel_geral = 1 
			GROUP BY tkt.project
			ORDER BY SUM(tkt.horas_previstas) DESC
		";

		//echo $sql;die;

		$result = $db2->query($sql)->result();

		return $result;
	}

	public function list_equipe_sistemas()
	{
		$db2 = $this->load->database('adicional', TRUE);

		$equipe = $db2
					->where('id_department', 18)
					->where('funcao IS NOT NULL')
					->where('funcao <>','SYS HEAD')
					->where('removed', 'N')
					->order_by('name ASC')
					->get('users')
					->result();

		return $equipe;

	}

	public function ultimo_tkt_trab($uid)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				tms.*,
				tkt.title,
				IF(loc.locais IS NOT NULL, 
					loc.locais, 
					(SELECT
							locais_trabalho.locais	
						FROM preferencia_local_usuario 
							JOIN locais_trabalho ON locais_trabalho.local_id =  preferencia_local_usuario.local_id
						WHERE id_user = tms.uid 
						ORDER BY preferencia ASC 
						LIMIT 1)
				) AS 'nome_local'
			FROM timesheet tms
				JOIN tickets tkt ON tkt.tid = tms.tid
				left JOIN locais_trabalho loc ON loc.local_id = tms.local_id
			WHERE tms.uid = ".$uid."
			ORDER BY tms.id_timesheet DESC
			LIMIT 1
		";

		$result = $db2->query($sql)->row();

		return $result;
	}

	public function horas_ult_tkt($tid, $uid, $somente_hoje = false)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				SUM(IF(tms.finished IS NULL, 
						TIMESTAMPDIFF(minute,tms.started,NOW()), 
						TIMESTAMPDIFF(minute,tms.started,tms.finished)
					)
				) AS MIN
			FROM tickets tkt 
				JOIN timesheet tms ON tms.tid = tkt.tid 
			WHERE tms.tid = ".$tid." 
			AND tms.uid = ".$uid."
		";

		if($somente_hoje != false) {
			$sql .= "AND tms.started BETWEEN '".date('Y-m-d 00:00:00')."' AND '".date('Y-m-d 23:59:59')."'";
		}

		$result = $db2->query($sql)->row();

		return $result;
	}

	public function tickets_worker_abertos($uid = null)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$status = array(-1, 4, 2);

		$categories = array(57);

		$db2
			->select('COUNT(tid) AS total')
			->where_not_in('status', $status)
			->where_not_in('category', $categories);

		if($uid != null) {
			$db2->where('worker', $uid);
		}

		$tickets = $db2->get('tickets')->row();

		return $tickets;
	}

	public function tickets_worker_progress($uid = null, $dev = false)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$status = array(3);
		$categories = array(57);

		$db2
			->select('COUNT(tid) AS total')
			->where_in('status', $status)
			->where_not_in('category', $categories);

		if($uid != null) {
			$db2->where('worker', $uid);
		}

		if($dev == true) {

			$db2->where('category', 28);

		} else {

			$db2->where('category <>', 28);
		}

		$tickets = $db2->get('tickets')->row();

		return $tickets;
	}

	public function tickets_atrasados($uid = null)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$status = array(-1, 4);
		$categories = array(57);

		$db2
			->select('COUNT(tid) AS total')
			->where('tkt_atrasado', 1)
			->where_not_in('status', $status)
			->where_not_in('category', $categories);

		if($uid != null) {
			$db2->where('worker', $uid);
		}

		$tickets = $db2->get('tickets')->row();

		return $tickets;
	}

	public function tickets_entregues_mes($mes, $ano, $somente_dev = null)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				COUNT(tkt.tid) AS total
			FROM tickets tkt
			WHERE tkt.status IN(4)
				AND tkt.category NOT IN(57)
				AND MONTH(tkt.completed) = ".$mes."
				AND YEAR(tkt.completed) = ".$ano."
		";

		if($somente_dev) {
			if($somente_dev == 1) {

				$sql .= " AND tkt.category = 28";

			} elseif ($somente_dev == 2) {
				
				$sql .= " AND tkt.category NOT IN(28)";
			}
		}

		$result = $db2->query($sql)->row();

		return $result;
	}

	public function list_tickets_tipo($mes, $ano, $cid)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				tkt.tid, 
				tkt.title, 
				DATE_FORMAT(tkt.created, '%d/%m/%Y') AS created, 
				tkt.description,
				tkt.started, 
				tkt.status AS ticket_tid, 
				tkt.worker, 
				tkt.horas_previstas,
				tkt.tkt_atrasado,
				tkt.diff_days,
				aut.name AS author, 
				aut.uid AS author_uid, 
				aut.email AS author_email,
				wkr.name AS worker, 
				wkr.uid AS worker_uid, 
				wkr.email AS worker_email,
				sta.label AS status, 
				sta.sid, 
				sta.cor_status,
				prj.name AS project, 
				prj.pid,
				cat.name as category, 
				cat.cid
			FROM tickets tkt
				JOIN users aut ON aut.uid = tkt.author
				JOIN users wkr ON wkr.uid = tkt.worker
				JOIN statuses sta ON sta.sid = tkt.status
				JOIN projects prj ON prj.pid = tkt.project 
				JOIN categories cat ON cat.cid = tkt.category
			WHERE tkt.status NOT IN(-1) 
				AND tkt.category IN(".$cid.")
				AND tkt.tid IN (
					SELECT 
						DISTINCT tid 
					FROM timesheet 
					WHERE MONTH(started) = ".$mes." 
						AND YEAR(started) = ".$ano."
				)
			GROUP BY tkt.tid
			ORDER BY tkt.tid ASC
		";

		$deps = $db2->query($sql)->result();			

		return $deps;
	}

	public function list_tickets_department($mes, $ano, $id_department)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				tkt.tid, 
				tkt.title, 
				DATE_FORMAT(tkt.created, '%d/%m/%Y') AS created, 
				tkt.description,
				tkt.started, 
				tkt.status AS ticket_tid, 
				tkt.worker, 
				tkt.horas_previstas,
				tkt.tkt_atrasado,
				tkt.diff_days,
				aut.name AS author, 
				aut.uid AS author_uid, 
				aut.email AS author_email,
				wkr.name AS worker, 
				wkr.uid AS worker_uid, 
				wkr.email AS worker_email,
				sta.label AS status, 
				sta.sid, 
				sta.cor_status,
				prj.name AS project, 
				prj.pid,
				cat.name as category, 
				cat.cid
			FROM tickets tkt
				JOIN users aut ON aut.uid = tkt.author
				JOIN users wkr ON wkr.uid = tkt.worker
				JOIN statuses sta ON sta.sid = tkt.status
				JOIN projects prj ON prj.pid = tkt.project 
				JOIN categories cat ON cat.cid = tkt.category
			WHERE tkt.status NOT IN(-1) 
				AND aut.id_department = ".$id_department."
				AND tkt.category NOT IN(57,28)
				AND prj.visivel_geral = 1
				AND cat.visivel_geral = 1
				AND tkt.tid IN (
					SELECT 
						DISTINCT tid 
					FROM timesheet 
					WHERE MONTH(started) = ".$mes." 
						AND YEAR(started) = ".$ano."
				)
			GROUP BY tkt.tid
			ORDER BY tkt.tid ASC
		";

		$deps = $db2->query($sql)->result();			

		return $deps;
	}

	public function list_devs_entregues_mes($mes, $ano)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				tkt.tid, 
				tkt.title, 
				DATE_FORMAT(tkt.created, '%d/%m/%Y') AS created,
				DATE_FORMAT(tkt.completed, '%d/%m/%Y') AS completed, 
				tkt.description,
				tkt.started, 
				tkt.status AS ticket_tid, 
				tkt.worker, 
				tkt.horas_previstas,
				tkt.tkt_atrasado,
				tkt.diff_days,
				aut.name AS author, 
				aut.uid AS author_uid, 
				aut.email AS author_email,
				wkr.name AS worker, 
				wkr.uid AS worker_uid, 
				wkr.email AS worker_email,
				sta.label AS status, 
				sta.sid, 
				sta.cor_status,
				prj.name AS project, 
				prj.pid,
				cat.name as category, 
				cat.cid
			FROM tickets tkt
				JOIN users aut ON aut.uid = tkt.author
				JOIN users wkr ON wkr.uid = tkt.worker
				JOIN statuses sta ON sta.sid = tkt.status
				JOIN projects prj ON prj.pid = tkt.project 
				JOIN categories cat ON cat.cid = tkt.category
			WHERE tkt.status IN(4)
				AND tkt.category = 28
				AND MONTH(tkt.completed) = ".$mes."
				AND YEAR(tkt.completed) = ".$ano."
			GROUP BY tkt.tid
			ORDER BY tkt.tid ASC
		";

		$deps = $db2->query($sql)->result();			

		return $deps;
	}

	public function total_tickets_mes($mes, $ano, $avaliados = false, $atraso_em_dia = null, $cid = null)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT 
				COUNT(DISTINCT tid) AS total 
			FROM tickets tkt
			WHERE tkt.status NOT IN(-1)
				AND tkt.category NOT IN(57, 28)
				AND MONTH(tkt.completed) = ".$mes."
				AND YEAR(tkt.completed) = ".$ano."
		";

		if($avaliados == true) {

			$sql .= " AND tkt.tid IN(SELECT ticket_id FROM pesquisa)";
		}

		if($atraso_em_dia) {

			$tipo = ($atraso_em_dia == 1) ? 0 : 1;

			$sql .= " AND tkt.tkt_atrasado = " . $tipo;
		}

		if($cid) {

			$sql .= " AND tkt.category = " . $cid;
		}

		$deps = $db2->query($sql)->row();			

		return $deps;
	}

	public function total_tickets_nota_mes($mes = null, $ano = null, $nota = null) {

		if($mes != null && $ano != null && $nota != null) {

			$db2 = $this->load->database('adicional', TRUE);

			$sql = "
				SELECT
					COUNT(distinct psq.ticket_id) AS total 
				FROM pesquisa psq
					INNER JOIN tickets tkt ON tkt.tid = psq.ticket_id
				WHERE tkt.status NOT IN(-1)
					AND tkt.category NOT IN(57, 28)
					AND MONTH(tkt.completed) = ".$mes."
					AND YEAR(tkt.completed) = ".$ano."
					AND psq.satisfacao = '".$nota."'
			";

			$notas = $db2->query($sql)->row();			

			return $notas;

		} else {

			return false;
		}
	}

	public function list_categories_tkts_fechados($mes, $ano)
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
				categories.*,
				(
					SELECT
						COUNT(distinct tkt.tid)
					FROM tickets tkt 
						JOIN projects prj ON prj.pid = tkt.project 
						JOIN categories ctg ON ctg.cid = tkt.category
					WHERE tkt.status NOT IN(-1) 
						AND tkt.category NOT IN(57, 28) 
						AND MONTH(tkt.completed) = ".$mes." 
						AND YEAR(tkt.completed) = ".$ano." 
						AND ctg.visivel_geral = 1 
						AND prj.visivel_geral = 1 
						AND tkt.category = categories.cid 
					GROUP BY ctg.name
				) AS total
			FROM categories
			WHERE visivel_geral = 1
				AND cid NOT IN(57,28)
			ORDER BY total desc
		";

		$deps = $db2->query($sql)->result();			

		return $deps;
	}

	public function list_mes_ano_filtro()
	{
		$db2 = $this->load->database('adicional', TRUE);

		$sql = "
			SELECT
			DATE_FORMAT(started, '%m/%Y') AS mes_ano,
			CONCAT(
				CASE
				    WHEN MONTH(started) = 1 THEN 'Janeiro'
				    WHEN MONTH(started) = 2 THEN 'Fevereiro'
				    WHEN MONTH(started) = 3 THEN 'Março'
				    WHEN MONTH(started) = 4 THEN 'Abril'
				    WHEN MONTH(started) = 5 THEN 'Maio'
				    WHEN MONTH(started) = 6 THEN 'Junho'
				    WHEN MONTH(started) = 7 THEN 'Julho'
				    WHEN MONTH(started) = 8 THEN 'Agosto'
				    WHEN MONTH(started) = 9 THEN 'Setembro'
				    WHEN MONTH(started) = 10 THEN 'Outubro'
				    WHEN MONTH(started) = 11 THEN 'Novembro'
				    WHEN MONTH(started) = 12 THEN 'Dezembro'    
		    		ELSE 'Desconhecido'
				END ,
				'/',YEAR(started)
			) AS txt_mes_ano
			FROM timesheet
			WHERE started > '2020-09-30 23:59:59'
			GROUP BY mes_ano
			ORDER BY started
		";

		$datas = $db2->query($sql)->result();			

		return $datas;
	}

	public function listar_log($dia)
	{
		$db2 = $this->load->database('adicional_site', TRUE);

		$sql = " SELECT * FROM log_transacoes WHERE tabela = 'formularios_interesse2' AND date(dth_insert) = '$dia'";

		$datas = $db2->query($sql)->result();

		return $datas;
	}

	public function listar_site($dia)
	{
		$db2 = $this->load->database('adicional_site', TRUE);

		$sql = "SELECT formularios_interesse2.nome,formularios_interesse2.email,cursos.nome_pt,cursos_datas.turma FROM 				formularios_interesse2 
				JOIN cursos ON formularios_interesse2.cod_curso = cursos.cod_curso
				LEFT JOIN cursos_datas ON formularios_interesse2.cod_data = cursos_datas.cod_data
				WHERE date(data) = '$dia' AND formularios_interesse2.erro = 0 ";

		$datas = $db2->query($sql)->result();

		return $datas;
	}

	public function listar_mkt($dia)
	{
		$db2 = $this->load->database('adicional_site', TRUE);

		$sql = "SELECT formularios_interesse2.nome,formularios_interesse2.email,cursos.nome_pt,cursos_datas.turma FROM 				formularios_interesse2 
				JOIN cursos ON formularios_interesse2.cod_curso = cursos.cod_curso
				LEFT JOIN cursos_datas ON formularios_interesse2.cod_data = cursos_datas.cod_data
				WHERE date(data) = '$dia' AND formularios_interesse2.erro = 0 
				AND formularios_interesse2.rdmkt = 1";

		$datas = $db2->query($sql)->result();

		return $datas;
	}

	public function listar_crm($dia)
	{
		$db2 = $this->load->database('adicional_site', TRUE);

		$sql = "SELECT formularios_interesse2.nome,formularios_interesse2.email,cursos.nome_pt,cursos_datas.turma FROM 				formularios_interesse2 
				JOIN cursos ON formularios_interesse2.cod_curso = cursos.cod_curso
				LEFT JOIN cursos_datas ON formularios_interesse2.cod_data = cursos_datas.cod_data
				WHERE date(data) = '$dia' AND formularios_interesse2.erro = 0 
				AND formularios_interesse2.rdcrm = 1";

		$datas = $db2->query($sql)->result();

		return $datas;
	}

	public function listar_oportunidades_manuais($dia)
	{
		$db2 = $this->load->database('adicional_site', TRUE);

		$sql = "
			SELECT 
			COUNT(cod_oportunidade) AS total
			FROM oportunidades_rdcrm
			WHERE DATE(created_at) = '".$dia."'
			AND excluido = 0
		";

		$datas = $db2->query($sql)->row();

		return $datas;
	}

	public function listar_oportunidades_erro($dia)
	{
		$db2 = $this->load->database('adicional_site', TRUE);

		$sql = "
			SELECT 
			COUNT(cod_formulario) AS total
			FROM formularios_interesse2
			WHERE DATE(data) = '".$dia."'
			AND erro = 1
		";

		$datas = $db2->query($sql)->row();

		return $datas;
	}

	public function listar_errocrm_marketing($dia)
	{
		$db2 = $this->load->database('adicional_site', TRUE);

		$sql="SELECT formularios_interesse2.nome,formularios_interesse2.email,cursos.nome_pt,cursos.curso 
			FROM formularios_interesse2
			JOIN cursos ON formularios_interesse2.cod_curso = cursos.cod_curso
			LEFT JOIN cursos_datas ON formularios_interesse2.cod_data = cursos_datas.cod_data
			WHERE rdmkt = 1
			AND rdcrm = 0
			AND erro <> 1
			AND DATE(data) = '".$dia."'";

		$datas = $db2->query($sql)->result();

		return $datas;
	}

}
