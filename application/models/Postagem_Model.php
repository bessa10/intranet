<?php

class Postagem_Model extends CI_Model
{
	public function gravar($FORM = null)
	{
		if($FORM != null) {

			$this->db->insert('tb_postagens', $FORM);

			$id = $this->db->insert_id('tb_postagens');

			return $id;

		} else {

			return false;
		}
	}

	public function alterar_postagem($dados = null, $id_postagem)
	{
		if($dados != null) {

			$this->db->where('id_postagem', $id_postagem)
				->update('tb_postagens', $dados);

			return true;

		} else {

			return false;
		}
	}

	public function gravar_anexos($attachments, $id_postagem) 
	{
		foreach ($attachments as $attachment) {

			$data['id_postagem']	= $id_postagem;
			$data['filename']		= $attachment['file_name'];
			$data['file_type']		= $attachment['file_type'];
			$data['file_ext']		= $attachment['file_ext'];

        	$supported_image = array('gif', 'jpg', 'jpeg', 'png');
        	$ext = strtolower(pathinfo($data['filename'], PATHINFO_EXTENSION));
        	
        	if(in_array($ext, $supported_image)) {
            
            	$data['is_image'] = 'Y';
        	}

        	$this->db->insert('tb_anexos_postagens', $data);
        }

        return true;
	}

	public function postagens_aprovadas()
	{
		$this->db
			->select('tb_usuarios.*')
			->select('tb_postagens.*')
			->join('tb_usuarios', 'tb_usuarios.id_usuario = tb_postagens.id_usuario')
			->where('tb_postagens.aprovado', 1)
			->where('tb_postagens.excluido', 0)
			->order_by('tb_postagens.dth_insert DESC');

		$postagens = $this->db->get('tb_postagens')->result();

		return $postagens;
	}

	public function postagens_aguardando()
	{
		$this->db
			->select('tb_usuarios.*')
			->select('tb_postagens.*')
			->join('tb_usuarios', 'tb_usuarios.id_usuario = tb_postagens.id_usuario')
			->where('tb_postagens.aprovado', 0)
			->where('tb_postagens.excluido', 0)
			->order_by('tb_postagens.dth_insert DESC');

		$postagens = $this->db->get('tb_postagens')->result();

		return $postagens;
	}

	public function aniversarios_mes()
	{	
		$mes_atual = date('m');

		$this->db
			->select('tb_usuarios.*')
			->where("MONTH(tb_usuarios.data_nascimento)", $mes_atual)
			->where('tb_usuarios.cancelado', 0)
			->order_by('tb_usuarios.data_nascimento');

		$usuarios = $this->db->get('tb_usuarios')->result();

		return $usuarios;
	}
}