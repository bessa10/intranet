<?php

class Gut_Model extends CI_Model 
{
	public function list_catalogo_servicos()
	{
		$db2 = $this->load->database('adicional', TRUE);

		$catalogos = $db2
			->select("categories_projects.*")
			->select('categories.name as nome_categoria')
			->select('projects.name as nome_projeto')
			->join('categories', 'categories.cid = categories_projects.cid')
			->join('projects', 'projects.pid = categories_projects.pid')
			->order_by('projects.name, categories.name')
			->get('categories_projects')
			->result();

		return $catalogos;

	}

	public function list_catalogo_servicos_ti()
	{
		$db2 = $this->load->database('adicional_ti', TRUE);

		$catalogos = $db2
			->select("categories_projects.*")
			->select('categories.name as nome_categoria')
			->select('projects.name as nome_projeto')
			->join('categories', 'categories.cid = categories_projects.cid')
			->join('projects', 'projects.pid = categories_projects.pid')
			->order_by('projects.name, categories.name')
			->get('categories_projects')
			->result();

		return $catalogos;

	}
	public function list_catalogo_servicos_facilities()
	{
		$db2 = $this->load->database('adicional_facilities', TRUE);

		$catalogos = $db2
			->select("categories_projects.*")
			->select('categories.name as nome_categoria')
			->select('projects.name as nome_projeto')
			->join('categories', 'categories.cid = categories_projects.cid')
			->join('projects', 'projects.pid = categories_projects.pid')
			->order_by('projects.name, categories.name')
			->get('categories_projects')
			->result();

		return $catalogos;

	}


	public function list_projects()
	{
		$db2 = $this->load->database('adicional', TRUE);

		$projects = $db2
			->order_by('name')
			->get('projects')
			->result();

		return $projects;

	}
	public function list_projects_ti()
	{
		$db2 = $this->load->database('adicional_ti', TRUE);

		$projects = $db2
			->order_by('name')
			->get('projects')
			->result();

		return $projects;

	}
	public function list_projects_facilities()
	{
		$db2 = $this->load->database('adicional_facilities', TRUE);

		$projects = $db2
			->order_by('name')
			->get('projects')
			->result();

		return $projects;

	}
}
