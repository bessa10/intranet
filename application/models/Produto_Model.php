<?php
class Produto_Model extends CI_Model
{
	public function add_curso($form)
    	{
    		$user = get_usuario();
    		$dados = array(
    			'nome' 			=> mb_strtoupper($form['nome'], 'UTF-8'),
				'dt_criacao' 	=> date("Y-m-d H:i:s"),
				'user_created' 	=> $user->nome
			);

			$this->db->insert('tb_curso', $dados);

			$curso = $this->db->insert_id();

			return $curso;
    	}

    public function update_nome_curso($idCurso, $form = null)
    {
    	$dados = array(
    		'nome' 			=> mb_strtoupper($form['nome'], 'UTF-8'),
    		'dt_criacao' 	=> date("Y-m-d H:i:s")
    	);

    	$this->db
    		->where('id_curso', $idCurso)
    		->update('tb_curso', $dados);

		return true;
    }

	public function get_products()
	{
		$this->db->select('tb_curso.curso');

		$query = $this->db->get('tb_curso');
		return $query->result();
	}

	public function get_nome_curso($form = null)
	{

		$this->db
			->where('nome', $form)
			->select('tb_curso.nome');

		$query = $this->db->get('tb_curso');

		return $query->row();
	}

	public function get_product($idCurso)
	{	
		$this->db
			->where('id_curso', $idCurso)
			->select('tb_curso.*');


		$query = $this->db->get('tb_curso');
		return $query->row();
	}


	public function add_docentes($form)
	{
		$dados = array(
			'num_func'          	=> 			$form['num_func'],
			'pessoa'            	=> 			$form['pessoa'],
			'tipo_pessoa'       	=> 			$form['tipo_pessoa'],
			'ativo'                 => 			$form['ativo'],
			'nome_compl'        	=> 			$form['nome_compl'],
			'nome_abrev'			=> 			$form['nome_abrev'],
			'nome_social'			=> 			$form['nome_social'],
			'dt_nasc'		    	=> 			$form['dt_nasc'],
			'sexo'					=> 			$form['sexo'],
			'est_civil'				=> 			$form['est_civil'],
			'ano_ingresso'			=> 			$form['ano_ingresso'],
			'cpf'					=> 			$form['cpf'],
			'rg_tipo'				=> 			$form['rg_tipo'],
			'rg_num'				=> 			$form['rg_num'],
			'rg_dtexp'				=> 			$form['rg_dtexp'],
			'rg_uf'					=> 			$form['rg_uf'],
			'rg_emissor'			=> 			$form['rg_emissor'],
			'endereco'				=> 			$form['endereco'],
			'end_num'				=> 			$form['end_num'],
			'end_compl'				=> 			$form['end_compl'],
			'bairro'				=> 			$form['bairro'],
			'cidade'				=> 			$form['cidade'],
			'uf'					=> 			$form['uf'],
			'cep'					=> 			$form['cep'],
			'fone'					=> 			$form['fone'],
			'celular'				=> 			$form['celular'],
			'e_mail'				=> 			$form['e_mail'],
			'url_particular'		=> 			$form['url_particular'],
			'titulacao'				=> 			$form['titulacao'],
			'atuacao_profis'		=> 			$form['atuacao_profis'],
			'trabalha_atualmente'	=> 			$form['trabalha_atualmente'],
			'onde_trabalha'			=> 			$form['onde_trabalha'],
			'obs'					=> 			$form['obs'],
			'banco'					=> 			$form['banco'],
			'agencia'				=> 			$form['agencia'],
			'conta_banco'			=> 			$form['conta_banco'],
			'razao_social'			=> 			$form['razao_social'],
			'endemp'				=> 			$form['endemp'],
			'endemp_num'			=> 			$form['endemp_num'],
			'endemp_compl'			=> 			$form['endemp_compl'],
			'endemp_bairro'			=> 			$form['endemp_bairro'],
			'endemp_municipio'		=> 			$form['endemp_municipio'],
			'endemp_cep'			=> 			$form['endemp_cep'],
			'fone_emp'				=> 			$form['fone_emp'],
			'e_mail_emp'			=> 			$form['e_mail_emp'],
			'cnpj'					=> 			$form['cnpj'],
			'url_profissional'		=> 			$form['url_profissional'],
			'preenchido'            =>          $form['preenchido']
		);

		$this->db->insert('tb_docentes', $dados);

		$docentes = $this->db->insert_id();

		return $docentes;
	}

	public function desativa_docente($form)
	{
		$dados = array(
			'num_func'          	=> 			$form['num_func'],
			'pessoa'            	=> 			$form['pessoa'],
			'tipo_pessoa'       	=> 			$form['tipo_pessoa'],
			'ativo'                 => 			$form['ativo'],
			'nome_compl'        	=> 			$form['nome_compl'],
			'nome_abrev'			=> 			$form['nome_abrev'],
			'nome_social'			=> 			$form['nome_social'],
			'dt_nasc'		    	=> 			$form['dt_nasc'],
			'sexo'					=> 			$form['sexo'],
			'est_civil'				=> 			$form['est_civil'],
			'ano_ingresso'			=> 			$form['ano_ingresso'],
			'cpf'					=> 			$form['cpf'],
			'rg_tipo'				=> 			$form['rg_tipo'],
			'rg_num'				=> 			$form['rg_num'],
			'rg_dtexp'				=> 			$form['rg_dtexp'],
			'rg_uf'					=> 			$form['rg_uf'],
			'rg_emissor'			=> 			$form['rg_emissor'],
			'endereco'				=> 			$form['endereco'],
			'end_num'				=> 			$form['end_num'],
			'end_compl'				=> 			$form['end_compl'],
			'bairro'				=> 			$form['bairro'],
			'cidade'				=> 			$form['cidade'],
			'uf'					=> 			$form['uf'],
			'cep'					=> 			$form['cep'],
			'fone'					=> 			$form['fone'],
			'celular'				=> 			$form['celular'],
			'e_mail'				=> 			$form['e_mail'],
			'url_particular'		=> 			$form['url_particular'],
			'titulacao'				=> 			$form['titulacao'],
			'atuacao_profis'		=> 			$form['atuacao_profis'],
			'trabalha_atualmente'	=> 			$form['trabalha_atualmente'],
			'onde_trabalha'			=> 			$form['onde_trabalha'],
			'obs'					=> 			$form['obs'],
			'banco'					=> 			$form['banco'],
			'agencia'				=> 			$form['agencia'],
			'conta_banco'			=> 			$form['conta_banco'],
			'razao_social'			=> 			$form['razao_social'],
			'endemp'				=> 			$form['endemp'],
			'endemp_num'			=> 			$form['endemp_num'],
			'endemp_compl'			=> 			$form['endemp_compl'],
			'endemp_bairro'			=> 			$form['endemp_bairro'],
			'endemp_municipio'		=> 			$form['endemp_municipio'],
			'endemp_cep'			=> 			$form['endemp_cep'],
			'fone_emp'				=> 			$form['fone_emp'],
			'e_mail_emp'			=> 			$form['e_mail_emp'],
			'cnpj'					=> 			$form['cnpj'],
			'url_profissional'		=> 			$form['url_profissional'],
			'preenchido'            =>          $form['preenchido']
		);


		$this->db->insert('tb_docentes', $dados);

		$docentes = $this->db->insert_id();

		return $docentes;
	}
		

	

	public function edit_docente($filtro2,$form)
	{
		$dados = array(
			'num_func'          	=> 			$form['num_func'],
			'pessoa'            	=> 			$form['pessoa'],
			'tipo_pessoa'       	=> 			$form['tipo_pessoa'],
			'ativo'                 => 			$form['ativo'],
			'nome_compl'        	=> 			$form['nome_compl'],
			'nome_abrev'			=> 			$form['nome_abrev'],
			'nome_social'			=> 			$form['nome_social'],
			'dt_nasc'		    	=> 			$form['dt_nasc'],
			'sexo'					=> 			$form['sexo'],
			'est_civil'				=> 			$form['est_civil'],
			'ano_ingresso'			=> 			$form['ano_ingresso'],
			'cpf'					=> 			$form['cpf'],
			'rg_tipo'				=> 			$form['rg_tipo'],
			'rg_num'				=> 			$form['rg_num'],
			'rg_dtexp'				=> 			$form['rg_dtexp'],
			'rg_uf'					=> 			$form['rg_uf'],
			'rg_emissor'			=> 			$form['rg_emissor'],
			'endereco'				=> 			$form['endereco'],
			'end_num'				=> 			$form['end_num'],
			'end_compl'				=> 			$form['end_compl'],
			'bairro'				=> 			$form['bairro'],
			'cidade'				=> 			$form['cidade'],
			'uf'					=> 			$form['uf'],
			'cep'					=> 			$form['cep'],
			'fone'					=> 			$form['fone'],
			'celular'				=> 			$form['celular'],
			'e_mail'				=> 			$form['e_mail'],
			'url_particular'		=> 			$form['url_particular'],
			'titulacao'				=> 			$form['titulacao'],
			'atuacao_profis'		=> 			$form['atuacao_profis'],
			'trabalha_atualmente'	=> 			$form['trabalha_atualmente'],
			'onde_trabalha'			=> 			$form['onde_trabalha'],
			'obs'					=> 			$form['obs'],
			'banco'					=> 			$form['banco'],
			'agencia'				=> 			$form['agencia'],
			'conta_banco'			=> 			$form['conta_banco'],
			'razao_social'			=> 			$form['razao_social'],
			'endemp'				=> 			$form['endemp'],
			'endemp_num'			=> 			$form['endemp_num'],
			'endemp_compl'			=> 			$form['endemp_compl'],
			'endemp_bairro'			=> 			$form['endemp_bairro'],
			'endemp_municipio'		=> 			$form['endemp_municipio'],
			'endemp_cep'			=> 			$form['endemp_cep'],
			'fone_emp'				=> 			$form['fone_emp'],
			'e_mail_emp'			=> 			$form['e_mail_emp'],
			'cnpj'					=> 			$form['cnpj'],
			'url_profissional'		=> 			$form['url_profissional'],
		);

		$this->db
		->where('num_func', $filtro2)
		->update('tb_docentes', $dados);
	}



	public function delete_curso_docente($idCurso, $form)
	{
		$this->db
			->where('id_curso', $idCurso)
			->where('id_docente', $form['id_docente'])
			->delete('tb_curso_docente');
	}

	public function delete_docente($docente)
	{
		$this->db
			->where('id_docente', $docente)
			->delete('tb_docentes');
	}


	public function add_especialidades()
	{
		$this->load->library('ApiLyceum');
		$consul_especialidade = $this->apilyceum->consulta_especilidades();

		foreach ($consul_especialidade['dados'] as $especialidade){
		 		$especialidade_verify = $this->verify_especialidade($especialidade['ESPECIALIDADE']);
				if(!$especialidade_verify) {
					$dados['especialidade'] = $especialidade['ESPECIALIDADE'];

					$this->db->insert('tb_especialidades', $dados);

				}

		}

		 return true;

	}

	

	public function get_especialidades()
	{
		$this->db->select('tb_especialidades.especialidade,tb_especialidades.id_especialidade');

		$query = $this->db->get('tb_especialidades');
		return $query->result();
	}

	public function get_especialidade()
	{
		$this->db->select('tb_especialidades.especialidade,tb_especialidades.id_especialidade');

		$query = $this->db->get('tb_especialidades');
		return $query->row();
	}

	public function get_area_atuacoes()
	{
		$this->db->select('tb_area_atuacao.*');

		$query = $this->db->get('tb_area_atuacao');
		return $query->result();
	}

	public function get_area_atuacao()
	{
		$this->db->select('tb_area_atuacao.*');

		$query = $this->db->get('tb_area_atuacao');
		return $query->row();
	}

	public function add_curso_especialidade($idCurso,$form)
	{

		for($i = 0; $i < count($form['especialidade']); $i++) {
			$validar_especialidade = $this->verify_especialidade_curso($form['especialidade'][$i], $idCurso);
			if (!$validar_especialidade) {
				$dados = array(
					'id_curso' => $idCurso,
					'id_especialidade' => $form['especialidade'][$i]
				);

				$this->db->insert('tb_curso_especialidade', $dados);
			}
		}

		return true;

	}

	public function add_curso_area_atuacao($idCurso,$form)
	{

		for($i = 0; $i < count($form['area_atuacao']); $i++) {
			$validar_area_atuacao = $this->verify_area_atuacao_curso($form['area_atuacao'][$i], $idCurso);
			if (!$validar_area_atuacao) {
				$dados = array(
					'id_curso' => $idCurso,
					'id_area' => $form['area_atuacao'][$i]
				);

				$this->db->insert('tb_curso_area_atuacao', $dados);
			}
		}

		return true;

	}


	public function verify_docente($form)
		{
			$verify =$this->db
							->where('num_func', $form['num_func'])
							->get('tb_docentes')
							->row();

			return $verify;
		}

		public function verify_mnemonico($form)
		{
			$verify = $this->db
							->where('mnemonico', $form['mnemonico'])
							->get('tb_curso')
							->row();

			return $verify;
		}

		public function verify_docente_base($filtro2)
		{
			$verify =$this->db
							->where('num_func', $filtro2)
							->get('tb_docentes')
							->row();

			return $verify;
		}

	public function verify_especialidade($especialidade)
	{
		$verify =$this->db
			->where('especialidade', $especialidade)
			->get('tb_especialidades')
			->row();

		return $verify;
	}


	public function verify_area_atuacao($area_atuacao)
	{
		$verify =$this->db
			->where('area_atuacao', $area_atuacao)
			->get('tb_area_atuacao')
			->row();

		return $verify;
	}
		public function add_curso_docente($idCurso,$add_docentes)
		{
			$dados = array(
				'id_curso' => $idCurso,
				'id_docente' => $add_docentes
			);


			$this->db->insert('tb_curso_docente', $dados);

			return true;

		}

	public function verify_docente_curso($idCurso,$add_docentes)
	{
		$verify_curso_docente =$this->db
			->where('id_docente', $add_docentes)
			->where('id_curso', $idCurso)
			->get('tb_curso_docente')
			->row();

		return $verify_curso_docente;
	}

	public function verify_especialidade_curso($id_especialidade, $idCurso)
	{
		$verify_curso_especialidade =$this->db
			->where('id_especialidade', $id_especialidade)
			->where('id_curso', $idCurso)
			->get('tb_curso_especialidade')
			->row();

		return $verify_curso_especialidade;
	}

	public function verify_area_atuacao_curso($id_area_atuacao, $idCurso)
	{
		$verify_curso_area_atuacao =$this->db
			->where('id_area', $id_area_atuacao)
			->where('id_curso', $idCurso)
			->get('tb_curso_area_atuacao')
			->row();

		return $verify_curso_area_atuacao;
	}

	public function get_docentes($idCurso)
	{
		$receber_docentes=$this->db
			->where('tb_curso_docente.id_curso' , $idCurso)
			->join('tb_docentes', 'tb_docentes.id_docente = tb_curso_docente.id_docente')
			->get('tb_curso_docente')
			->result();

		return $receber_docentes;
	}
	public function get_docente($idCurso)
	{
		$receber_docentes=$this->db
			->where('tb_curso_docente.id_curso' , $idCurso)
			->join('tb_docentes', 'tb_docentes.id_docente = tb_curso_docente.id_docente')
			->get('tb_curso_docente')
			->row();

		return $receber_docentes;
	}

	public function add_categoria($form, $idCurso)
	{
		$dados = array(
			'tipo' 			=> $form['tipo'],
			'modalidade' 	=> $form['modalidade'],
			'grupo_curso' 	=> $form['grupo_curso'],
			'mnemonico'		=> $form['mnemonico']
		);

		$this->db
			->where('id_curso', $idCurso)
			->update('tb_curso', $dados);

		return true;
	}

	public function add_persona($form)
	{
		$dados = array(
			'nome' => $form['nome'],
			'idade'=> $form['idade'],
			'especialidade' => $form['especialidade'],
			'cargo_ocupacao' => $form['cargo_ocupacao'],
			'onde_trabalha' => $form['onde_trabalha'],
			'nivel_especializacao' => $form['nivel_especializacao'],
			'meios_comunicacao' => $form['meios_comunicacao'],
			'principais_objetivos' => $form['principais_objetivos'],
			'principais_problemas' => $form['principais_problemas'],
			'como_curso_pode_ajudar' => $form['como_curso_pode_ajudar'],
		);

		$this->db->insert('tb_persona', $dados);

		$persona = $this->db->insert_id();

		return $persona;

	}

	public function get_personas($idCurso)
	{
		$receber_persona=$this->db
			->where('tb_curso_persona.id_curso' , $idCurso)
			->join('tb_persona', 'tb_persona.id_persona = tb_curso_persona.id_persona')
			->get('tb_curso_persona')
			->result();

		return $receber_persona;
	}

	public function add_curso_persona($idCurso,$persona)
	{
		$dados = array(
			'id_curso' => $idCurso,
			'id_persona' => $persona
		);


		$this->db->insert('tb_curso_persona', $dados);

		return true;

	}

	public function verify_persona_curso($idCurso,$persona)
	{
		$verify_curso_persona =$this->db
			->where('id_curso', $idCurso)
			->where('id_persona', $persona)
			->get('tb_curso_persona')
			->row();

		return $verify_curso_persona;
	}

	public function add_relevancia($form, $idCurso)
	{
		$dados = array(
			'relevancia' => $form['relevancia'],
			'objetivos_especificos' => $form['objetivos_especificos'],
			'diferenciais_determinantes' => $form['diferenciais_determinantes'],
			'concorrentes' => $form['concorrentes'],
			'congresso_eventos' => $form['congresso_eventos']
		);

		$this->db
			->where('id_curso', $idCurso)
			->update('tb_curso', $dados);

		return true;


	}

	public function add_key_words($idCurso,$add_keywords)
	{
		$dados = array(
			'id_curso' => $idCurso,
			'key_words' => $add_keywords
		);

		$this->db->insert('tb_curso_keyword', $dados);
	}

	public function alterar_etapa($id_curso = null, $etapa = null)
	{
		if($id_curso != null && $etapa != null) {

			$dados['etapa']	= $etapa;

			$this->db->where('id_curso', $id_curso)->update('tb_curso', $dados);

			return true;

		} else {

			return false;
		}
	}

	public function cursos_incompletos()
	{
		$cursos = $this->db
					->select('tb_curso.*')
					->select('nome_etapa AS nome_etapa, etp.url AS url_etapa')
					->join('tb_etapa_cad etp', 'etp.id_etapa = tb_curso.etapa', 'left')
					->where('tb_curso.cad_completo', 0)
					->where('tb_curso.excluido', 0)
					->order_by('dt_criacao DESC')
					->get('tb_curso')
					->result();

		return $cursos;
	}

	public function list_modalidades()
	{
		$modalidades = $this->db
						->order_by('modalidade')
						->get('tb_modalidades')
						->result();

		return $modalidades;	
	}
}
