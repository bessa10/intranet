<?php

class Card_Model extends CI_Model
{

	public function add_cartao($FORM = null)
	{
		
		if($FORM != null) {
			$this->db->insert('tb_cartao', $FORM);
			$id = $this->db->insert_id('tb_cartao');
			return $id;

		} else {
		
			return false;
		}
	}

	public function get_cartao($id_cartao = null)
	{

		$this->db
			->where('id_cartao', $id_cartao)
			->select('tb_cartao.*');

		$query = $this->db->get('tb_cartao');
		return $query->row();
	
	}


}