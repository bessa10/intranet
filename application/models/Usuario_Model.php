<?php

class Usuario_Model extends CI_Model
{
	public function findUsuario($id_usuario)
	{
		$usuario = $this->db
						->where('id_usuario', $id_usuario)
						->where('cancelado', 0)
						->get('tb_usuarios')
						->row();

		return $usuario;
	}
}