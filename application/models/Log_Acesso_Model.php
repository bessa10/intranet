<?php


class Log_Acesso_Model extends CI_Model
{
	public function grava_log($url,$user)
	{
		
    	$dados = array(
    		'uid' 			=> $user->id_usuario,
			'nome' 			=> $user->nome,
			'url_pagina' 	=> $url,
			'data' 			=> date('Y-m-d H:i:s')
		);

		$this->db->insert('log_acesso', $dados);

		$log = $this->db->insert_id();

		return $log;
    }
}